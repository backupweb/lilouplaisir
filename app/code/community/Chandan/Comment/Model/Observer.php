<?php

class Chandan_Comment_Model_Observer extends Varien_Object
{
	 public function commentadd(Varien_Event_Observer $observer) {
		$order = $observer->getEvent()->getOrder();
		$orderid = $order->getIncrementId();
		$customeremail = $order->getCustomerEmail();	
		$orderComment = Mage::getSingleton('core/session')->gettextval();
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablename   = Mage::getSingleton('core/resource')->getTableName('comment');
		$write->insert($tablename, array("ordernumber" => $orderid, "customeremail" => $customeremail, "comment" => $orderComment));	
		$order->addStatusHistoryComment($orderComment);
		$order->save();
		Mage::getSingleton('core/session')->setnewcomment($orderComment);
		Mage::getSingleton('core/session')->unstextval();
    }   
}
