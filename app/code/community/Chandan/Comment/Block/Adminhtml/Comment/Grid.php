<?php
class Chandan_Comment_Block_Adminhtml_Comment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('commentGrid');
      $this->setDefaultSort('comment_id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('comment/comment')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('comment_id', array(
          'header'    => Mage::helper('chandan_comment')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'comment_id',
      ));

      $this->addColumn('ordernumber', array(
          'header'    => Mage::helper('chandan_comment')->__('Order Number'),
          'align'     =>'left',
		    'width'     => '50px',
          'index'     => 'ordernumber',
      ));

	  
      $this->addColumn('customeremail', array(
			'header'    => Mage::helper('chandan_comment')->__('Customer Email Address'),
			'width'     => '150px',
			'index'     => 'customeremail',
      ));
	  
	   $this->addColumn('comment', array(
			'header'    => Mage::helper('chandan_comment')->__('Order Comment'),
			'width'     => '450px',
			'index'     => 'comment',
      ));
	 

    /*  $this->addColumn('status', array(
          'header'    => Mage::helper('chandan_comment')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	 */
       // $this->addColumn('action',
       //    array(
       //         'header'    =>  Mage::helper('chandan_comment')->__('Action'),
       //         'width'     => '100',
       //         'type'      => 'action',
       //         'getter'    => 'getId',
       //         'actions'   => array(
       //             array(
       //                 'caption'   => Mage::helper('chandan_comment')->__('Edit'),
       //                 'url'       => array('base'=> '*/*/edit'),
       //                 'field'     => 'id'
       //             )
       //         ),
       //         'filter'    => false,
       //         'sortable'  => false,
       //         'index'     => 'stores',
       //         'is_system' => true,
       // ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('chandan_comment')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('chandan_comment')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('comment_id');
        $this->getMassactionBlock()->setFormFieldName('comment');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('chandan_comment')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('chandan_comment')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('comment/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('chandan_comment')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('chandan_comment')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}