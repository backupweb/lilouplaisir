<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var LaPoste_Label_Helper_Data $helper */
$helper = Mage::helper('laposte_label');

/* Add Hs code */
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_hs_code', array(
    'group'             => 'General',
    'type'              => 'varchar',
    'backend'           => '',
    'frontend'          => '',
    'label'             => $helper->__('Customs Tariff Number'),
    'input'             => 'text',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'note'              => $helper->__(
        'Required for international shipment. Get code <a href="' . $helper->getHsCodeLink() . '" target="_blank">here</a>.'
    )
));

/* Add shipping label file column */
$this->getConnection()->addColumn(
    $this->getTable('sales/shipment'), 'shipping_label_file', "VARCHAR(255) NULL"
);

$installer->endSetup();