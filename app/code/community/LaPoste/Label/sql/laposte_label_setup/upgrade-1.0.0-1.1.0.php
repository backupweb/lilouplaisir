<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$this->getConnection()->addColumn(
    $this->getTable('sales/order'), 'colissimo_insurance', "INT(1) NULL DEFAULT 0"
);

$installer->endSetup();