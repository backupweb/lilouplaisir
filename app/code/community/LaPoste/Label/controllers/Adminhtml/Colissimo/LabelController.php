<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

require_once Mage::getModuleDir('controllers', 'LaPoste_Label'). DS . 'Adminhtml' . DS . 'Sales' . DS . 'Order' . DS . 'ShipmentController.php';

class LaPoste_Label_Adminhtml_Colissimo_LabelController extends LaPoste_Label_Adminhtml_Sales_Order_ShipmentController
{

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Print Label
     */
    public function printLabelAction()
    {
        try {
            /** @var LaPoste_Label_Helper_Data $helper */
            $helper = Mage::helper('laposte_label');

            $type = $this->getRequest()->getParam('type');

            /** @var LaPoste_Label_Model_Sales_Order_Shipment $shipment */
            $shipment = $this->_initShipment();

            $canMerge = false;

            switch ($type) {
                case 'label':
                    $labelFile = $shipment->getShippingLabel();
                    $canMerge = true;
                    break;
                case 'cn23':
                    $labelFile = $shipment->getShippingCn23();
                    break;
                case 'return':
                    $labelFile = $helper->getLabelDir() . $helper->getReturnFileName($shipment->getOrder()->getIncrementId());
                    break;
                default:
                    $labelFile = $shipment->getShippingLabel();
                    break;
            }

            if ($labelFile) {
                $file = file_get_contents($labelFile);

                if ($canMerge && $helper->getMergeShippingSlips() && preg_match('/\.pdf$/', $labelFile)) {
                    /** @var Mage_Sales_Model_Order_Pdf_Shipment $pdfShipment */
                    $pdfShipment = Mage::getModel('sales/order_pdf_shipment');
                    $packingSlips = $pdfShipment->getPdf(array($shipment));
                    $file = $helper->mergePdfFiles(array($packingSlips->render(), $file));
                }

                return $this->_prepareDownloadResponse(basename($labelFile), $file);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()
                ->addError(Mage::helper('laposte_label')->__('An error occurred while creating shipping label.'));
        }
        $this->_redirect('*/*');
    }

    /**
     * Generate Label
     */
    public function generateLabelAction()
    {
        try {
            $shipment = $this->_initShipment();
            if (!$shipment) {
                $this->_forward('noRoute');
                return;
            }

            $message = '';

            /** @var LaPoste_Label_Helper_Data $helper */
            $helper = Mage::helper('laposte_label');

            $sendMail = false;

            if (!$shipment->getId()) {
                $shipment->register();
                $shipment->setEmailSent(true);
                $message .= $helper->__('The shipment has been created.') . ' ';
                $sendMail = $shipment->getOrder()->getCustomerNoteNotify();
            }

            $this->_createShippingLabel($shipment);
            $this->_saveShipment($shipment);

            if ($sendMail) {
                $shipment->sendEmail(true);
            }

            $message .= $helper->__('The shipping label has been created.');

            $this->_getSession()->addSuccess($message);
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*');
    }

    /**
     * Mass print deposit action
     */
    public function massPrintDepositAction()
    {
        $shipmentIds = $this->getRequest()->getPost('shipment_ids', array());

        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $data = array(
            'shipments' => array(),
        );

        $totalWeight = 0;
        $store       = null;

        foreach ($shipmentIds as $shipmentId) {
            /** @var Mage_Sales_Model_Order_Shipment $shipment */
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);

            if (!$shipment->hasData()) {
                continue;
            }

            if (!$helper->isColissimoLabelAllowed($shipment->getOrder())) {
                continue;
            }

            /** @var Mage_Sales_Model_Order_Address $address */
            $address = $shipment->getShippingAddress();

            /** @var Mage_Sales_Model_Resource_Order_Shipment_Track_Collection $tracks */
            $tracks = Mage::getResourceModel('sales/order_shipment_track_collection');

            $tracks->setShipmentFilter($shipment->getId());
            $tracks->setOrder('created_at', 'DESC');
            $tracks->setPageSize(1);

            /** @var Mage_Sales_Model_Order_Shipment_Track $track */
            foreach ($tracks as $track) {
                $data['shipments'][] = array(
                    'increment_id' => $shipment->getOrder()->getIncrementId(),
                    'name' => $address->getFirstname() . ' ' . $address->getLastname(),
                    'tracking' => $track->getNumber(),
                    'postcode' => $address->getPostcode(),
                    'country' => $address->getCountryId(),
                    'weight' => floatval($shipment->getTotalWeight()),
                    'nm' => 0
                );
            }

            $totalWeight += $shipment->getTotalWeight();
            $store = $shipment->getStore();
        }

        $data['customer'] = array(
            'account_number'  => $helper->getAccountNumber($store),
            'commercial_name' => $helper->getCommercialName($store),
        );

        $data['site'] = array(
            'name'   => $helper->getSiteName($store),
            'number' => $helper->getSiteNumber($store),
        );

        $data['summary'] = array(
            'total_shipment' => count($data['shipments']),
            'total_weight'   => $totalWeight,
        );

        /** @var LaPoste_Label_Model_Deposit $deposit */
        $deposit = Mage::getModel('laposte_label/deposit');
        $content = $deposit->getDepositFile($data);

        $this->_prepareDownloadResponse($deposit->getFileName(), $content);
    }

    /**
     * Update Weight Action
     */
    public function saveGridDataAction()
    {
        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');

        $orderId = $this->getRequest()->getParam('order_id');
        $field   = $this->getRequest()->getParam('field');
        $value   = $this->getRequest()->getParam('value');

        if ($orderId && $field) {
            /* @var $model Mage_Sales_Model_Order */
            $model = Mage::getModel('sales/order');
            $model->load($orderId);

            $model->setData($field, $value)->save();

            $model->load($orderId);

            $this->getResponse()->setBody(
                $helper->jsonEncode(
                    array(
                        'value' => $model->getData($field),
                    )
                )
            );
        }
    }

    /**
     * Return Label Action
     */
    public function returnAction()
    {
        $orderId = $this->getRequest()->getParam('order_id', false);

        if (!$orderId) {
            $this->_redirect('*/*');
            return;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        if (!$order->hasData()) {
            $this->_redirect('*/*');
            return;
        }

        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /** @var Mage_Sales_Model_Resource_Order_Shipment_Collection $shipments */
        $shipments = $order->getShipmentsCollection();

        if (!$shipments->getSize()) {
            $this->_getSession()->addError($helper->__('Order %s has no shipment', $order->getIncrementId()));
            $this->_redirect('*/*');
            return;
        }

        Mage::register('return_order', $order);

        $this->_initAction()->renderLayout();
    }

    /**
     * Create Return Action
     */
    public function createReturnAction()
    {
        $params = $this->getRequest()->getParams();

        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($params['order_id']);

        if (!$order->hasData()) {
            $this->_redirect('*/*');
            return;
        }

        /** @var Mage_Sales_Model_Resource_Order_Shipment_Collection $shipments */
        $shipments = $order->getShipmentsCollection();

        if (!$shipments->getSize()) {
            $this->_getSession()->addError($helper->__('Order %s has no shipment', $order->getIncrementId()));
            $this->_redirect('*/*');
            return;
        }

        $data = array(
            'is_return'                           => true,
            'order_number'                        => $params['order_number'],
            'order_shipment'                      => $shipments->getFirstItem(),
            'product_code'                        => 'CORE',
            'output_printing_type'                => 'PDF_A4_300dpi',
            'service_info'                        => $params['recipient']['service_info'],
            'shipper_contact_company_name'        => $params['shipper']['company'],
            'shipper_contact_person_first_name'   => $params['shipper']['firstname'],
            'shipper_contact_person_last_name'    => $params['shipper']['lastname'],
            'shipper_contact_phone_number'        => $params['shipper']['telephone'],
            'shipper_address_street1'             => $params['shipper']['street_1'],
            'shipper_address_street2'             => $params['shipper']['street_2'],
            'shipper_address_street3'             => $params['shipper']['street_3'],
            'shipper_address_street4'             => '',
            'shipper_address_country_code'        => $params['shipper']['country_id'],
            'shipper_address_city'                => $params['shipper']['city'],
            'shipper_address_postal_code'         => $params['shipper']['postcode'],
            'shipper_email'                       => $params['shipper']['email'],
            'recipient_contact_company_name'      => $params['recipient']['company'],
            'recipient_contact_person_first_name' => $params['recipient']['firstname'],
            'recipient_contact_person_last_name'  => $params['recipient']['lastname'],
            'recipient_contact_phone_number'      => $params['recipient']['telephone'],
            'recipient_address_street1'           => $params['recipient']['street_1'],
            'recipient_address_street2'           => $params['recipient']['street_2'],
            'recipient_address_street3'           => $params['recipient']['street_3'],
            'recipient_address_street4'           => '',
            'recipient_address_country_code'      => $params['recipient']['country_id'],
            'recipient_address_city'              => $params['recipient']['city'],
            'recipient_address_postal_code'       => $params['recipient']['postcode'],
            'recipient_email'                     => $params['recipient']['email'],
        );

        if ($params['insurance'] == 1) {
            if ($params['insurance_value'] > 1500) {
                $params['insurance_value'] = 1500;
            }
            $data['insurance_value'] = $params['insurance_value'] * 100;
        }

        if ($this->getRequest()->getParam('save_address')) {
            $helper->saveReturnAddress($params['recipient'], $params['store_id']);
        }

        /** @var Mage_Shipping_Model_Shipment_Request $request */
        $request = Mage::getModel('shipping/shipment_request');
        $request->setData($data);

        /** @var LaPoste_Label_Model_Label $label */
        $label = Mage::getModel('laposte_label/label');
        $result = $label->doShipmentRequest($request);

        if ($result->getErrors()) {
            $this->_getSession()->addError($result->getErrors());
            $this->_redirect('*/*/return', array('order_id' => $params['order_id']));
            return;
        }

        $this->_getSession()->addSuccess($helper->__('Return label was created for order %s', $order->getIncrementId()));
        $this->_redirect('*/*');
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/colissimo/label');
    }

    /**
     * Retrieve Admin Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getAdminSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout();

        $this->_title(Mage::helper('laposte_label')->__('Colissimo'))
            ->_title(Mage::helper('laposte_label')->__('Labels'));

        $this->_setActiveMenu('sales');

        return $this;
    }

}