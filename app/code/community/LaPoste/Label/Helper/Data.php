<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Retrieve WSDL configuration
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return array
     */
    public function getWsdlConfig($store)
    {
        return array(
            'wsdl'     => 'https://ws.colissimo.fr/sls-ws/SlsServiceWS?wsdl',
            'login'    => $this->getAccountNumber($store),
            'password' => Mage::getStoreConfig('shipping/label/account_password', $store),
        );
    }

    /**
     * Update country code
     *
     * @param string $countryCode
     * @param string $postcode
     * @return string
     */
    public function getCountryCode($countryCode, $postcode)
    {
        if ($countryCode == 'FR') {
            if ($postcode == '98000') {
                $countryCode = 'MC';
            }
            if (preg_match('/^AD/', $postcode)) {
                $countryCode = 'AD';
            }
        }

        return $countryCode;
    }

    /**
     * Retrieve account number
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getAccountNumber($store = null)
    {
        return Mage::getStoreConfig('shipping/label/account_number', $store);
    }

    /**
     * Retrieve Commercial Name
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getCommercialName($store = null)
    {
        return Mage::getStoreConfig('shipping/label/commercial_name', $store);
    }

    /**
     * Retrieve Shipper Company
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getShipperCompany($store = null)
    {
        return Mage::getStoreConfig('shipping/label/shipper_company', $store);
    }

    /**
     * Retrieve deposit site name
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getSiteName($store = null)
    {
        return Mage::getStoreConfig('shipping/label/site_name', $store);
    }

    /**
     * Retrieve deposit site number
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getSiteNumber($store = null)
    {
        return Mage::getStoreConfig('shipping/label/site_number', $store);
    }

    /**
     * Retrieve weight rate conversion
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return int
     */
    public function getWeightRate($store = null)
    {
        return Mage::getStoreConfig('shipping/label/weight_rate', $store) ?: 1;
    }

    /**
     * Retrieve output printing type
     *
     * @return string
     */
    public function getOutputPrintingType()
    {
        return Mage::getStoreConfig('shipping/label/printing_type') ?: 'PDF_A4_300dpi';
    }

    /**
     * Retrieve if label and Shipping Slips must be merged
     *
     * @return bool
     */
    public function getMergeShippingSlips()
    {
        return Mage::getStoreConfigFlag('shipping/label/packingslips_merge');
    }

    /**
     * Retrieve if label auto-creation is enabled
     *
     * @return bool
     */
    public function isAutoCreate()
    {
        return Mage::getStoreConfigFlag('shipping/label/autocreate_label');
    }

    /**
     * Retrieve Import directory
     *
     * @return string
     */
    public function getLabelDir()
    {
        $directory = Mage::getBaseDir('var') . DS . 'export' . DS . 'labels' . DS;

        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        return $directory;
    }

    /**
     * Retrieve Return label file name
     *
     * @param string $name
     * @return string
     */
    public function getReturnFileName($name)
    {
        return $this->getLabelFileName($name, 'CORE', 'RETURN', 'pdf');
    }

    /**
     * Retrieve Label file
     *
     * @param string $name
     * @param string $type
     * @param string $suffix
     * @param string $extension
     * @return string
     */
    public function getLabelFileName($name, $type, $suffix = null, $extension = null)
    {
        return 'label_'
            . $name
            . '_'
            . $type
            . ($suffix ? '_' . $suffix : '')
            . '.'
            . ($extension ? $extension : $this->getExtension());
    }

    /**
     * Retrieve file extension
     */
    public function getExtension()
    {
        $type = $this->getOutputPrintingType();

        return strtolower(substr($type, 0, 3));
    }

    /**
     * Check if Colissimo module from Magentix is enabled
     *
     * @return bool
     */
    public function isColissimoEnabled()
    {
        return Mage::helper('core')->isModuleEnabled('LaPoste_Colissimo');
    }

    /**
     * Retrieve if order is allowed to print Colissimo Label
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function isColissimoLabelAllowed($order)
    {
        $allowed = $this->getFilterMethod();

        if (!$allowed) {
            return true;
        }

        $methods = array();

        if ($allowed) {
            $methods = explode(',', $allowed);
        }

        if (in_array($order->getShippingMethod(), $methods)) {
            return true;
        }

        return false;
    }

    /**
     * Retrieve Dom Tom Country with postcode
     *
     * @param string $postcode
     * @return bool
     */
    public function isDomTom($postcode)
    {
        $isDomTom = false;
        $postcodes = $this->getDomTomPostcodes();

        $postcode = preg_replace('/\s+/', '', $postcode);
        foreach ($postcodes as $code => $regex) {
            if (preg_match($regex, $postcode)) {
                $isDomTom = true;
                break;
            }
        }
        return $isDomTom;
    }

    /**
     * Retrieve Dom Tom Country with postcode
     *
     * @param string $postcode
     * @return string
     */
    public function getDomTomCountry($postcode)
    {
        $countryId = 'FR';
        $postcodes = $this->getDomTomPostcodes();

        $postcode = preg_replace('/\s+/', '', $postcode);
        foreach ($postcodes as $code => $regex) {
            if (preg_match($regex, $postcode)) {
                $countryId = $code;
                break;
            }
        }
        return $countryId;
    }

    /**
     * Retrieve Dom-Tom postcodes
     *
     * @return array
     */
    public function getDomTomPostcodes()
    {
        return array(
            'BL' => '/^97133$/', // Saint-Barthélemy
            'MF' => '/^97150$/', // Saint-Martin (partie française)
            'GP' => '/^971[0-9]{2}$/', // Guadeloupe
            'MQ' => '/^972[0-9]{2}$/', // Martinique
            'GF' => '/^973[0-9]{2}$/', // Guyane
            'RE' => '/^974[0-9]{2}$/', // La réunion
            'PM' => '/^975[0-9]{2}$/', // St-Pierre-et-Miquelon
            'YT' => '/^976[0-9]{2}$/', // Mayotte
            'TF' => '/^984[0-9]{2}$/', // Terres-Australes
            'WF' => '/^986[0-9]{2}$/', // Wallis-et-Futuna
            'PF' => '/^987[0-9]{2}$/', // Polynésie Française
            'NC' => '/^988[0-9]{2}$/', // Nouvelle-Calédonie
        );
    }

    /**
     * Retrieve link for HS code generation
     *
     * @return string
     */
    public function getHsCodeLink()
    {
        return 'https://pro.douane.gouv.fr/rita-encyclopedie/public/nomenclatures/';
    }

    /**
     * Retrieve order filter status
     *
     * @return string
     */
    public function getFilterMethod()
    {
        return Mage::getStoreConfig('shipping/label/methods');
    }

    /**
     * Retrieve order filter status
     *
     * @return string
     */
    public function getFilterStatus()
    {
        return Mage::getStoreConfig('shipping/label/order_status');
    }

    /**
     * Retrieve default Mobile Phone
     *
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getDefaultMobileNumber($store = null)
    {
        return Mage::getStoreConfig('shipping/label/default_mobile_number', $store);
    }

    /**
     * Matches product codes for WS
     *
     * @param string $productCode
     * @return string
     */
    public function getProductCode($productCode)
    {
        $matches = array(
            'ACP'  => 'BPR',
            'CDI'  => 'BPR',
            'COLD' => 'DOM',
            'COL'  => 'DOS',
        );

        if (isset($matches[$productCode])) {
            return $matches[$productCode];
        }

        return $productCode;
    }

    /**
     * Retrieve Mobile Number
     *
     * @param string $countryId
     * @param string $phoneNumber
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getMobileNumber($countryId, $phoneNumber, $store = null)
    {
        $phoneData = $this->getMobileCountryValidation($countryId);

        $phoneNumber = preg_replace('/\s+/', '', $phoneNumber);

        if (!preg_match($phoneData->getData('code_regex'), $phoneNumber)) {
            $phoneNumber = $phoneData->getData('phone_code') . ltrim($phoneNumber, 0);
        }

        if (!preg_match($phoneData->getData('phone_regex'), $phoneNumber)) {
            $phoneNumber = '';
            if ($countryId == 'FR') {
                $phoneNumber = $this->getDefaultMobileNumber($store);
            }
        }

        return $phoneNumber;
    }

    /**
     * Retrieve mobile phone configuration for delivery country
     *
     * @param string $countryId
     * @return Varien_Object
     */
    public function getMobileCountryValidation($countryId)
    {
        $countries = array(
            'FR' => array(
                'phone_regex'   => '/^\+33(6|7)[0-9]{8}$/',
                'code_regex'    => '/^\+33/',
                'phone_code'    => '+33',
            ),
            'BE' => array(
                'phone_regex' => '/^\+324[0-9]{8}$/',
                'code_regex'  => '/^\+32/',
                'phone_code'  => '+32',
            ),
            'NL' => array(
                'phone_regex' => '/^\+316[0-9]{8}$/',
                'code_regex'  => '/^\+31/',
                'phone_code'  => '+31',
            ),
            'DE' => array(
                'phone_regex' => '/^\+491[5-7]{1}[0-9]{7,9}$/',
                'code_regex'  => '/^\+49/',
                'phone_code'  => '+49',
            ),
            'GB' => array(
                'phone_regex' => '/^\+447[3-9]{1}[0-9]{8}$/',
                'code_regex'  => '/^\+44/',
                'phone_code'  => '+44',
            ),
            'LU' => array(
                'phone_regex' => '/^\+3526[0-9]{8}$/',
                'code_regex'  => '/^\+352/',
                'phone_code'  => '+352',
            ),
            'ES' => array(
                'phone_regex' => '/^\+346[0-9]{8}$/',
                'code_regex'  => '/^\+34/',
                'phone_code'  => '+34',
            ),
            'AT' => array(
                'phone_regex' => '/^\+43/',
                'code_regex'  => '/^\+43/',
                'phone_code'  => '+43',
            ),
            'EE' => array(
                'phone_regex' => '/^\+372/',
                'code_regex'  => '/^\+372/',
                'phone_code'  => '+372',
            ),
            'HU' => array(
                'phone_regex' => '/^\+36/',
                'code_regex'  => '/^\+36/',
                'phone_code'  => '+36',
            ),
            'LV' => array(
                'phone_regex' => '/^\+371/',
                'code_regex'  => '/^\+371/',
                'phone_code'  => '+371',
            ),
            'LT' => array(
                'phone_regex' => '/^\+370/',
                'code_regex'  => '/^\+370/',
                'phone_code'  => '+370',
            ),
            'CZ' => array(
                'phone_regex' => '/^\+420/',
                'code_regex'  => '/^\+420/',
                'phone_code'  => '+420',
            ),
            'SK' => array(
                'phone_regex' => '/^\+421/',
                'code_regex'  => '/^\+421/',
                'phone_code'  => '+421',
            ),
            'SI' => array(
                'phone_regex' => '/^\+386/',
                'code_regex'  => '/^\+386/',
                'phone_code'  => '+386',
            ),
            'CH' => array(
                'phone_regex' => '/^\+41/',
                'code_regex'  => '/^\+41/',
                'phone_code'  => '+41',
            ),
            'PT' => array(
                'phone_regex' => '/^\+351/',
                'code_regex'  => '/^\+351/',
                'phone_code'  => '+351',
            ),
            'BG' => array(
                'phone_regex' => '/^\+359/',
                'code_regex'  => '/^\+359/',
                'phone_code'  => '+359',
            ),
            'CY' => array(
                'phone_regex' => '/^\+357/',
                'code_regex'  => '/^\+357/',
                'phone_code'  => '+357',
            ),
            'HR' => array(
                'phone_regex' => '/^\+385/',
                'code_regex'  => '/^\+385/',
                'phone_code'  => '+385',
            ),
            'DK' => array(
                'phone_regex' => '/^\+45/',
                'code_regex'  => '/^\+45/',
                'phone_code'  => '+45',
            ),
            'FI' => array(
                'phone_regex' => '/^\+358/',
                'code_regex'  => '/^\+358/',
                'phone_code'  => '+358',
            ),
            'GR' => array(
                'phone_regex' => '/^\+30/',
                'code_regex'  => '/^\+30/',
                'phone_code'  => '+30',
            ),
            'IE' => array(
                'phone_regex' => '/^\+353/',
                'code_regex'  => '/^\+353/',
                'phone_code'  => '+353',
            ),
            'IS' => array(
                'phone_regex' => '/^\+354/',
                'code_regex'  => '/^\+354/',
                'phone_code'  => '+354',
            ),
            'IT' => array(
                'phone_regex' => '/^\+39/',
                'code_regex'  => '/^\+39/',
                'phone_code'  => '+39',
            ),
            'MT' => array(
                'phone_regex' => '/^\+356/',
                'code_regex'  => '/^\+356/',
                'phone_code'  => '+356',
            ),
            'NO' => array(
                'phone_regex' => '/^\+47/',
                'code_regex'  => '/^\+47/',
                'phone_code'  => '+47',
            ),
            'PL' => array(
                'phone_regex' => '/^\+48/',
                'code_regex'  => '/^\+48/',
                'phone_code'  => '+48',
            ),
            'SE' => array(
                'phone_regex' => '/^\+46/',
                'code_regex'  => '/^\+46/',
                'phone_code'  => '+46',
            ),
            'RO' => array(
                'phone_regex' => '/^\+40/',
                'code_regex'  => '/^\+40/',
                'phone_code'  => '+40',
            ),
        );

        if (!isset($countries[$countryId])) {
            $countryId = 'FR';
        }

        $data = $countries[$countryId];

        $object = new Varien_Object();
        $object->setData($data);

        Mage::dispatchEvent(
            'laposte_label_mobile_phone_validation',
            array(
                'country'    => $countryId,
                'validation' => $object
            )
        );

        return $object;
    }

    /**
     * Save return address in config
     *
     * @param array $address
     * @param int $storeId
     * @return bool
     */
    public function saveReturnAddress($address, $storeId)
    {
        foreach ($address as $field => $value) {
            Mage::getConfig()->saveConfig(
                'shipping/label/return/' . $field,
                $value,
                'stores',
                $storeId
            );
            Mage::getConfig()->cleanCache();
        }

        return true;
    }

    /**
     * Merge Pdf Files
     *
     * @param array $files
     * @return string
     */
    public function mergePdfFiles($files)
    {
        $merged = new Zend_Pdf();

        foreach ($files as $file) {
            $pdf = Zend_Pdf::parse($file);
            $extractor = new Zend_Pdf_Resource_Extractor();
            foreach ($pdf->pages as $page) {
                $pdfExtract = $extractor->clonePage($page);
                $merged->pages[] = $pdfExtract;
            }
        }

        return $merged->render();
    }
}