<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Model_Label extends Mage_Core_Model_Abstract
{

    /**
     * Generate Shipping label file
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return Varien_Object
     */
    public function doShipmentRequest($request)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $order = $request->getOrderShipment()->getOrder();

        if (!is_array($request->getPackages())) {
            $packages = $this->_initDefaultPackage($order);
            $request->setPackages($packages);
            $request->getOrderShipment()->setPackages($packages);
        }

        $params = $this->_getParams($request);

        /* Add Package Weight in Kg */
        $weight = $params->getWeight();
        if (!$weight) {
            $weight = $request->getPackageWeight();
        }
        if (!$weight) {
            $weight = $order->getWeight();
        }
        $request->setPackageWeight($this->_convertWeight($weight, $order->getStore()));
        $request->getOrderShipment()->setTotalWeight($request->getPackageWeight());

        /* Update postcodes */
        $request->setShipperAddressPostalCode(
            preg_replace('/\s+/', '', $request->getShipperAddressPostalCode())
        );
        $request->setRecipientAddressPostalCode(
            preg_replace('/\s+/', '', $request->getRecipientAddressPostalCode())
        );

        /* Update country */
        $request->setShipperAddressCountryCode(
            $helper->getCountryCode(
                $request->getShipperAddressCountryCode(),
                $request->getShipperAddressPostalCode()
            )
        );
        $request->setRecipientAddressCountryCode(
            $helper->getCountryCode(
                $request->getRecipientAddressCountryCode(),
                $request->getRecipientAddressPostalCode()
            )
        );

        /* Add address lines 3 and 4 */
        if ((int)Mage::getStoreConfig('customer/address/street_lines') >= 3) {
            $address = $order->getShippingAddress();

            $request->setRecipientAddressStreet3($address->getStreet(3));
            $request->setRecipientAddressStreet4($address->getStreet(4));
        }

        /* Retrieve correct mobile number */
        $request->setRecipientContactPhoneNumber(
            $helper->getMobileNumber(
                $request->getRecipientAddressCountryCode(),
                $request->getRecipientContactPhoneNumber(),
                $order->getStore()
            )
        );

        /* Add Specific data if empty */
        if (!$request->getCommercialName()) {
            $request->setCommercialName($helper->getCommercialName($order->getStore()));
        }

        if (!$request->getOrderNumber()) {
            $shipmentIncrementId = $request->getOrderShipment()->getIncrementId();
            $request->setOrderNumber(
                $order->getIncrementId() . ($shipmentIncrementId ? '_' . $shipmentIncrementId : '')
            );
        }

        if (!$request->getRecipientEmail()) {
            $request->setRecipientEmail($order->getCustomerEmail());
        }

        if ($helper->getShipperCompany($order->getStore())) {
            $customContact = $helper->getShipperCompany($order->getStore());
            $request->setShipperContactCompanyName($customContact);
            $request->setShipperContactPersonFirstName($customContact);
            $request->setShipperContactPersonLastName('');
        }

        /* Add CN23 */
        if ($this->_isCn23($request)) {
            $packageItems = $this->_getItems($request);

            $customsDeclaration = array(
                'includeCustomsDeclarations' => true,
                'contents' => array(
                    'article' => array(),
                    'category' => array('value' => 3),
                )
            );

            $totalWeight = 0;
            foreach ($packageItems as $item) {
                $itemWeight = $this->_convertWeight($item->getWeight(), $order->getStore());
                $customsDeclaration['contents']['article'][] = array(
                    'description'   => $item->getName(),
                    'quantity'      => (int)$item->getQty(),
                    'weight'        => $itemWeight,
                    'value'         => $item->getPrice() > 0 ? $item->getPrice() : 0.01,
                    'hsCode'        => $this->_getProductHsCode($item->getProductId()),
                    'originCountry' => 'FR',
                );
                $totalWeight = $totalWeight + ((int)$item->getQty() * floatval($itemWeight));
            }
            $totalWeight = number_format($totalWeight, 2, '.', '');

            $request->setPackageWeight($totalWeight);
            $request->getOrderShipment()->setTotalWeight($totalWeight);

            $request->setCustomsDeclarations($customsDeclaration);

            $totalShippingAmount = (int)($order->getShippingAmount() * 100);
            $request->setTotalAmount($totalShippingAmount ?: 100);

            if ($helper->isDomTom($request->getRecipientAddressPostalCode())) {
                $request->setRecipientAddressCountryCode(
                    $helper->getDomTomCountry($request->getRecipientAddressPostalCode())
                );
            }
        }

        if ($helper->isColissimoEnabled() && !$request->getIsReturn()) {
            $address = $order->getShippingAddress();

            if (!$request->getProductCode()) {
                $request->setProductCode(
                    $helper->getProductCode($address->getColissimoProductCode())
                );
            }

            if (!$request->getPickupLocationId()) {
                $request->setPickupLocationId($address->getColissimoPickupId());
            }

            if (!$request->getInsuranceValue()) {
                if ($params->getInsurance() && !$request->getIsReturn()) {
                    $insuranceValue = intval($order->getSubtotal() * 100);
                    if ($insuranceValue > 150000) {
                        $insuranceValue = 150000;
                    }
                    $request->setInsuranceValue($insuranceValue);
                }
            }

            if ($address->getColissimoNetworkCode() == 'X00') {
                $request->setFields(
                    array(
                        'PUDO_NETWORK_CODE'       => $address->getColissimoNetworkCode(),
                        'PUDO_POINT_NAME'         => $address->getCompany(),
                        'PUDO_POINT_ADDRESS_1'    => $address->getStreet1(),
                        'PUDO_POINT_ADDRESS_2'    => $address->getStreet2(),
                        'PUDO_POINT_TOWN'         => $address->getCity(),
                        'PUDO_POINT_ZIP_CODE'     => $address->getPostcode(),
                        'PUDO_POINT_COUNTRY_CODE' => $address->getCountryId(),
                        'CUSTOMER_ACCOUNT_NUMBER' => $helper->getAccountNumber($order->getStore()),
                    )
                );
            }
        }

        Mage::dispatchEvent('laposte_label_do_shipment_before', array('request'  => $request));

        $pdf = $this->_generateLabel($request);

        if (!$pdf['label']) {
            $label = array(
                'errors' => $helper->__('Order: %s', $order->getIncrementId()) . ' - ' .$pdf['message']
            );
        } else {
            $label = array(
                'info' => array(
                    array(
                        'tracking_number' => $pdf['tracking'],
                        'label_file'      => $pdf['label'],
                    )
                )
            );
        }

        $result = new Varien_Object($label);

        Mage::dispatchEvent('laposte_label_do_shipment_after', array('result'  => $result, 'request' => $request));

        return $result;
    }

    /**
     * Retrieve label data with WS call
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return array
     */
    protected function _generateLabel($request)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        if (!$request->getOrderNumber() || !$request->getProductCode()) {
            return array(
                'label'    => false,
                'message'  => $helper->__('Empty order number or product code'),
                'tracking' => false,
            );
        }

        $config = $helper->getWsdlConfig($request->getOrderShipment()->getStore());

        if (!$config['login'] || !$config['password']) {
            return array(
                'label'    => false,
                'message'  => $helper->__('Empty Web Service login or password'),
                'tracking' => false,
            );
        }

        $client = new LaPoste_Label_Model_Soap($config['wsdl'], array('wsdl_cache' => 0));

        $client->setOrderNumber($request->getOrderNumber());
        $client->setProductCode($request->getProductCode());
        $client->setIsReturn($request->getIsReturn());

        $shipping = array(
            'generateLabelRequest' => array(
                'contractNumber' => $config['login'],
                'password'       => $config['password'],
                'outputFormat'   => array(
                    'x'                  => 0,
                    'y'                  => 0,
                    'outputPrintingType' => $request->getOutputPrintingType() ?: $helper->getOutputPrintingType(),
                    'returnType'         => '',
                ),
                'letter'         => array(
                    'service'   => array(
                        'productCode'          => $request->getProductCode(),
                        'depositDate'          => date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' +1 days')),
                        'mailBoxPicking'       => false,
                        'transportationAmount' => 0,
                        'totalAmount'          => $request->getTotalAmount(),
                        'orderNumber'          => $request->getOrderNumber(),
                        'commercialName'       => $request->getCommercialName(),
                        'returnTypeChoice'     => 2,
                    ),
                    'parcel'    => array(
                        'insuranceValue'      => $request->getInsuranceValue() ?: 0,
                        'recommendationLevel' => $request->getRecommendationLevel(),
                        'weight'              => $request->getPackageWeight(),
                        'nonMachinable'       => false,
                        'COD'                 => false,
                        'CODAmount'           => 0,
                        'returnReceipt'       => 0,
                        'instructions'        => '',
                        'pickupLocationId'    => $request->getPickupLocationId(),
                        'ftd'                 => 0,
                    ),
                    'sender'    => array(
                        'senderParcelRef' => $request->getOrderNumber(),
                        'address'         => array(
                            'companyName'  => $request->getShipperContactCompanyName(),
                            'firstName'    => $request->getShipperContactPersonFirstName(),
                            'lastName'     => $request->getShipperContactPersonLastName(),
                            'line0'        => $request->getShipperAddressStreet2(),
                            'line1'        => $request->getShipperAddressStreet3(),
                            'line2'        => $request->getShipperAddressStreet1(),
                            'line3'        => $request->getShipperAddressStreet4(),
                            'countryCode'  => $request->getShipperAddressCountryCode(),
                            'city'         => $request->getShipperAddressCity(),
                            'zipCode'      => $request->getShipperAddressPostalCode(),
                            'phoneNumber'  => $request->getShipperContactPhoneNumber(),
                            'mobileNumber' => $request->getShipperContactPhoneNumber(),
                            'doorCode1'    => $request->getShipperAddressDoorCode1(),
                            'doorCode2'    => $request->getShipperAddressDoorCode2(),
                            'intercom'     => $request->getShipperAddressIntercom(),
                            'email'        => $request->getShipperEmail(),
                            'language'     => $request->getShipperAddressCountryCode(),
                        ),
                    ),
                    'addressee' => array(
                        'addresseeParcelRef' => $request->getOrderNumber(),
                        'codeBarForReference' => true,
                        'serviceInfo'         => $request->getServiceInfo(),
                        'address'             => array(
                            'companyName'  => $request->getRecipientContactCompanyName(),
                            'firstName'    => $request->getRecipientContactPersonFirstName(),
                            'lastName'     => $request->getRecipientContactPersonLastName(),
                            'line0'        => $request->getRecipientAddressStreet2(),
                            'line1'        => $request->getRecipientAddressStreet3(),
                            'line2'        => $request->getRecipientAddressStreet1(),
                            'line3'        => $request->getRecipientAddressStreet4(),
                            'countryCode'  => $request->getRecipientAddressCountryCode(),
                            'city'         => $request->getRecipientAddressCity(),
                            'zipCode'      => $request->getRecipientAddressPostalCode(),
                            'phoneNumber'  => $request->getRecipientContactPhoneNumber(),
                            'mobileNumber' => $request->getRecipientContactPhoneNumber(),
                            'doorCode1'    => $request->getRecipientAddressDoorCode1(),
                            'doorCode2'    => $request->getRecipientAddressDoorCode2(),
                            'intercom'     => $request->getRecipientAddressIntercom(),
                            'email'        => $request->getRecipientEmail(),
                            'language'     => $request->getRecipientAddressCountryCode(),
                        ),
                    ),

                ),
            )
        );

        if ($request->getCustomsDeclarations()) { // CN23
            $shipping['generateLabelRequest']['letter']['customsDeclarations'] = $request->getCustomsDeclarations();
        }

        if ($request->getFields()) { // International Pickup X00
            $shipping['generateLabelRequest']['fields'] = array('field' => array());
            foreach ($request->getFields() as $field => $value) {
                $shipping['generateLabelRequest']['fields']['field'][] = array(
                    'key'   => $field,
                    'value' => $value,
                );
            }
        }

        try {
            $result = $client->generateLabel($shipping);

            if ($result->return->messages->type !== "SUCCESS") {
                $messages = $result->return->messages;
                $message  = '';
                if (is_array($messages)) {
                    foreach ($messages as $info) {
                        $message .= $info->messageContent . '. ';
                    }
                } else {
                    $message = $result->return->messages->messageContent;
                }

                return array(
                    'label'    => false,
                    'message'  => $message,
                    'tracking' => false,
                );
            }

            return array(
                'label'    => $result->return->messages->messageContent,
                'message'  => false,
                'tracking' => $result->return->labelResponse->parcelNumber,
            );
        } catch (Exception $e) {
            return array(
                'label'    => false,
                'message'  => $e->getMessage(),
                'tracking' => false,
            );
        }
    }

    /**
     * Retrieve request params
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return Varien_Object
     */
    protected function _getParams($request)
    {
        $params = new Varien_Object();

        if (is_array($request->getPackages())) {
            $packages = $request->getPackages();
            foreach ($packages as $package) {
                $params->setData('insurance', $package['params']['container'] === 'insurance' ? true : false);
                $params->setData('weight', ($params->getWeight() ?: 0) + $package['params']['weight']);
            }
        }

        return $params;
    }

    /**
     * Retrieve request items
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return Varien_Data_Collection
     */
    protected function _getItems($request)
    {
        $items = new Varien_Data_Collection();

        if (is_array($request->getPackages())) {
            $packages = $request->getPackages();
            foreach ($packages as $package) {
                foreach ($package['items'] as $data) {
                    $item = new Varien_Object();
                    $item->setData($data);
                    $items->addItem($item);
                }
            }
        }

        return $items;
    }

    /**
     * Convert weight in KG
     *
     * @param float $weight
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return string
     */
    protected function _convertWeight($weight, $store)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $weight = $weight * $helper->getWeightRate($store) / 1000;

        if ($weight < 0.01) {
            $weight = 0.01;
        }

        return number_format($weight, 2, '.', '');
    }

    /**
     * Retrieve is shipping has CN23
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return bool
     */
    protected function _isCn23($request)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $isCn23 = false;

        if ($request->getRecipientAddressCountryCode() !== 'FR') {
            $isCn23 = true;
        }

        if ($request->getRecipientAddressCountryCode() == 'FR' &&
            $helper->isDomTom($request->getRecipientAddressPostalCode()))
        {
            $isCn23 = true;
        }

        return $isCn23;
    }

    /**
     * Retrieve product hs code attribute
     *
     * @param int $productId
     * @return string
     */
    protected function _getProductHsCode($productId)
    {
        /** @var Mage_Catalog_Model_Resource_Product $product */
        $resource = Mage::getResourceModel('catalog/product');

        $value = $resource->getAttributeRawValue($productId, 'product_hs_code', 0);

        if ($value) {
            $value = preg_replace('/\s+/', '', $value);
        }

        return $value;
    }

    /**
     * Init default request if request is empty
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _initDefaultPackage($order)
    {
        $items = array();

        foreach ($order->getAllVisibleItems() as $item) {
            $qty = (int)$item->getQtyOrdered() - (int)$item->getQtyCanceled();

            if ($qty) {
                $items[] = array(
                    'qty'           => $qty,
                    'customs_value' => '',
                    'name'          => $item->getName(),
                    'weight'        => $item->getWeight(),
                    'product_id'    => $item->getProductId(),
                    'order_item_id' => $item->getParentItemId(),
                    'price'         => $item->getPrice(),
                );
            }
        }

        $packages = array(
            1 => array(
                'params' => array(
                    'container'          => $order->getColissimoInsurance() ? 'insurance' : '',
                    'weight'             => $order->getWeight(),
                    'customs_value'      => '',
                    'length'             => '',
                    'width'              => '',
                    'height'             => '',
                    'weight_units'       => 'KILOGRAM',
                    'dimension_units'    => 'CENTIMETER',
                    'content_type'       => '',
                    'content_type_other' => '',
                ),
                'items' => $items,
            )
        );

        return $packages;
    }

}