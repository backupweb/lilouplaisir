<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/**
 * With WS, SOAP response is MTOM, with PDF file in attachment.
 * Basic SoapClient can not handle this response (XML error).
 */
class LaPoste_Label_Model_Soap extends SoapClient
{

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var string $productCode
     */
    protected $productCode = null;

    /**
     * @var bool $isReturn
     */
    protected $isReturn    = false;

    /**
     * Performs a SOAP request
     *
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param int $one_way [optional]
     * @return string The XML SOAP response.
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $response = parent::__doRequest($request, $location, $action, $version, $one_way);

        if ($this->isMtom($response)) {
            return $this->saveFile($response);
        }

        return $response;
    }

    /**
     * Set Order Number
     *
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * Set Product Code
     *
     * @param string $productCode
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
    }

    /**
     * Set Is Return
     *
     * @param bool $return
     */
    public function setIsReturn($return)
    {
        $this->isReturn = $return;
    }

    /**
     * Check if soap response is xop+xml
     *
     * @param string $response
     *
     * @return mixed
     */
    private function isMtom($response)
    {
        return strpos($response, "Content-Type: application/xop+xml") !== false;
    }

    /**
     * Save PDF file from response
     *
     * @param string $response
     *
     * @return mixed
     */
    private function saveFile($response)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $xopSpilt  = substr($response, 0, stripos($response, "\r\n"));
        $responses = explode($xopSpilt, $response);

        if (!isset($responses[1]) && !isset($responses[2])) {
            return $this->getSoapEnvelope($helper->__('SOAP response invalid'), 'ERROR');
        }

        $message = $this->extractResponse($responses[1]);

        if ($responses[2] == '--') {
            return $message;
        }

        $files = array();

        $label = $this->extractResponse($responses[2]); // Label
        $fileName = $helper->getLabelFileName($this->orderNumber, $this->productCode);
        if ($this->isReturn) {
            $fileName = $helper->getReturnFileName($this->orderNumber);
        }
        file_put_contents($helper->getLabelDir() . $fileName, trim($label));
        $files[] = $fileName;

        if (isset($responses[3])) { // CN23
            if ($responses[3] !== '--') {
                $cn23 = $this->extractResponse($responses[3]);
                $fileName = $helper->getLabelFileName($this->orderNumber, $this->productCode, 'CN23', 'pdf');
                file_put_contents($helper->getLabelDir() . $fileName, trim($cn23));
                $files[] = $fileName;
            }
        }

        return $this->getSoapEnvelope(
            join(';', $files),
            'SUCCESS',
            $this->extractNode('parcelNumber', $message)
        );
    }

    /**
     * Extract response
     *
     * @param string $data
     * @return string
     */
    private function extractResponse($data)
    {
        return preg_replace("/^(.*\n){4}/", "", $data);
    }

    /**
     * Extract response node
     *
     * @param string $node
     * @param string $data
     * @return string
     */
    private function extractNode($node, $data)
    {
        $result = '';

        if (preg_match('/<' . $node . '>(?P<result>.*)<\/' . $node . '>/', $data, $match)) {
            $result = $match['result'];
        }

        return $result;
    }

    /**
     * Retrieve PDF success envelope
     *
     * @param string $message
     * @param string $type
     * @param string $tracking
     * @return string
     */
    private function getSoapEnvelope($message, $type, $tracking = '')
    {
        return '<?xml version="1.0"?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
                <soap:Header/>
                <soap:Body>
                    <ns2:generateLabelResponse xmlns:ns2="http://sls.ws.coliposte.fr">
                        <return>
                            <messages>
                                <messageContent>' . $message . '</messageContent>
                                <type>' . $type . '</type>
                            </messages>
                            <labelResponse>
                                <parcelNumber>' . $tracking . '</parcelNumber>
                            </labelResponse>
                        </return>
                    </ns2:generateLabelResponse>
                </soap:Body>
            </soap:Envelope>';
    }
}