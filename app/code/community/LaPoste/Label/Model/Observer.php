<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Model_Observer
{

    /**
     * Update insurance for order
     *
     * @param Varien_Event_Observer $observer
     */
    public function updateInsurance(Varien_Event_Observer $observer)
    {
        /** @var Mage_Shipping_Model_Shipment_Request $request */
        $request = $observer->getEvent()->getRequest();

        $value = 0;
        if ($request->getInsuranceValue()) {
            $value = 1;
        }

        $request->getOrderShipment()->getOrder()->setColissimoInsurance($value);
    }

    /**
     * Add print shipping label button to order view
     *
     * @param Varien_Event_Observer $observer
     */
    public function addOrderButton(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::registry('sales_order');

            if ($order->hasShipments()) {
                /** @var Mage_Adminhtml_Helper_Data $helper */
                $helper = Mage::helper('adminhtml');

                foreach ($order->getShipmentsCollection() as $shipment) {
                    if (!$shipment->getData('shipping_label_file')) {
                        continue;
                    }

                    $url = $helper->getUrl(
                        'adminhtml/sales_order_shipment/printLabel',
                        array(
                            '_query' => array('shipment_id' => $shipment->getId())
                        )
                    );
                    $block->addButton('print_shipping_label_' . $shipment->getId(), array(
                        'label' => Mage::helper('laposte_label')->__('Print label #%s', $shipment->getIncrementId()),
                        'onclick' => "window.location = '" . $url . "'",
                        'class' => 'go'
                    ));
                }
            }
        }
    }

    /**
     * Automatic label creation on shipment
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function createLabel(Varien_Event_Observer $observer)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        if (!$helper->isAutoCreate()) {
            return $this;
        }

        /* @var $shipment Mage_Sales_Model_Order_Shipment */
        $shipment = $observer->getEvent()->getShipment();

        if ($shipment->getData('label_auto_create_processing')) {
            return $this;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $shipment->getOrder();

        $method = $order->getShippingMethod(true);

        if ($method->getMethod() !== 'colissimo') {
            return $this;
        }

        if ($shipment->getData('shipping_label_file')) {
            return $this;
        }

        $shipment->setData('label_auto_create_processing', true);

        $carrier = $order->getShippingCarrier();

        /** @var Mage_Shipping_Model_Shipping $shipping */
        $shipping = Mage::getModel('shipping/shipping');

        if ($carrier->isShippingLabelsAvailable()) {
            $response = $shipping->requestToShipment($shipment);
        } else if ($helper->isColissimoLabelAllowed($order)) {
            /** @var LaPoste_Label_Model_Label $label */
            $label = Mage::getModel('laposte_label/label');
            /** @var $request LaPoste_Label_Model_Shipment_Request */
            $request = Mage::getModel('laposte_label/shipment_request');
            $response = $label->doShipmentRequest($request->requestToShipment($shipment));
        } else {
            return $this;
        }

        if ($response->hasErrors()) {
            $comment = $helper->__('Label auto creation error: %s', $response->getErrors());
            $order->addStatusHistoryComment($comment);
            $order->save();
            return $this;
        }
        if (!$response->hasInfo()) {
            return $this;
        }

        $trackingNumbers = array();
        $labelsFile      = array();
        $info            = $response->getInfo();

        foreach ($info as $inf) {
            if (!empty($inf['tracking_number'])) {
                $trackingNumbers[] = $inf['tracking_number'];
            }
            if (!empty($inf['label_file'])) {
                $labelsFile[] = $inf['label_file'];
            }
        }

        if (!empty($labelsFile)) {
            $shipment->setData('shipping_label_file', join(';', $labelsFile));
        }
        $carrierCode  = $carrier->getCarrierCode();
        $carrierTitle = Mage::getStoreConfig('carriers/' . $carrierCode . '/title', $shipment->getStoreId());

        if (!empty($trackingNumbers)) {
            foreach ($trackingNumbers as $trackingNumber) {
                $track = Mage::getModel('sales/order_shipment_track')
                    ->setNumber($trackingNumber)
                    ->setCarrierCode($carrierCode)
                    ->setTitle($carrierTitle);
                $shipment->addTrack($track);
            }
        }

        $shipment->save();

        return $this;
    }
}
