<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Model_Sales_Order_Shipment extends Mage_Sales_Model_Order_Shipment
{

    /**
     * Get shipping label and decode by db adapter
     *
     * @override: get label file if label content is empty
     *
     * @return string
     */
    public function getShippingLabel()
    {
        $label = parent::getShippingLabel();

        if (!$label) {
            /** @var LaPoste_Label_Helper_Data $helper */
            $helper = Mage::helper('laposte_label');

            $file = $this->getData('shipping_label_file');

            if ($file) {
                $files = explode(';', $file);
                if (isset($files[0])) {
                    $label = $helper->getLabelDir() . $files[0];
                }
            }
        }

        return $label;
    }

    /**
     * Get CN23 file
     *
     * New method
     *
     * @return string
     */
    public function getShippingCn23()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $file = $this->getData('shipping_label_file');
        $cn23 = '';

        if ($file) {
            $files = explode(';', $file);
            if (isset($files[1])) {
                $cn23 = $helper->getLabelDir() . $files[1];
            }
        }

        return $cn23;
    }
}