<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Model_Resource_Shipment extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('sales/shipment', 'entity_id');
    }

    /**
     * Retrieve order ids by shipment ids
     *
     * @param array $shipmentIds
     * @return array
     */
    public function getOrderIdsByShipmentIds($shipmentIds)
    {
        $read = $this->getReadConnection();

        return $read->fetchCol(
            $read->select()
                ->from($this->getMainTable(), array('order_id'))
                ->where('entity_id IN (?)', $shipmentIds)
        );
    }
}