<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Model_System_Printing
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'PDF_10x15_300dpi', 'label' => 'PDF_10x15_300dpi'),
            array('value' => 'PDF_A4_300dpi',    'label' => 'PDF_A4_300dpi'),
            array('value' => 'ZPL_10x15_203dpi', 'label' => 'ZPL_10x15_203dpi'),
            array('value' => 'ZPL_10x15_300dpi', 'label' => 'ZPL_10x15_300dpi'),
            array('value' => 'DPL_10x15_203dpi', 'label' => 'DPL_10x15_203dpi'),
            array('value' => 'DPL_10x15_300dpi', 'label' => 'DPL_10x15_300dpi'),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'PDF_10x15_300dpi' => 'PDF_10x15_300dpi',
            'PDF_A4_300dpi'    => 'PDF_A4_300dpi',
            'ZPL_10x15_203dpi' => 'ZPL_10x15_203dpi',
            'ZPL_10x15_300dpi' => 'ZPL_10x15_300dpi',
            'DPL_10x15_203dpi' => 'DPL_10x15_203dpi',
            'DPL_10x15_300dpi' => 'DPL_10x15_300dpi',
        );
    }

}
