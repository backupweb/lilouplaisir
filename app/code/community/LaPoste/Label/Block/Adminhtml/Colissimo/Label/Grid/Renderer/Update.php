<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Label_Grid_Renderer_Update
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('laposte/label/renderer/update.phtml');
    }

    /**
     * Render HTML
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $this->setOrderId($row->getOrderId());
        $this->setValue($this->_getValue($row));
        $this->setCanUpdate(!$row->getShippingLabelFile() ? true : false);
        $this->setFieldName($this->getColumn()->getIndex());
        $this->setInput($this->getColumn()->getInput());

        return $this->toHtml();
    }

}