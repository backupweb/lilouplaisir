<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Label
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_colissimo_label';
        $this->_blockGroup = 'laposte_label';
        $this->_headerText = Mage::helper('laposte_label')->__('Labels');

        parent::__construct();

        $this->removeButton('add');
    }

}