<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Return_Create_Tabs_Insurance
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::registry('return_order');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('Insurance')
            )
        );

        $fieldset->addField(
            'insurance', 'select', array(
                'name'  => 'insurance',
                'label' => $helper->__('Ad Valorem Insurance'),
                'values' => [
                    [
                        'label' => $helper->__('No'),
                        'value' => 0
                    ],
                    [
                        'label' => $helper->__('Yes'),
                        'value' => 1
                    ]
                ]
            )
        );

        $fieldset->addField(
            'insurance_value', 'text', array(
                'name'  => 'insurance_value',
                'label' => $helper->__('Insurance Amount'),
                'after_element_html' => '<small>' . $helper->__('Maximum Amount: 1500€') . '</small>',
                'class' => 'validate-zero-or-greater'
            )
        );

        $form->setValues(['insurance_value' => $order->getSubtotal(), 'insurance' => 0]);
        $this->setForm($form);

        $this->setChild('form_after', $this->getLayout()
            ->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap('insurance', 'insurance')
            ->addFieldMap('insurance_value', 'insurance_value')
            ->addFieldDependence('insurance_value', 'insurance', 1)
        );

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_label')->__('Insurance');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_label')->__('Insurance');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}