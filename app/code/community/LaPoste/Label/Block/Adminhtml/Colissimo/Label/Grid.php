<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Label_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('label_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('desc');
    }

    /**
     * Prepare Collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /* @var $collection Mage_Sales_Model_Resource_Order_Collection */
        $collection = Mage::getModel('sales/order')->getCollection();

        $collection->addExpressionFieldToSelect(
            'entity_id',
            'IF (shipping_label.order_id, CONCAT(main_table.entity_id, "-", shipping_label.entity_id), main_table.entity_id)',
            'entity_id'
        );

        $collection->addExpressionFieldToSelect(
            'order_id',
            'main_table.entity_id',
            'order_id'
        );

        if (count($this->_getStatusFilter())) {
            $collection->addFieldToFilter('status', array('in' => $this->_getStatusFilter()));
        }

        if (count($this->_getMethodFilter())) {
            $collection->addFieldToFilter('shipping_method',  array('in' => $this->_getMethodFilter()));
        }

        $columns = array(
            'country_id',
        );

        if ($helper->isColissimoEnabled()) {
            $columns[] = 'colissimo_product_code';
            $columns[] = 'colissimo_pickup_id';
            $columns[] = 'colissimo_network_code';
        }

        $collection->join(
            array('shipping_address' => 'sales/order_address'),
            'main_table.entity_id = shipping_address.parent_id AND shipping_address.address_type = "shipping"',
            $columns
        );

        $collection->getSelect()->joinLeft(
            array('shipping_label' => $collection->getTable('sales/shipment')),
            'main_table.entity_id = shipping_label.order_id',
            array(
                'shipping_label_file' => 'shipping_label_file',
                'shipment_id'         => 'entity_id',
                'shipment_created_at' => 'created_at',
            )
        );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $this->setColumnRenderers(
            array(
                'label_update'        => 'laposte_label/adminhtml_colissimo_label_grid_renderer_update',
                'massaction_extended' => 'laposte_label/adminhtml_colissimo_label_grid_widget_massaction',
            )
        );

        $this->addColumn('created_at', array(
                'header'   => $helper->__('Order Date'),
                'index'    => 'created_at',
                'type'     => 'date',
                'filter_index' => 'main_table.created_at',
            )
        );

        $this->addColumn('shipment_created_at', array(
                'header'   => $helper->__('Shipment Date'),
                'index'    => 'shipment_created_at',
                'type'     => 'date',
                'filter_index' => 'shipping_label.created_at',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                    'header'        => $helper->__('Store View'),
                    'index'         => 'store_id',
                    'type'          => 'store',
                    'store_all'     => true,
                    'store_view'    => true,
                    'sortable'      => false,
                    'skipEmptyStoresLabel' => true,
                    'width'         => '200px',
                    'filter_index'  => 'main_table.store_id',
                    'filter_condition_callback' => array($this, '_filterStoreCondition'),
                )
            );
        }

        $this->addColumn('increment_id', array(
                'header'   => $helper->__('Order'),
                'index'    => 'increment_id',
                'filter_index' => 'main_table.increment_id',
            )
        );

        /* @var $statuses Mage_Sales_Model_Resource_Order_Status_Collection */
        $statuses = Mage::getResourceModel('sales/order_status_collection');

        $this->addColumn('status', array(
                'header'   => $helper->__('Status'),
                'index'    => 'status',
                'type'     => 'options',
                'width'    => '70px',
                'options'  => $statuses->toOptionHash(),
            )
        );

        $this->addColumn('shipping_description', array(
                'header'   => $helper->__('Method'),
                'index'    => 'shipping_description',
            )
        );

        if ($helper->isColissimoEnabled()) {

            $this->addColumn('colissimo_product_code', array(
                    'header'  => $helper->__('Code'),
                    'index'   => 'colissimo_product_code',
                    'filter'  => false,
                )
            );

            $this->addColumn('colissimo_pickup_id', array(
                    'header' => $helper->__('Pickup'),
                    'index' => 'colissimo_pickup_id',
                    'filter' => false,
                )
            );

            $this->addColumn('colissimo_network_code', array(
                    'header' => $helper->__('Network'),
                    'index' => 'colissimo_network_code',
                    'filter' => false,
                )
            );

        }

        $this->addColumn('country_id', array(
                'header'   => $helper->__('Country'),
                'index'    => 'country_id',
                'type'     => 'country',
            )
        );

        $this->addColumn('weight', array(
                'header'   => $helper->__('Weight'),
                'index'    => 'weight',
                'type'     => 'label_update',
                'input'    => 'text',
                'filter'   => false,
            )
        );

        $this->addColumn('colissimo_insurance', array(
                'header'   => $helper->__('Insurance'),
                'index'    => 'colissimo_insurance',
                'type'     => 'label_update',
                'input'    => 'checkbox',
                'filter'   => false,
            )
        );

        $this->addColumn('customer_note_notify', array(
                'header'   => $helper->__('Notify Customer'),
                'index'    => 'customer_note_notify',
                'type'     => 'label_update',
                'input'    => 'checkbox',
                'filter'   => false,
            )
        );

        $this->addColumn(
            'shipping_label_status',
            array(
                'header'         => $helper->__('Label'),
                'align'          => 'left',
                'index'          => 'shipping_label_file',
                'width'          => '150px',
                'frame_callback' => array($this, 'decorateShippingLabelStatus'),
                'filter'         => false,
            )
        );

        $this->addColumn(
            'shipping_label_cn23',
            array(
                'header'         => $helper->__('CN23 (Customs)'),
                'align'          => 'left',
                'index'          => 'shipping_label_file',
                'width'          => '80px',
                'frame_callback' => array($this, 'decorateShippingLabelCn23'),
                'filter'         => false,
                'sortable'       => false,
            )
        );

        $this->addColumn(
            'shipping_label_return',
            array(
                'header'         => $helper->__('Return'),
                'align'          => 'left',
                'index'          => 'shipping_label_return',
                'width'          => '80px',
                'frame_callback' => array($this, 'decorateShippingLabelReturn'),
                'filter'         => false,
                'sortable'       => false,
            )
        );

        $this->addColumn('action',
            array(
                'header'    => $helper->__('Action'),
                'type'      => 'action',
                'getter'    => 'getOrderId',
                'actions'   => array(
                    array(
                        'caption' => $helper->__('View order'),
                        'url'     => array('base' => '*/sales_order/view'),
                        'field'   => 'order_id',
                        'popup'   => 1
                    ),
                    array(
                        'caption' => $helper->__('Return Label'),
                        'url'     => array('base' => '*/*/return'),
                        'field'   => 'order_id',
                    ),
                ),
                'filter'    => false,
                'sortable'  => false,
                'width'     => '100px',
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Retrieve Row Url
     *
     * @param object $row
     *
     * @return bool
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Decorate shipping label column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     *
     * @return string
     */
    public function decorateShippingLabelStatus($value, $row, $column, $isExport)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $url = $this->getUrl('*/*/generateLabel',
            array(
                'order_id'    => $row->getOrderId(),
                'shipment_id' => $row->getShipmentId()
            )
        );

        $files = explode(';', $row->getShippingLabelFile());
        $file = '';
        if (isset($files[0])) {
            $file = $files[0];
        }

        if (is_file($helper->getLabelDir() . $file)) {
            $class = 'grid-severity-notice';
            $value = $helper->__('Print label');
            $url   = $this->getUrl('*/*/printLabel', array('shipment_id' => $row->getShipmentId(), 'type' => 'label'));
        } else if ($row->getShippingLabelFile()) {
            $class = 'grid-severity-critical';
            $value = $helper->__('Create new label');
        } else if ($row->getShipmentId()) {
            $class = 'grid-severity-critical';
            $value = $helper->__('Create label');
        } else {
            $class = 'grid-severity-critical';
            $value = $helper->__('Create Shipment & label');
        }

        return '<span class="' . $class . '"><span><a href="' . $url . '">' . $value . '</a></span></span>';
    }

    /**
     * Decorate shipping label column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     *
     * @return string
     */
    public function decorateShippingLabelCn23($value, $row, $column, $isExport)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $files = explode(';', $row->getShippingLabelFile());
        $file = '';
        if (isset($files[1])) {
            $file = $files[1];
        }

        if (is_file($helper->getLabelDir() . $file)) {
            $value = $helper->__('Print');
            $url   = $this->getUrl('*/*/printLabel', array('shipment_id' => $row->getShipmentId(), 'type' => 'cn23'));

            return '<span class="grid-severity-notice"><span><a href="' . $url . '">' . $value . '</a></span></span>';
        }

        return '<span class="grid-severity-minor"><span>' . $helper->__('None') . '</span></span>';
    }

    /**
     * Decorate shipping label column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     *
     * @return string
     */
    public function decorateShippingLabelReturn($value, $row, $column, $isExport)
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $file = $helper->getReturnFileName($row->getIncrementId());

        if (is_file($helper->getLabelDir() . $file)) {
            $value = $helper->__('Print');
            $url   = $this->getUrl('*/*/printLabel', array('shipment_id' => $row->getShipmentId(), 'type' => 'return'));

            return '<span class="grid-severity-notice"><span><a href="' . $url . '">' . $value . '</a></span></span>';
        }

        return '<span class="grid-severity-minor"><span>' . $helper->__('None') . '</span></span>';
    }

    /**
     * Prepare Massaction
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $this->setMassactionIdField('shipment_id');
        $this->getMassactionBlock()->setFormFieldName('shipment_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('print_deposit', array(
            'label' => $helper->__('Print Colissimo Deposit'),
            'url' => $this->getUrl('*/*/massPrintDeposit', array('_current' => true)),
        ));

        $this->getMassactionBlock()->addItem('print_shipping_label', array(
            'label'=> $helper->__('Print Shipping Labels (only PDF)'),
            'url'  => $this->getUrl('*/*/massPrintShippingLabel'),
        ));

        return $this;
    }

    /**
     * Prepare grid massaction column
     *
     * @return unknown
     */
    protected function _prepareMassactionColumn()
    {
        $columnId = 'massaction';
        $massactionColumn = $this->getLayout()->createBlock('adminhtml/widget_grid_column')
            ->setData(array(
                'index'          => $this->getMassactionIdField(),
                'use_index'      => true,
                'filter_index'   => $this->getMassactionIdFilter(),
                'type'           => 'massaction_extended',
                'name'           => $this->getMassactionBlock()->getFormFieldName(),
                'align'          => 'center',
                'is_system'      => true,
                'disabled_value' => null,
                'filter'         => false,
            ));

        if ($this->getNoFilterMassactionColumn()) {
            $massactionColumn->setData('filter', false);
        }

        $massactionColumn->setSelected($this->getMassactionBlock()->getSelected())
            ->setGrid($this)
            ->setId($columnId);

        $oldColumns = $this->_columns;
        $this->_columns = array();
        $this->_columns[$columnId] = $massactionColumn;
        $this->_columns = array_merge($this->_columns, $oldColumns);
        return $this;
    }

    /**
     * Retrieve status filter
     *
     * @return array
     */
    protected function _getStatusFilter()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $config = $helper->getFilterStatus();

        $statuses = array();

        if ($config) {
            $statuses = explode(',', $config);
        }

        return $statuses;
    }

    /**
     * Retrieve methods filter
     *
     * @return array
     */
    protected function _getMethodFilter()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $config = $helper->getFilterMethod();

        $methods = array();

        if ($config) {
            $methods = explode(',', $config);
        }

        return $methods;
    }

    /**
     * Filter Store Condition
     *
     * @param Mage_Sales_Model_Resource_Order_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $collection->addFieldToFilter($column->getFilterIndex(), $column->getFilter()->getValue());
    }
}
