<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Return_Create_Tabs_Shipper
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::registry('return_order');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('Shipper')
            )
        );

        $fieldset->addField(
            'shipper_email', 'text', array(
                'name'     => 'shipper[email]',
                'label'    => $helper->__('Email'),
                'required' => false,
                'class'    => 'validate-email',
            )
        );

        $fieldset->addField(
            'shipper_company', 'text', array(
                'name'     => 'shipper[company]',
                'label'    => $helper->__('Company'),
                'required' => false,
            )
        );

        $fieldset->addField(
            'shipper_firstname', 'text', array(
                'name'     => 'shipper[firstname]',
                'label'    => $helper->__('First Name'),
                'required' => false,
            )
        );

        $fieldset->addField(
            'shipper_lastname', 'text', array(
                'name'     => 'shipper[lastname]',
                'label'    => $helper->__('Last Name'),
                'required' => false,
            )
        );

        $fieldset->addField(
            'shipper_telephone', 'text', array(
                'name'     => 'shipper[telephone]',
                'label'    => $helper->__('Telephone'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'shipper_street_1', 'text', array(
                'name'     => 'shipper[street_1]',
                'label'    => $helper->__('Street 1'),
                'required' => true,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'shipper_street_2', 'text', array(
                'name'     => 'shipper[street_2]',
                'label'    => $helper->__('Street 2'),
                'required' => false,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'shipper_street_3', 'text', array(
                'name'     => 'shipper[street_3]',
                'label'    => $helper->__('Street 3'),
                'required' => false,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'shipper_city', 'text', array(
                'name'     => 'shipper[city]',
                'label'    => $helper->__('City'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'shipper_postcode', 'text', array(
                'name'     => 'shipper[postcode]',
                'label'    => $helper->__('Postcode'),
                'required' => true,
                'class'    => 'validate-zip-international',
            )
        );

        $fieldset->addField(
            'shipper_country_id', 'select', array(
                'name'     => 'shipper[country_id]',
                'label'    => $helper->__('Country'),
                'values'   => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
                'required' => true,
                'class'    => 'validate-select',
            )
        );

        $data = $order->getBillingAddress()->getData();
        foreach ($data as $key => $value) {
            $data['shipper_' . $key] = $value;
            unset($data[$key]);
        }

        $form->setValues($data);
        $form->addValues(array('shipper_street_1' => $order->getBillingAddress()->getStreet1()));
        $form->addValues(array('shipper_street_2' => $order->getBillingAddress()->getStreet2()));
        $form->addValues(array('shipper_street_3' => $order->getBillingAddress()->getStreet3()));
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_label')->__('Shipper');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_label')->__('Shipper');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}