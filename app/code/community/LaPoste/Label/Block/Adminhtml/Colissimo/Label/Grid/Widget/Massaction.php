<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Label_Grid_Widget_Massaction
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Massaction
{

    /**
     * Returns HTML of the checkbox
     *
     * @param string $value
     * @param bool   $checked
     * @return string
     */
    protected function _getCheckboxHtml($value, $checked)
    {
        $html = '';

        if ($value !== $this->getColumn()->getDisabledValue()) {
            $html .= '<input type="checkbox" name="' . $this->getColumn()->getName() . '" ';
            $html .= 'value="' . $this->escapeHtml($value) . '" class="massaction-checkbox"' . $checked . '/>';
        }

        return $html;
    }
}