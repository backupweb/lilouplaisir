<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Return_Create extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected $_mode = 'create';

    /**
     * Construct
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_colissimo_return';
        $this->_blockGroup = 'laposte_label';
        $this->_objectId   = 'order_id';

        parent::__construct();

        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        $this->removeButton('delete');
        $this->removeButton('reset');
        $this->_updateButton('save',   'label', $helper->__('Create'));
    }

    /**
     * Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__(Mage::helper('laposte_label')->__('Return label for order %s', Mage::registry('return_order')->getIncrementId()));
    }
}