<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Label_Block_Adminhtml_Colissimo_Return_Create_Tabs_Recipient
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_Label_Helper_Data $helper */
        $helper = Mage::helper('laposte_label');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::registry('return_order');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('Recipient')
            )
        );

        $fieldset->addField(
            'recipient_email', 'text', array(
                'name'     => 'recipient[email]',
                'label'    => $helper->__('Email'),
                'required' => true,
                'class'    => 'validate-email',
            )
        );

        $fieldset->addField(
            'recipient_company', 'text', array(
                'name'     => 'recipient[company]',
                'label'    => $helper->__('Company'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'recipient_service_info', 'text', array(
                'name'     => 'recipient[service_info]',
                'label'    => $helper->__('Service'),
                'required' => false,
            )
        );

        $fieldset->addField(
            'recipient_firstname', 'text', array(
                'name'     => 'recipient[firstname]',
                'label'    => $helper->__('First Name'),
            )
        );

        $fieldset->addField(
            'recipient_lastname', 'text', array(
                'name'     => 'recipient[lastname]',
                'label'    => $helper->__('Last Name'),
            )
        );

        $fieldset->addField(
            'recipient_telephone', 'text', array(
                'name'     => 'recipient[telephone]',
                'label'    => $helper->__('Telephone'),
                'required' => false,
            )
        );

        $fieldset->addField(
            'recipient_street_1', 'text', array(
                'name'     => 'recipient[street_1]',
                'label'    => $helper->__('Street 1'),
                'required' => true,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'recipient_street_2', 'text', array(
                'name'     => 'recipient[street_2]',
                'label'    => $helper->__('Street 2'),
                'required' => false,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'recipient_street_3', 'text', array(
                'name'     => 'recipient[street_3]',
                'label'    => $helper->__('Street 3'),
                'required' => false,
                'class'    => 'validate-alphanum-with-spaces',
            )
        );

        $fieldset->addField(
            'recipient_city', 'text', array(
                'name'     => 'recipient[city]',
                'label'    => $helper->__('City'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'recipient_postcode', 'text', array(
                'name'     => 'recipient[postcode]',
                'label'    => $helper->__('Postcode'),
                'required' => true,
                'class'    => 'validate-zip-international',
            )
        );

        $fieldset->addField(
            'recipient_country_id', 'select', array(
                'name'     => 'recipient[country_id]',
                'label'    => $helper->__('Country'),
                'values'   => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
                'required' => true,
                'class'    => 'validate-select',
            )
        );

        $storeId = $order->getStoreId();

        $fieldset->addField(
            'save_address', 'checkbox', array(
                'name'  => 'save_address',
                'label' => $helper->__('Save address for next returns (store %s)', $storeId),
            )
        );

        $fieldset->addField(
            'order_id', 'hidden', array(
                'name'     => 'order_id',
            )
        );

        $fieldset->addField(
            'store_id', 'hidden', array(
                'name'     => 'store_id',
            )
        );

        $fieldset->addField(
            'order_number', 'hidden', array(
                'name'     => 'order_number',
            )
        );

        $data = array(
            'order_number' => $order->getIncrementId(),
            'order_id'     => $order->getId(),
            'store_id'     => $order->getStoreId(),
            'save_address' => 1,
        );
        foreach ($fieldset->getElements() as $element) {
            if (!isset($data[$element->getId()])) {
                $data[$element->getId()] = Mage::getStoreConfig(
                    'shipping/label/return/' . preg_replace('/recipient_/', '', $element->getId()),
                    $storeId
                );
            }
        }

        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_label')->__('Recipient');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_label')->__('Recipient');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}