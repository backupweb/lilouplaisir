<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Frontend_Colissimo_Pickup extends LaPoste_Colissimo_Block_Frontend_Colissimo
{

    /**
     * Retrieve Pickup Controller URL
     *
     * @return string
     */
    public function getControllerUrl()
    {
        return $this->getUrl('colissimo/pickup/', array('_secure' => $this->isSecure()));
    }

    /**
     * Retrieve if map must be shown
     *
     * @return int
     */
    public function getShowMap()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->canShowMap();
    }

    /**
     * Retrieve Google Api Key
     *
     * @return string
     */
    public function getMapApiKey()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->getMapApiKey();
    }

    /**
     * Retrieve skin JS Url
     *
     * @return string
     */
    public function getSkinJs()
    {
        return $this->getBaseSkinUrl('js/script.js');
    }

    /**
     * Retrieve skin CSS Url
     *
     * @return string
     */
    public function getSkinCss()
    {
        return $this->getBaseSkinUrl('css/style.css');
    }

    /**
     * Retrieve base skin URL
     *
     * @param string $file
     * @return string
     */
    public function getBaseSkinUrl($file)
    {
        $skinUrl = '';

        if ($this->getSkinData()->getData('skin')) {
            $skinUrl = $this->getSkinUrl(
                $this->getSkinData()->getData('skin') . '/' . $file, array('_secure' => $this->isSecure())
            );
        }

        return $skinUrl;
    }

    /**
     * Retrieve skin data
     *
     * @return Varien_Object
     */
    public function getSkinData()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');
        $data = $helper->getSkinData();

        $object = new Varien_Object();
        $object->setData($data);

        Mage::dispatchEvent('laposte_colissimo_pickup_skin', array('skin' => $object, 'pickup' => $this));

        return $object;
    }
}