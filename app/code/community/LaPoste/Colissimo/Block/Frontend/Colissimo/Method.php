<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Frontend_Colissimo_Method extends LaPoste_Colissimo_Block_Frontend_Colissimo
{

    /**
     * Retrieve Method Controller URL
     *
     * @return string
     */
    public function getControllerUrl()
    {
        return $this->getUrl('colissimo/method/', array('_secure' => $this->isSecure()));
    }

    /**
     * Retrieve method JS
     *
     * @param string $script
     * @return string
     */
    public function getMethodJs($script)
    {
        return $this->getSkinUrl('colissimo/method/' . $script, array('_secure' => $this->isSecure()));
    }

    /**
     * Retrieve Options as Json
     *
     * @return bool
     */
    public function getOptionsJson()
    {
        $options = array(
            'homecl_colissimo'        => array('phone' => $this->phoneIsRequired('homecl')),
            'homesi_colissimo'        => array('phone' => $this->phoneIsRequired('homesi')),
            'international_colissimo' => array('phone' => $this->phoneIsRequired('international')),
            'domtomcl_colissimo'      => array('phone' => $this->phoneIsRequired('domtomcl')),
            'domtomsi_colissimo'      => array('phone' => $this->phoneIsRequired('domtomsi')),
        );

        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');

        return $helper->jsonEncode($options);
    }

    /**
     * Retrieve if mobile phone is required
     *
     * @param string $method
     * @return bool
     */
    public function phoneIsRequired($method)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->phoneIsRequired($method);
    }
}