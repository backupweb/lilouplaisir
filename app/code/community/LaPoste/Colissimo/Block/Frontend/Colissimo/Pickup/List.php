<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Frontend_Colissimo_Pickup_List extends LaPoste_Colissimo_Block_Frontend_Colissimo_Pickup
{

    /**
     * @var LaPoste_Colissimo_Model_Pickup_Collection
     */
    protected $_collection = null;

    /**
     * Get relevant path to template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->getSkinData()->getData('template');
    }

    /**
     * Retrieve list of pickup
     *
     * @return LaPoste_Colissimo_Model_Pickup_Collection
     */
    public function getList()
    {
        /** @var LaPoste_Colissimo_Model_Pickup_Collection $pickup */
        $pickup = Mage::getModel('laposte_colissimo/pickup_collection');

        if (is_null($this->_collection)) {
            $this->_collection = $pickup->getList(
                $this->getAddress(),
                $this->getCity(),
                $this->getPostcode(),
                $this->getCountryId(),
                $this->getWeight()
            );
        }

        return $this->_collection;
    }

    /**
     * Retrieve pickup as JSON
     *
     * @return string
     */
    public function getPickupAsJson()
    {
        $collection = $this->getList();

        $pickup = array();

        $icon = $this->getBaseSkinUrl('images/colissimo.png');

        foreach ($collection as $item) {
            /** @var LaPoste_Colissimo_Model_Pickup_Interface $item */
            $pickup[] = array(
                '<strong>' . $item->getNom() . '</strong><br />' . $item->getAdresse1(),
                $item->getCoordGeolocalisationLatitude(),
                $item->getCoordGeolocalisationLongitude(),
                'colissimo-pickup-' . $item->getIdentifiant(),
                $icon,
            );
        }

        return json_encode($pickup);
    }

    /**
     * Retrieve available countries
     *
     * @return array
     */
    public function getCountries()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $available = $helper->getSpecificCountry('pickup');

        $locale = Mage::app()->getLocale();

        $countries = array();

        if ($available) {
            $codes = explode(',', $available);
            foreach ($codes as $code) {
                if (preg_match('/[A-Z]{2}/', $code)) {
                    $countries[$code] = $locale->getCountryTranslation($code);
                }
            }
        }

        if (!count($countries)) {
            $countries = array(
                'FR' => $locale->getCountryTranslation('FR')
            );
        }

        return $countries;
    }

    /**
     * Check if address is valid
     *
     * @return bool
     */
    public function isValidAddress()
    {
        return $this->getCity() && $this->getPostcode() && $this->getCountryId();
    }

    /**
     * Retrieve Full Street
     *
     * @param LaPoste_Colissimo_Model_Pickup $pickup
     * @param string $separator
     * @return string
     */
    public function getFullStreet($pickup, $separator)
    {
        return $pickup->getData('adresse1') .
            ($pickup->getData('adresse2') ? $separator . $pickup->getData('adresse2') : '') .
            ($pickup->getData('adresse3') ? $separator . $pickup->getData('adresse3') : '');
    }

    /**
     * Check holiday
     *
     * @param LaPoste_Colissimo_Model_Pickup $pickup
     * @return bool|object
     */
    public function isHoliday($pickup)
    {
        return $pickup->getListeConges();
    }
    /**
     * Retrieve Holiday Start
     *
     * @param LaPoste_Colissimo_Model_Pickup $pickup
     * @return string
     */
    public function getHolidayStart($pickup)
    {
        if ($this->isHoliday($pickup)) {
            return date('d/m/Y', strtotime($pickup->getListeConges()->calendarDeDebut));
        }
        return '';
    }
    /**
     * Retrieve Holiday End
     *
     * @param LaPoste_Colissimo_Model_Pickup $pickup
     * @return string
     */
    public function getHolidayEnd($pickup)
    {
        if ($this->isHoliday($pickup)) {
            return date('d/m/Y', strtotime($pickup->getListeConges()->calendarDeFin));
        }
        return '';
    }

    /**
     * Retrieve current weight (in gram)
     *
     * @return float
     */
    public function getWeight()
    {
        if ($this->getData('weight')) {
            $weight = $this->getData('weight');
        } else {
            $weight = $this->getShippingAddress()->getWeight();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->convertWeight($weight);
    }

    /**
     * Retrieve address
     *
     * @return string
     */
    public function getAddress()
    {
        if (!is_null($this->getData('address'))) {
            $address = $this->getData('address');
        } else {
            $address = $this->getShippingAddress()->getStreet1();
        }

        return $address;
    }

    /**
     * Retrieve postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        if (!is_null($this->getData('postcode'))) {
            $postcode = $this->getData('postcode');
        } else {
            $postcode = $this->getShippingAddress()->getPostcode();
        }

        return $postcode;
    }

    /**
     * Retrieve city
     *
     * @return string
     */
    public function getCity()
    {
        if (!is_null($this->getData('city'))) {
            $city = $this->getData('city');
        } else {
            $city = $this->getShippingAddress()->getCity();
        }

        return $city;
    }

    /**
     * Retrieve country id
     *
     * @return int
     */
    public function getCountryId()
    {
        if (!is_null($this->getData('country_id'))) {
            $countryId = $this->getData('country_id');
        } else {
            $countryId = $this->getShippingAddress()->getCountryId();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $countryId = $helper->getCountry($countryId, $this->getPostcode());

        return $countryId;
    }

    /**
     * Retrieve Firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        if (!is_null($this->getData('firstname'))) {
            $firstname = $this->getData('firstname');
        } else {
            $firstname = $this->getShippingAddress()->getFirstname();
        }

        return $firstname;
    }

    /**
     * Retrieve Lastname
     *
     * @return string
     */
    public function getLastname()
    {
        if (!is_null($this->getData('lastname'))) {
            $lastname = $this->getData('lastname');
        } else {
            $lastname = $this->getShippingAddress()->getLastname();
        }

        return $lastname;
    }

    /**
     * Retrieve Telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        if (!is_null($this->getData('telephone'))) {
            $telephone = $this->getData('telephone');
        } else {
            $telephone = $this->getShippingAddress()->getTelephone();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');
        $telephone = $helper->getTelephone($telephone, $this->getCountryId(), 'remove');

        return $telephone;
    }

    /**
     * Retrieve phone code
     *
     * @return string
     */
    public function getPhoneCode()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->getPhoneCode($this->getCountryId());
    }

    /**
     * Retrieve if deliver form must be shown
     *
     * @return bool
     */
    public function showDeliverForm()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->showDeliverForm('pickup');
    }

    /**
     * Retrieve if mobile phone is required
     *
     * @return bool
     */
    public function phoneIsRequired()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->phoneIsRequired('pickup');
    }

    /**
     * Retrieve current pickup id
     *
     * @return string
     */
    public function getPickupId()
    {
        if (!is_null($this->getData('pickup_id'))) {
            $pickupId = $this->getData('pickup_id');
        } else {
            $pickupId = $this->getShippingAddress()->getColissimoPickupId();
        }

        return $pickupId;
    }

    /**
     * Address setter
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->setData('address', $address);
    }

    /**
     * Postcode setter
     *
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->setData('postcode', $postcode);
    }

    /**
     * City setter
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->setData('city', $city);
    }

    /**
     * Country setter
     *
     * @param string $countryId
     */
    public function setCountryId($countryId)
    {
        $this->setData('country_id', $countryId);
    }

    /**
     * Retrieve Checkout Context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->getData('context');
    }
}