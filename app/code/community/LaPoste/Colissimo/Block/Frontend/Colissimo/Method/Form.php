<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Frontend_Colissimo_Method_Form extends LaPoste_Colissimo_Block_Frontend_Colissimo_Method
{

    /**
     * Set template
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('colissimo/colissimo/method/form.phtml');
    }

    /**
     * Retrieve Telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        if (!is_null($this->getData('telephone'))) {
            $telephone = $this->getData('telephone');
        } else {
            $telephone = $this->getShippingAddress()->getTelephone();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');
        $telephone = $helper->getTelephone($telephone, $this->getCountryId(), 'remove');

        return $telephone;
    }

    /**
     * Retrieve phone code
     *
     * @return string
     */
    public function getPhoneCode()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->getPhoneCode($this->getCountryId());
    }

    /**
     * Retrieve country id
     *
     * @return int
     */
    public function getCountryId()
    {
        if (!is_null($this->getData('country_id'))) {
            $countryId = $this->getData('country_id');
        } else {
            $countryId = $this->getShippingAddress()->getCountryId();
        }

        return $countryId;
    }

}