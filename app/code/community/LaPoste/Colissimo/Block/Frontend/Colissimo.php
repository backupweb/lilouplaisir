<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Frontend_Colissimo extends Mage_Core_Block_Template
{

    /**
     * Retrieve lib JS Url
     *
     * @param string $file
     * @return string
     */
    public function getLibJs($file)
    {
        return $this->getSkinUrl('colissimo/lib/' . $file, array('_secure' => $this->isSecure()));
    }

    /**
     * Check store is secure
     *
     * @return bool
     */
    protected function isSecure()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->isSecure();
    }

    /**
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getShippingAddress()
    {
        /** @var Mage_Checkout_Model_Session $session */
        $session = Mage::getSingleton('checkout/session');

        return $session->getQuote()->getShippingAddress();
    }

    /**
     * Retrieve if pickup must be shown
     *
     * @return bool
     */
    public function canShowPickup()
    {
        return $this->canShow('pickup');
    }

    /**
     * Retrieve if method must be shown
     *
     * @return bool
     */
    public function canShowMethod()
    {
        $methods = array('international', 'homecl', 'homesi', 'domtomcl', 'domtomsi');

        foreach ($methods as $method) {
            if ($this->canShow($method)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve if method must be shown
     *
     * @param $method
     * @return bool
     */
    public function canShow($method)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->isActive($method);
    }

    /**
     * Set Checkout Context
     *
     * @param string $context
     */
    public function setContext($context)
    {
        $this->setData('context', $context);
    }
}