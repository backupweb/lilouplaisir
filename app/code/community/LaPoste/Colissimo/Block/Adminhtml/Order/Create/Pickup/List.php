<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Order_Create_Pickup_List extends Mage_Adminhtml_Block_Template
{

    /**
     * @var LaPoste_Colissimo_Model_Pickup_Collection
     */
    protected $_collection = null;

    /**
     * Retrieve list of pickup
     *
     * @return LaPoste_Colissimo_Model_Pickup_Collection
     */
    public function getList()
    {
        /** @var LaPoste_Colissimo_Model_Pickup_Collection $pickup */
        $pickup = Mage::getModel('laposte_colissimo/pickup_collection');

        if (is_null($this->_collection)) {
            $this->_collection = $pickup->getList(
                $this->getAddress(),
                $this->getCity(),
                $this->getPostcode(),
                $this->getCountryId(),
                $this->getWeight()
            );
        }

        return $this->_collection;
    }

    /**
     * Retrieve current pickup id
     *
     * @return string
     */
    public function getPickupId()
    {
        if (!is_null($this->getData('pickup_id'))) {
            $pickupId = $this->getData('pickup_id');
        } else {
            $pickupId = $this->getShippingAddress()->getColissimoPickupId();
        }

        if (!$pickupId) {
            $pickupId = $this->getList()->getFirstItem()->getIdentifiant();
        }

        return $pickupId;
    }

    /**
     * Retrieve current weight (in gram)
     *
     * @return float
     */
    public function getWeight()
    {
        if ($this->getData('weight')) {
            $weight = $this->getData('weight');
        } else {
            $weight = $this->getShippingAddress()->getWeight();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        return $helper->convertWeight($weight);
    }

    /**
     * Retrieve address
     *
     * @return string
     */
    public function getAddress()
    {
        if (!is_null($this->getData('address'))) {
            $address = $this->getData('address');
        } else {
            $address = $this->getShippingAddress()->getStreet1();
        }

        return $address;
    }

    /**
     * Retrieve postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        if (!is_null($this->getData('postcode'))) {
            $postcode = $this->getData('postcode');
        } else {
            $postcode = $this->getShippingAddress()->getPostcode();
        }

        return $postcode;
    }

    /**
     * Retrieve city
     *
     * @return string
     */
    public function getCity()
    {
        if (!is_null($this->getData('city'))) {
            $city = $this->getData('city');
        } else {
            $city = $this->getShippingAddress()->getCity();
        }

        return $city;
    }

    /**
     * Retrieve country id
     *
     * @return int
     */
    public function getCountryId()
    {
        if (!is_null($this->getData('country_id'))) {
            $countryId = $this->getData('country_id');
        } else {
            $countryId = $this->getShippingAddress()->getCountryId();
        }

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $countryId = $helper->getCountry($countryId, $this->getPostcode());

        return $countryId;
    }

    /**
     * Retrieve shipping address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getShippingAddress()
    {
        /* @var $create Mage_Adminhtml_Model_Sales_Order_Create */
        $create = Mage::getSingleton('adminhtml/sales_order_create');

        return $create->getShippingAddress();
    }
}