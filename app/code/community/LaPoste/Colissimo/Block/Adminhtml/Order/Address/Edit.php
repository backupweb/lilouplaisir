<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Order_Address_Edit extends Mage_Adminhtml_Block_Template
{

    /**
     * Check if administrator can edit address
     *
     * @return bool
     */
    public function canEdit()
    {
        $address = Mage::registry('order_address');

        if ($address->getAddressType() == 'shipping' && $address->getColissimoPickupId()) {
            return false;
        }

        return true;
    }

    /**
     * Retrieve Field To Hidden
     *
     * @return array
     */
    public function disabledEditAttribute()
    {
        return array(
            'company',
            'street0',
            'street1',
            'city',
            'country_id',
            'region_id',
            'postcode',
        );
    }

}