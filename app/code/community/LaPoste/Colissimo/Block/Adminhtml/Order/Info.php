<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Order_Info
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('laposte/colissimo/order/info.phtml');
    }

    /**
     * Retrieve order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * Retrieve Shipping Address
     *
     * @return Mage_Sales_Model_Order_Address
     */
    public function getShippingAddress()
    {
        return $this->getOrder()->getShippingAddress();
    }

    /**
     * Retrieve Tab Label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_colissimo')->__('Colissimo');
    }

    /**
     * Retrieve Tab Title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_colissimo')->__('Colissimo');
    }

    /**
     * Check can show table
     *
     * @return bool
     */
    public function canShowTab()
    {
        if (!$this->getShippingAddress()) {
            return false;
        }
        if (!$this->getShippingAddress()->getColissimoProductCode()) {
            return false;
        }
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

}