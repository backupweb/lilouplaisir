<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_colissimo_price';
        $this->_blockGroup = 'laposte_colissimo';
        $this->_headerText = Mage::helper('laposte_colissimo')->__('Manage Prices');

        $this->_addButtonLabel = Mage::helper('laposte_colissimo')->__('Add price');

        parent::__construct();
    }

}