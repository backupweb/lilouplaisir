<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Grid_Widget_Import extends Mage_Adminhtml_Block_Template
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('laposte/colissimo/price/widget/import.phtml');
    }

    /**
     * Retrieve import URL
     *
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }

}