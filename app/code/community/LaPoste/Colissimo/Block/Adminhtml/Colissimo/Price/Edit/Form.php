<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                'id'      => 'edit_form',
                'method'  => 'post',
                'action'  => $this->getUrl('*/*/save'),
            )
        );

        if (Mage::registry('colissimo_price')->getId()) {
            $form->addField('price_id', 'hidden', array('name' => 'price_id'));
            $form->setValues(array('price_id' => Mage::registry('colissimo_price')->getId()));
        }

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
