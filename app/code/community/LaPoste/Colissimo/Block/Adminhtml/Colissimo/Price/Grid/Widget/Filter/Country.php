<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Grid_Widget_Filter_Country
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select
{

    /**
     * Retrieve options
     *
     * @return array
     */
    protected function _getOptions()
    {
        $filter = $this->_getCountryFilter();

        $options = array();

        if (count($filter)) {
            /** @var Mage_Directory_Model_Resource_Country_Collection $countries */
            $countries = Mage::getResourceModel('directory/country_collection');

            $options = $countries
                ->addCountryCodeFilter($this->_getCountryFilter(), 'iso2')
                ->load()
                ->toOptionArray(false);
        }

        array_unshift($options, array(
            'value' => '',
            'label' => Mage::helper('laposte_colissimo')->__('All Countries')
        ));

        return $options;
    }

    /**
     * Retrieve status filter
     *
     * @return array
     */
    protected function _getCountryFilter()
    {
        /** @var LaPoste_Colissimo_Model_Price $price */
        $price = Mage::getModel('laposte_colissimo/price');

        return $price->getCountries();
    }
}
