<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Edit_Tabs_Conditions
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $model = Mage::registry('colissimo_price');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('Conditions')
            )
        );

        $shippingMethod = $fieldset->addField(
            'shipping_method', 'select', array(
                'name'     => 'shipping_method',
                'label'    => $helper->__('Shipping Method'),
                'class'    => 'required-entry',
                'required' => true,
                'values'   => Mage::getSingleton('laposte_colissimo/system_carrier')->toOptionArray(),
            )
        );

        $countries = array();

        foreach ($this->getShippingCountry() as $code => $class) {
            $countries[$code] = $fieldset->addField(
                $code . '_country_id', 'select', array(
                    'name' => $code . '[country_id]',
                    'label' => $helper->__('Country'),
                    'class' => 'required-entry',
                    'required' => true,
                    'values' => Mage::getSingleton($class)->toOptionArray(),
                )
            );
        }

        $fieldset->addField(
            'weight_from', 'text', array(
                'name'     => 'weight_from',
                'label'    => $helper->__('Weight (From)'),
                'after_element_html' => '<p class="nm"><small>' . $helper->__('Weight in gram') . '</small></p>',
            )
        );

        $fieldset->addField(
            'weight_to', 'text', array(
                'name'     => 'weight_to',
                'label'    => $helper->__('Weight (To)'),
                'after_element_html' => '<p class="nm"><small>' . $helper->__('Weight in gram') . '</small></p>',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => $helper->__('Store View'),
                'title'     => $helper->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));

            /* @var $renderer Mage_Adminhtml_Block_Store_Switcher_Form_Renderer_Fieldset_Element */
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'store_id',
                'value'     => 0
            ));
            $model->setStoreId(0);
        }

        /** @var Mage_Adminhtml_Block_Widget_Form_Element_Dependence $dependence */
        $dependence = $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence');
        $dependence->addFieldMap($shippingMethod->getHtmlId(), $shippingMethod->getName());

        foreach ($this->getShippingCountry() as $code => $class) {
            $dependence->addFieldMap($countries[$code]->getHtmlId(), $countries[$code]->getName());

            $model->setData($countries[$code]->getHtmlId(), $model->getCountryId());

            $dependence->addFieldDependence(
                $countries[$code]->getName(),
                $shippingMethod->getName(),
                $code
            );
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        $this->setChild('form_after', $dependence);

        return parent::_prepareForm();
    }

    /**
     * Get country values
     *
     * @return array
     */
    public function getShippingCountry()
    {
        return array(
            'homecl'        => 'laposte_colissimo/system_country_home_classic',
            'homesi'        => 'laposte_colissimo/system_country_home_signature',
            'pickup'        => 'laposte_colissimo/system_country_pickup',
            'international' => 'laposte_colissimo/system_country_international',
            'domtomcl'      => 'laposte_colissimo/system_country_domtom_classic',
            'domtomsi'      => 'laposte_colissimo/system_country_domtom_signature',
        );
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_colissimo')->__('Conditions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_colissimo')->__('Conditions');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

}