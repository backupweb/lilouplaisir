<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('shipping_method');
        $this->setDefaultDir('asc');
        $this->setId('price_grid');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve Store Id
     *
     * @return Mage_Core_Model_Store
     */
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * Prepare Collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var LaPoste_Colissimo_Model_Resource_Price_Collection $collection */
        $collection = Mage::getModel('laposte_colissimo/price')->getCollection();

        if ($this->_isExport) {
            $collection->getSelect()->reset(Zend_Db_Select::ORDER);
            $collection->getSelect()->order('store_id ASC');
            $collection->getSelect()->order('shipping_method ASC');
            $collection->getSelect()->order('country_id ASC');
            $collection->getSelect()->order('weight_from ASC');
        } else {
            $sort = $this->getParam($this->getVarNameSort());
            if (!$sort || $sort == 'shipping_method') {
                $collection->getSelect()->order('store_id ASC');
                $collection->getSelect()->order('shipping_method ASC');
                $collection->getSelect()->order('country_id ASC');
                $collection->getSelect()->order('weight_from ASC');
            }
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $this->setColumnFilters(
            array(
                'colissimo_country' => 'laposte_colissimo/adminhtml_colissimo_price_grid_widget_filter_country',
            )
        );

        $this->setColumnRenderers(
            array(
                'colissimo_country'      => 'adminhtml/widget_grid_column_renderer_country',
                'colissimo_price_update' => 'laposte_colissimo/adminhtml_colissimo_price_grid_renderer_update',
            )
        );

        if ($this->_isExport) {
            $this->addColumn('shipping_method_export', array(
                    'header'  => 'shipping_method',
                    'index'   => 'shipping_method',
                )
            );
            $this->addColumn('store_id_export', array(
                    'header' => 'store_id',
                    'index'  => 'store_id',
                )
            );
            $this->addColumn('country_export', array(
                    'header' => 'country_name',
                    'index'  => 'country_id',
                    'type'   => 'country',
                )
            );
        }

        if (!$this->_isExport) {
            $this->addColumn('color', array(
                    'header' => '',
                    'align' => 'left',
                    'index' => 'shipping_method',
                    'width' => '20px',
                    'frame_callback' => array($this, 'decorateMethod'),
                    'sortable'      => false,
                    'filter'        => false,
                )
            );
        }

        $this->addColumn('shipping_method', array(
                'header'         => !$this->_isExport ? $helper->__('Method') : 'shipping_name',
                'align'          => 'left',
                'index'          => 'shipping_method',
                'type'           => 'options',
                'options'        => Mage::getSingleton('laposte_colissimo/system_carrier')->toArray(),
                'width'          => '250px',
            )
        );

        $this->addColumn('country_id', array(
                'header'  => !$this->_isExport ? $helper->__('Country') : 'country_id',
                'align'   => 'left',
                'index'   => 'country_id',
                'type'    => !$this->_isExport ? 'colissimo_country' : 'text',
                'width'   => '150px',
            )
        );

        $this->addColumn('weight_from', array(
                'header'  => !$this->_isExport ? $helper->__('Weight (From)') : 'weight_from',
                'align'   => 'left',
                'type'    => !$this->_isExport ? 'colissimo_price_update' : 'text',
                'index'   => 'weight_from',
                'default' => '',
                'width'   => '150px',
                'filter'  => false,
            )
        );

        $this->addColumn('weight_to', array(
                'header'  => !$this->_isExport ? $helper->__('Weight (To)') : 'weight_to',
                'align'   => 'left',
                'type'    => !$this->_isExport ? 'colissimo_price_update' : 'text',
                'index'   => 'weight_to',
                'default' => '',
                'width'   => '150px',
                'filter'  => false,
            )
        );

        $this->addColumn('price', array(
                'header' => !$this->_isExport ? $helper->__('Shipping Amount') : 'price',
                'align'  => 'left',
                'type'   => !$this->_isExport ? 'colissimo_price_update' : 'text',
                'index'  => 'price',
                'width'  => '150px',
                'filter' => false,
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                    'header'        => !$this->_isExport ? $helper->__('Store View') : 'store_id',
                    'index'         => 'store_id',
                    'type'          => 'store',
                    'store_all'     => true,
                    'store_view'    => true,
                    'sortable'      => false,
                    'filter'        => false,
                    'skipEmptyStoresLabel' => true,
                    'width'         => '200px',
                )
            );
        }

        if (!$this->_isExport) {
            $this->addColumn('action',
                array(
                    'header'    => $helper->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                        array(
                            'caption'  => $helper->__('Edit'),
                            'url'      => array(
                                'base' =>'*/*/edit',
                            ),
                            'field'    => 'price_id'
                        ),
                        array(
                            'caption'  => $helper->__('Duplicate'),
                            'url'      => array(
                                'base' =>'*/*/duplicate',
                            ),
                            'field'    => 'price_id'
                        ),
                        array(
                            'caption'  => $helper->__('Delete'),
                            'url'      => array(
                                'base' =>'*/*/delete',
                            ),
                            'field'    => 'price_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                )
            );
        }

        $this->addExportType('*/*/exportCsv', $helper->__('CSV'));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve Row Url
     *
     * @param object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('price_id' => $row->getId()));
    }

    /**
     * Prepare Massaction
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $this->setMassactionIdField('price_id');
        $this->getMassactionBlock()->setFormFieldName('price_ids');

        $this->getMassactionBlock()->addItem('delete', array(
                'label'   => $helper->__('Delete'),
                'url'     => $this->getUrl('*/*/massDelete'),
                'confirm' => $helper->__('Are you sure?')
            )
        );

        return $this;
    }

    /**
     * Retrieve main button Html
     *
     * @return string
     */
    public function getMainButtonsHtml()
    {
        /** @var LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Grid_Widget_Import $import */
        $import = $this->getLayout()->createBlock('laposte_colissimo/adminhtml_colissimo_price_grid_widget_import');
        $html = $import->toHtml();

        $html .= parent::getMainButtonsHtml();

        return $html;
    }

    /**
     * Decorate expeditor_is_exported column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     *
     * @return string
     */
    public function decorateMethod($value, $row, $column, $isExport)
    {
        $print = $value;

        if (!$isExport) {
            $n = crc32($value . $row->getStoreId() * 5);
            $n &= 0xffffffff;
            $print = '<span class="colissimo-price-color" style="background-color:#' . substr("000000" . dechex($n), -6) . '"></span>';
        }

        return $print;
    }

}