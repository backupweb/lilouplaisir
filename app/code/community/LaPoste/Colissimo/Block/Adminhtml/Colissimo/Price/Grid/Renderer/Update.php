<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Grid_Renderer_Update
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('laposte/colissimo/price/renderer/update.phtml');
    }

    /**
     * Render HTML
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $this->setPriceId($row->getPriceId());
        $this->setField($this->getColumn()->getIndex());
        $this->setValue($this->_getValue($row));

        return $this->toHtml();
    }

}