<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('price_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('laposte_colissimo')->__('Price'));
    }

}