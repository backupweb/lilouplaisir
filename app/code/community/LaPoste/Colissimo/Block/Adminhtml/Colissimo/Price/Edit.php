<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Block_Adminhtml_Colissimo_Price_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Construct
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_colissimo_price';
        $this->_blockGroup = 'laposte_colissimo';
        $this->_objectId   = 'price_id';

        parent::__construct();

        $this->_updateButton('save',   'label', Mage::helper('laposte_colissimo')->__('Save Price'));
        $this->_updateButton('delete', 'label', Mage::helper('laposte_colissimo')->__('Delete Price'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('laposte_colissimo')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
    }

    /**
     * Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $title = Mage::registry('colissimo_price')->getId() ? 'Edit Price' : 'Add Price';
        return $this->__(Mage::helper('laposte_colissimo')->__($title));
    }

}