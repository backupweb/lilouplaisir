<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('laposte_colissimo/price')}` (
  `price_id`        INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_id`      VARCHAR(2) DEFAULT NULL,
  `store_id`        SMALLINT(5) unsigned NOT NULL DEFAULT 0,
  `price`           DECIMAL(12,4) NOT NULL DEFAULT '0.0000',
  `weight_from`     DECIMAL(12,4) DEFAULT NULL,
  `weight_to`       DECIMAL(12,4) DEFAULT NULL,
  `shipping_method` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='Colissimo Prices'
");

$installer->endSetup();