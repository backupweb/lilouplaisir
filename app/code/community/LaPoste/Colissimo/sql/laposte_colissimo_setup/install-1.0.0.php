<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* Add quote and order columns */

$data = array(
    'colissimo_product_code',
    'colissimo_network_code',
    'colissimo_pickup_id',
);

foreach ($data as $column) {
    $this->getConnection()->addColumn(
        $this->getTable('sales/quote_address'), $column, "VARCHAR(255) NULL"
    );
    $this->getConnection()->addColumn(
        $this->getTable('sales/order_address'), $column, "VARCHAR(255) NULL"
    );
}

$installer->endSetup();