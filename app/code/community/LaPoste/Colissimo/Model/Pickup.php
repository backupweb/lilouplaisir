<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Pickup extends Varien_Object implements LaPoste_Colissimo_Model_Pickup_Interface
{

    /**
     * Load specific pickup
     *
     * @param string $pickupId
     * @param string $network
     * @return $this
     */
    public function load($pickupId, $network)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $apiConfig = $helper->getApiConfig();

        if ($apiConfig['login'] && $apiConfig['password']) {

            try {
                $client = new SoapClient($apiConfig['wsdl'], array('encoding' => 'iso-8859-1'));

                $data = array(
                    'accountNumber' => $apiConfig['login'],
                    'password'      => $apiConfig['password'],
                    'date'          => date('d/m/Y'),
                    'filterRelay'   => '1',
                    'id'            => $pickupId,
                    'reseau'        => $network ?: '',
                );

                $result = $client->findPointRetraitAcheminementByID($data)->return;

                if ($result->errorMessage == 'Code retour OK') {
                    foreach ($result->pointRetraitAcheminement as $k => $v) {
                        $key = preg_replace_callback(
                            '/([A-Z])/', create_function('$m', 'return "_".strtolower($m[1]);'), $k
                        );
                        if (is_string($v)) {
                            $v = utf8_encode($v);
                        }
                        $this->setData(trim($key, '_'), $v);
                    }
                } else {
                    Mage::log($result->errorMessage, Zend_Log::ERR, $helper->getLogFile());
                }
            } catch (SoapFault $fault) {
                Mage::log($fault->getMessage(), Zend_Log::ERR, $helper->getLogFile());
            } catch (Exception $e) {
                Mage::log($e->getMessage(), Zend_Log::ERR, $helper->getLogFile());
            }

        }

        return $this;
    }

    /**
     * Save shipping Address
     *
     * @param Varien_object $request
     * @return array|bool
     */
    public function saveAddress($request)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $pickup = $request->getPickup();

        if (!$pickup) {
            return array(
                'success' => false,
                'error'   => true,
                'message' => $helper->__('Please select pickup')
            );
        }

        list($pickupId, $network) = explode('-', $pickup);

        /** @var LaPoste_Colissimo_Model_Pickup $pickup */
        $this->load($pickupId, $network);

        if ($this->hasData()) {
            /** @var Mage_Sales_Model_Order_Address $shippingAddress */
            $shippingAddress = $request->getShippingAddress();

            /** @var Mage_Sales_Model_Order_Address $billingAddress */
            $billingAddress = $request->getBillingAddress();

            /* Update shipping address with pickup data */
            $shippingAddress->setSameAsBilling(0)
                ->setCustomerAddressId(null)
                ->setSaveInAddressBook(0)
                ->setColissimoPickupId($this->getIdentifiant())
                ->setColissimoProductCode($this->getTypeDePoint())
                ->setColissimoNetworkCode($this->getReseau())
                ->setCity($this->getLocalite())
                ->setPostcode($this->getCodePostal())
                ->setCompany($this->getNom())
                ->setStreet($this->getAdresse1() . "\n" . $this->getAdresse2())
                ->setCountryId($this->getCodePays())
                ->setFax(null)
                ->setRegionId(null)
                ->setRegion(null);

            /* Update shipping customer data */
            $additional = array('lastname', 'firstname', 'telephone');

            foreach ($additional as $attribute) {
                if ($request->getData($attribute)) {
                    $value = $request->getData($attribute);
                    if ($attribute == 'telephone') {
                        $value = $helper->getTelephone(
                            preg_replace('/[^0-9+]/', '', $value), $shippingAddress->getCountryId(), 'add'
                        );
                    }
                    $shippingAddress->setData($attribute, $value);
                }
            }

            /* Update billing address with pickup data */
            $billingAddress->setColissimoPickupId($this->getIdentifiant())
                ->setColissimoProductCode($this->getTypeDePoint())
                ->setColissimoNetworkCode($this->getReseau());

            /* Save addresses */
            $shippingAddress->save();
            $billingAddress->save();

            /* Valid telephone */
            list($carrierCode, $method) = explode('_', $request->getShippingMethod(), 2);

            if ($helper->phoneIsRequired($carrierCode)) {
                if (!$helper->isValidMobileNumber($shippingAddress->getTelephone(), $shippingAddress->getCountryId())) {
                    $example = $helper->getConfig(
                        'country_settings/' . $shippingAddress->getCountryId() . '/phone_example'
                    );
                    return array(
                        'success' => false,
                        'error' => true,
                        'message' => $helper->__(
                            'Please enter a valid mobile phone number. Example: %s', $example
                        )
                    );
                }
            }
        }

        return true;
    }

    /**
     * Save pickup address in session
     *
     * @return $this
     */
    public function saveInSession()
    {
        if (!$this->hasData()) {
            return $this;
        }

        $shippingAddress = array(
            'same_as_billing'        => 0,
            'customer_address_id'    => null,
            'save_in_address_book'   => 0,
            'colissimo_pickup_id'    => $this->getIdentifiant(),
            'colissimo_product_code' => $this->getTypeDePoint(),
            'colissimo_network_code' => $this->getReseau(),
            'city'                   => $this->getLocalite(),
            'postcode'               => $this->getCodePostal(),
            'company'                => $this->getNom(),
            'street'                 => $this->getAdresse1() . "\n" . $this->getAdresse2(),
            'country_id'             => $this->getCodePays(),
            'fax'                    => null,
            'region_id'              => null,
            'region'                 => null,
        );

        $this->getCheckoutSession()->setData('colissimo_pickup_address', $shippingAddress);

        return $this;
    }

    /**
     * Load pickup saved in session
     *
     * @return array
     */
    public function loadFromSession()
    {
        return $this->getCheckoutSession()->getData('colissimo_pickup_address');
    }

    /**
     * Delete pickup saved in session
     *
     * @return $this
     */
    public function deleteFromSession()
    {
        $this->getCheckoutSession()->unsetData('colissimo_pickup_address');

        return $this;
    }

    /**
     * Retrieve checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        /** @var Mage_Checkout_Model_Type_Onepage $onePage */
        $onePage = Mage::getSingleton('checkout/type_onepage');

        return $onePage->getCheckout();
    }

    /**
     * Pickup name
     *
     * @return string
     */
    public function getNom()
    {
        return $this->getData('nom');
    }

    /**
     * Pickup address line 1
     *
     * @return string
     */
    public function getAdresse1()
    {
        return $this->getData('adresse1');
    }

    /**
     * Pickup address line 2
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->getData('address2');
    }

    /**
     * Pickup address line 3
     *
     * @return string
     */
    public function getAdresse3()
    {
        return $this->getData('adresse3');
    }

    /**
     * Pickup postcode
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->getData('code_postal');
    }

    /**
     * Pickup city
     *
     * @return string
     */
    public function getLocalite()
    {
        return $this->getData('localite');
    }

    /**
     * Pickup country code
     *
     * @return string
     */
    public function getCodePays()
    {
        return $this->getData('code_pays');
    }

    /**
     * Pickup language
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->getData('langue');
    }

    /**
     * Pickup country
     *
     * @return string
     */
    public function getLibellePays()
    {
        return $this->getData('libelle_pays');
    }

    /**
     * Pickup has parking
     *
     * @return string
     */
    public function getParking()
    {
        return $this->getData('parking');
    }

    /**
     * Pickup identifier
     *
     * @return string
     */
    public function getIdentifiant()
    {
        return $this->getData('identifiant');
    }

    /**
     * Pickup product code
     *
     * @return string
     */
    public function getTypeDePoint()
    {
        return $this->getData('type_de_point');
    }

    /**
     * Pickup network code
     *
     * @return string
     */
    public function getReseau()
    {
        return $this->getData('reseau');
    }

    /**
     * Pickup latitude
     *
     * @return string
     */
    public function getCoordGeolocalisationLatitude()
    {
        return $this->getData('coord_geolocalisation_latitude');
    }

    /**
     * Pickup longitude
     *
     * @return string
     */
    public function getCoordGeolocalisationLongitude()
    {
        return $this->getData('coord_geolocalisation_longitude');
    }

    /**
     * Pickup has handicap access
     *
     * @return int
     */
    public function getAccesPersonneMobiliteReduite()
    {
        return $this->getData('acces_personneMobilite_reduite');
    }

    /**
     * Pickup partial holidays
     *
     * @return string
     */
    public function getCongesPartiel()
    {
        return $this->getData('conges_partiel');
    }

    /**
     * Pickup distance from address in meter
     *
     * @return string
     */
    public function getDistanceEnMetre()
    {
        return $this->getData('distance_en_metre');
    }

    /**
     * Pickup monday opening
     *
     * @return string
     */
    public function getHorairesOuvertureLundi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_lundi')
        );
    }

    /**
     * Pickup tuesday opening
     *
     * @return string
     */
    public function getHorairesOuvertureMardi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_mardi')
        );
    }

    /**
     * Pickup wednesday opening
     *
     * @return string
     */
    public function getHorairesOuvertureMercredi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_mercredi')
        );
    }

    /**
     * Pickup thursday opening
     *
     * @return string
     */
    public function getHorairesOuvertureJeudi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_jeudi')
        );
    }

    /**
     * Pickup friday opening
     *
     * @return string
     */
    public function getHorairesOuvertureVendredi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_vendredi')
        );
    }

    /**
     * Pickup saturday opening
     *
     * @return string
     */
    public function getHorairesOuvertureSamedi()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_samedi')
        );
    }

    /**
     * Pickup sunday opening
     *
     * @return string
     */
    public function getHorairesOuvertureDimanche()
    {
        return $this->formatOpening(
            $this->getData('horaires_ouverture_dimanche')
        );
    }

    /**
     * Pickup localisation tip
     *
     * @return string
     */
    public function getIndiceDeLocalisation()
    {
        return $this->getData('indice_de_localisation');
    }

    /**
     * Pickup activity beginning
     *
     * @return string
     */
    public function getPeriodeActiviteHoraireDeb()
    {
        return $this->getData('periode_activite_horaire_deb');
    }

    /**
     * Pickup activity ending
     *
     * @return string
     */
    public function getPeriodeActiviteHoraireFin()
    {
        return $this->getData('periode_activite_horaire_fin');
    }

    /**
     * Pickup maximum weight
     *
     * @return string
     */
    public function getPoidsMaxi()
    {
        return $this->getData('poids_maxi');
    }

    /**
     * Pickup has handling tool
     *
     * @return string
     */
    public function getLoanOfHandlingTool()
    {
        return $this->getData('loan_of_handling_tool');
    }

    /**
     * Pickup data for pickup shipping label
     *
     * @return string
     */
    public function getDistributionSort()
    {
        return $this->getData('distribution_sort');
    }

    /**
     * Pickup data for pickup shipping label
     *
     * @return string
     */
    public function getLotAcheminement()
    {
        return $this->getData('lot_acheminement');
    }

    /**
     * Pickup data for pickup shipping label
     *
     * @return string
     */
    public function getVersionPlanTri()
    {
        return $this->getData('version_plan_tri');
    }

    /**
     * Pickup Holidays
     *
     * @return bool|object
     */
    public function getListeConges()
    {
        return is_object($this->getData('liste_conges')) ? $this->getData('liste_conges') : false;
    }

    /**
     * Format opening day
     *
     * @param string $day
     * @return string|null
     */
    protected function formatOpening($day)
    {
        $date = trim(
            preg_replace(
                array('/00:00-00:00/', '/:/', '/ /', '/-/'),
                array('', 'h', ' / ', ' - '),
                $day
            ), ' / '
        );
        return $date ?: null;
    }

}