<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Pickup_Collection extends Varien_Data_Collection
{

    /**
     * Retrieve Pickup list
     *
     * @param string $street
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @param float $weight
     *
     * @return $this
     */
    public function getList($street, $city, $postcode, $country, $weight = null)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $apiConfig = $helper->getApiConfig();

        if ($apiConfig['login'] && $apiConfig['password']) {
            try {
                $client = new SoapClient($apiConfig['wsdl'], array('encoding' => 'iso-8859-1'));
                $optionInter = $helper->getConfig(
                    'pickup/country/' . $country . '/option_inter'
                );
                $data = array(
                    'accountNumber' => $apiConfig['login'],
                    'password'      => $apiConfig['password'],
                    'address'       => $street,
                    'zipCode'       => preg_replace('/\s+/', '', $postcode),
                    'city'          => $city,
                    'countryCode'   => $country,
                    'shippingDate'  => date('d/m/Y'),
                    'filterRelay'   => $helper->getFilterRelay($country),
                    'requestId'     => md5(rand(0, 99999)),
                    'optionInter'   => $optionInter ?: 0,
                );

                if ($weight) {
                    if (floatval($weight) > 0) {
                        $weight = round($weight, 0);
                        if ($weight < 1) {
                            $weight = 1;
                        }
                        $data['weight'] = $weight; // Weight in gram
                    }
                }

                $result = $client->findRDVPointRetraitAcheminement($data)->return;

                if ($result->errorMessage == 'Code retour OK') {
                    if (isset($result->listePointRetraitAcheminement)) {
                        foreach ($result->listePointRetraitAcheminement as $data) {
                            /** @var LaPoste_Colissimo_Model_Pickup $object */
                            $object = Mage::getModel('laposte_colissimo/pickup');
                            foreach ($data as $k => $v) {
                                $key = preg_replace_callback(
                                    '/([A-Z])/', create_function('$m', 'return "_".strtolower($m[1]);'), $k
                                );
                                if (is_string($v)) {
                                    $v = utf8_encode($v);
                                }
                                $object->setData(trim($key, '_'), $v);
                            }
                            $this->addItem($object);
                        }
                    }
                } else {
                    Mage::log($result->errorMessage, Zend_Log::ERR, $helper->getLogFile());
                }
            } catch (SoapFault $fault) {
                Mage::log($fault->getMessage(), Zend_Log::ERR, $helper->getLogFile());
            } catch (Exception $e) {
                Mage::log($e->getMessage(), Zend_Log::ERR, $helper->getLogFile());
            }
        }

        return $this;
    }

}