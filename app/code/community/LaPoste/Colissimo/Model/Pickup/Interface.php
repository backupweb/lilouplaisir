<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

interface LaPoste_Colissimo_Model_Pickup_Interface
{

    /**
     * @return string
     */
    public function getNom();

    /**
     * @return string
     */
    public function getAdresse1();

    /**
     * @return string
     */
    public function getAdresse2();

    /**
     * @return string
     */
    public function getAdresse3();

    /**
     * @return string
     */
    public function getCodePostal();

    /**
     * @return string
     */
    public function getLocalite();

    /**
     * @return string
     */
    public function getCodePays();

    /**
     * @return string
     */
    public function getLangue();

    /**
     * @return string
     */
    public function getLibellePays();

    /**
     * @return string
     */
    public function getParking();

    /**
     * @return string
     */
    public function getIdentifiant();

    /**
     * @return string
     */
    public function getTypeDePoint();

    /**
     * @return string
     */
    public function getReseau();

    /**
     * @return string
     */
    public function getCoordGeolocalisationLatitude();

    /**
     * @return string
     */
    public function getCoordGeolocalisationLongitude();

    /**
     * @return int
     */
    public function getAccesPersonneMobiliteReduite();

    /**
     * @return string
     */
    public function getCongesPartiel();

    /**
     * @return string
     */
    public function getDistanceEnMetre();

    /**
     * @return string
     */
    public function getHorairesOuvertureLundi();

    /**
     * @return string
     */
    public function getHorairesOuvertureMardi();

    /**
     * @return string
     */
    public function getHorairesOuvertureMercredi();

    /**
     * @return string
     */
    public function getHorairesOuvertureJeudi();

    /**
     * @return string
     */
    public function getHorairesOuvertureVendredi();

    /**
     * @return string
     */
    public function getHorairesOuvertureSamedi();

    /**
     * @return string
     */
    public function getHorairesOuvertureDimanche();

    /**
     * @return string
     */
    public function getIndiceDeLocalisation();

    /**
     * @return string
     */
    public function getPeriodeActiviteHoraireDeb();

    /**
     * @return string
     */
    public function getPeriodeActiviteHoraireFin();

    /**
     * @return string
     */
    public function getPoidsMaxi();

    /**
     * @return string
     */
    public function getLoanOfHandlingTool();

    /**
     * @return string
     */
    public function getDistributionSort();

    /**
     * @return string
     */
    public function getLotAcheminement();

    /**
     * @return string
     */
    public function getVersionPlanTri();

    /**
     * @return bool|object
     */
    public function getListeConges();

}
