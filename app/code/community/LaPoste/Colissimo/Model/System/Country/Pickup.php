<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_System_Country_Pickup extends LaPoste_Colissimo_Model_System_Country
{

    /**
     * Retrieve country filter
     *
     * @return array
     */
    public function getFilter()
    {
        $config = $this->getHelper()->getConfig('pickup/country');

        $filter = array();

        if (is_array($config)) {
            $filter = array_keys($config);
        }

        return $filter;
    }

}
