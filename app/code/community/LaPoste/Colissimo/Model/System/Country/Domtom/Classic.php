<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_System_Country_Domtom_Classic extends LaPoste_Colissimo_Model_System_Country
{

    /**
     * Retrieve country filter
     *
     * @return array
     */
    public function getFilter()
    {
        return $this->getHelper()->getDomTomCountries();
    }

}
