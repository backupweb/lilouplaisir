<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_System_Filter
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = $this->getHelper();

        return array(
            array('value' => '1', 'label' => $helper->__('Post office and shop')),
            array('value' => '0', 'label' => $helper->__('Post office only')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $helper = $this->getHelper();

        return array(
            '1' => $helper->__('Post office and shop'),
            '0' => $helper->__('Post office only'),
        );
    }

    /**
     * Retrieve Jelper
     *
     * @return LaPoste_Colissimo_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('laposte_colissimo');
    }

}
