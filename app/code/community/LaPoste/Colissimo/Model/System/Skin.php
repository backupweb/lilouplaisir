<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_System_Skin
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $available = $helper->getSkins();
        $skins = array();

        foreach ($available as $code => $data) {
            $skins[] = array(
                'value' => $code,
                'label' => $data['name'],
            );
        }

        return $skins;
    }

}