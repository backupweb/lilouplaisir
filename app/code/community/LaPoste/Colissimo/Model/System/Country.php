<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_System_Country
{

    /**
     * Retrieve countries
     *
     * @return mixed
     */
    public function toOptionArray()
    {
        /** @var Mage_Directory_Model_Resource_Country_Collection $options */
        $options = Mage::getResourceModel('directory/country_collection');

        if (count($this->getFilter())) {
            $options->addCountryCodeFilter($this->getFilter());
        }

        /** @var Mage_Directory_Model_Resource_Country_Collection $data */
        $data = $options->loadData();

        return $data->toOptionArray(false);
    }

    /**
     * Retrieve country filter
     *
     * @return array
     */
    public function getFilter()
    {
        return array();
    }

    /**
     * Retrieve helper
     *
     * @return LaPoste_Colissimo_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('laposte_colissimo');
    }

}
