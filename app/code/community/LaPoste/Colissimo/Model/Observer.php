<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Observer
{

    const LAPOSTE_COLISSIMO_SESSION_ADDRESS = 'laposte_colissimo_address';

    /**
     * Do not save Address in address book if pickup
     * @event customer_address_save_before
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkAddress(Varien_Event_Observer $observer)
    {
        /* @var $address Mage_Customer_Model_Address */
        $address = $observer->getCustomerAddress();

        if ($address->getColissimoPickupId()) {
            $address->isDeleted(true);
        }

        $address->setColissimoPickupId(null);
        $address->setColissimoNetworkCode(null);
        $address->setColissimoProductCode(null);
    }

    /**
     * Save colissimo shipping data (default onepage checkout)
     * @event controller_action_postdispatch_checkout_onepage_saveShippingMethod
     *
     * @param Varien_Event_Observer $observer
     */
    public function setOnepageShippingData(Varien_Event_Observer $observer)
    {
        /** @var Mage_Checkout_OnepageController $action */
        $action  = $observer->getControllerAction();
        $request = $action->getRequest();

        $method = $request->getPost('shipping_method');

        /* @var $shippingAddress Mage_Sales_Model_Order_Address */
        $shippingAddress = $action->getOnepage()->getQuote()->getShippingAddress();

        /* @var $billingAddress Mage_Sales_Model_Order_Address */
        $billingAddress = $action->getOnepage()->getQuote()->getBillingAddress();

        if ($shippingAddress->getColissimoPickupId() && $this->_getAddress()) {
            foreach ($this->_getAddress() as $attribute => $value) {
                $shippingAddress->setData($attribute, $value);
            }
            $this->_unsAddress();
        }

        $this->_resetAddressColissimoData($shippingAddress);
        $this->_resetAddressColissimoData($billingAddress);

        if (!$this->_isColissimo($method)) {
            return;
        }

        $data = new Varien_Object();
        $data->setData($request->getPost());
        $data->setShippingAddress($shippingAddress);
        $data->setBillingAddress($billingAddress);

        $alias = 'laposte_colissimo/method';

        if ($method == 'pickup_colissimo') {
            $this->_saveAddress($shippingAddress, $this->_getSaveAddressData());
            $alias = 'laposte_colissimo/pickup';
        }

        /** @var LaPoste_Colissimo_Model_Method|LaPoste_Colissimo_Model_Pickup $model */
        $model = Mage::getModel($alias);
        $result = $model->saveAddress($data, true);

        if (is_array($result)) {
            $action->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Save colissimo shipping data (admin checkout)
     * @event controller_action_predispatch_adminhtml_sales_order_create_loadblock
     *
     * @param Varien_Event_Observer $observer
     */
    public function setAdminShippingData(Varien_Event_Observer $observer)
    {
        /** @var Mage_Checkout_OnepageController $action */
        $action  = $observer->getControllerAction();
        $request = $action->getRequest();

        $orderData = $request->getPost('order');

        if (!isset($orderData['shipping_method'])) {
            return;
        }

        $method = $orderData['shipping_method'];

        /** @var Mage_Adminhtml_Model_Sales_Order_Create $orderCreate */
        $orderCreate = Mage::getSingleton('adminhtml/sales_order_create');

        /* @var $shippingAddress Mage_Sales_Model_Order_Address */
        $shippingAddress = $orderCreate->getQuote()->getShippingAddress();

        /* @var $billingAddress Mage_Sales_Model_Order_Address */
        $billingAddress = $orderCreate->getQuote()->getBillingAddress();

        $this->_resetAddressColissimoData($shippingAddress);
        $this->_resetAddressColissimoData($billingAddress);

        if (!$this->_isColissimo($method)) {
            return;
        }

        $data = new Varien_Object();
        $data->setData($request->getPost());
        $data->setShippingMethod($method);
        $data->setTelephone($shippingAddress->getTelephone());
        $data->setShippingAddress($shippingAddress);
        $data->setBillingAddress($billingAddress);

        $alias = 'laposte_colissimo/method';

        if ($method == 'pickup_colissimo') {
            $alias = 'laposte_colissimo/pickup';
        }

        /** @var LaPoste_Colissimo_Model_Method|LaPoste_Colissimo_Model_Pickup $model */
        $model = Mage::getModel($alias);
        $model->saveAddress($data);
    }

    /**
     * Set Order Shipping Data (all checkout)
     * @event sales_order_place_before
     *
     * @param Varien_Event_Observer $observer
     */
    public function setOrderShippingData(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        $method = $order->getShippingMethod(true);

        $code = $helper->getProductCode($method['carrier_code']);
        if ($code) {
            $order->getShippingAddress()->setColissimoProductCode($code);
            $order->getBillingAddress()->setColissimoProductCode($code);
        }

        if ($method['carrier_code'] == 'pickup' && $method['method'] == 'colissimo') {
            /** @var LaPoste_Colissimo_Model_Pickup $pickup */
            $pickup = Mage::getModel('laposte_colissimo/pickup');

            if (is_array($pickup->loadFromSession())) {
                $pickupAddress = $pickup->loadFromSession();
                $order->getShippingAddress()->addData($pickupAddress);
                $pickup->deleteFromSession();
            }
        }
    }

    /**
     * Unset colissimo shipping data
     * @event controller_action_postdispatch_checkout_onepage_savebilling
     * @event controller_action_postdispatch_checkout_onepage_saveshipping
     *
     * @param Varien_Event_Observer $observer
     */
    public function unsetOnepageShippingData(Varien_Event_Observer $observer)
    {
        /** @var Mage_Checkout_OnepageController $action */
        $action  = $observer->getControllerAction();

        $this->_resetAddressColissimoData($action->getOnepage()->getQuote()->getShippingAddress());
        $this->_resetAddressColissimoData($action->getOnepage()->getQuote()->getBillingAddress());

        $this->_unsAddress();
    }

    /**
     * Retrieve data to save
     *
     * @return array
     */
    protected function _getSaveAddressData()
    {
        return array(
            'customer_address_id',
            'save_in_address_book',
            'firstname',
            'lastname',
            'company',
            'street',
            'city',
            'postcode',
            'country_id',
            'telephone',
            'fax',
            'region',
            'region_id',
        );
    }

    /**
     * Reset address Colissimo data
     *
     * @param Mage_Sales_Model_Order_Address $address
     */
    protected function _resetAddressColissimoData($address)
    {
        $address->setColissimoProductCode(null);
        $address->setColissimoPickupId(null);
        $address->setColissimoNetworkCode(null);

        $address->save();
    }

    /**
     * Save current shipping address
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @param array $attributes
     */
    protected function _saveAddress($address, $attributes)
    {
        $save = array();
        foreach ($attributes as $attribute) {
            $save[$attribute] = $address->getData($attribute);
        }

        $this->_getSession()->setData(self::LAPOSTE_COLISSIMO_SESSION_ADDRESS, $save);
    }

    /**
     * Retrieve saved address data
     *
     * @return array
     */
    protected function _getAddress()
    {
        return $this->_getSession()->getData(self::LAPOSTE_COLISSIMO_SESSION_ADDRESS);
    }

    /**
     * Unset saved address data
     */
    protected function _unsAddress()
    {
        $this->_getSession()->unsetData(self::LAPOSTE_COLISSIMO_SESSION_ADDRESS);
    }

    /**
     * Check if method is Colissimo
     *
     * @param $method
     * @return bool
     */
    protected function _isColissimo($method)
    {
        return preg_match('/^(.*)_colissimo$/', $method);
    }

    /**
     * Retrieve checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }
}