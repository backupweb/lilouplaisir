<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Method extends Varien_Object
{

    /**
     * Save shipping Address
     *
     * @param Varien_object $request
     * @return array|bool
     */
    public function saveAddress($request)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        /** @var Mage_Sales_Model_Order_Address $shippingAddress */
        $shippingAddress = $request->getShippingAddress();

        /** @var Mage_Sales_Model_Order_Address $billingAddress */
        $billingAddress = $request->getBillingAddress();

        /* Update telephone */
        $telephone = $request->getTelephone();
        if ($telephone) {
            $telephone = $helper->getTelephone(
                preg_replace('/[^0-9+]/', '', $telephone), $shippingAddress->getCountryId(), 'add'
            );
            $shippingAddress->setTelephone($telephone);
        }

        /* Update Product code */
        list($carrierCode, $method) = explode('_', $request->getShippingMethod(), 2);
        $code = $helper->getProductCode($carrierCode);
        if ($code) {
            $shippingAddress->setColissimoProductCode($code);
            $billingAddress->setColissimoProductCode($code);
        }

        /* Save addresses */
        $shippingAddress->save();
        $billingAddress->save();

        /* Valid telephone */
        if ($helper->phoneIsRequired($carrierCode)) {
            if (!$helper->isValidMobileNumber($shippingAddress->getTelephone(), $shippingAddress->getCountryId())) {
                $example = $helper->getConfig(
                    'country_settings/' . $shippingAddress->getCountryId() . '/phone_example'
                );
                return array(
                    'success' => false,
                    'error' => true,
                    'message' => $helper->__(
                        'Please enter a valid mobile phone number. Example: %s', $example
                    )
                );
            }
        }

        return true;
    }

}