<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

abstract class LaPoste_Colissimo_Model_Carrier_Colissimo
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    /**
     * @var string
     */
    protected $_method = 'colissimo';

    /**
     * @var string
     */
    protected $_trackingUrl = 'https://www.laposte.fr/particulier/outils/suivre-vos-envois?code=';

    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigData('active')) {
            return false;
        }

        $shippingWeight = $this->getHelper()->convertWeight($request->getPackageWeight());

        $maxWeight = $this->getHelper()->getConfig(
            $this->_code . '/country/' . $request->getDestCountryId() . '/max_weight'
        );
        if ($shippingWeight > $maxWeight) {
            return false;
        }

        $price = 0;

        if ($request->getFreeShipping() !== true) {
            $price = $this->getShippingPrice(
                $this->getCarrierCode(),
                $request->getDestCountryId(),
                $shippingWeight,
                $request->getStoreId()
            );
        }

        /* @var $result Mage_Shipping_Model_Rate_Result */
        $result = Mage::getModel('shipping/rate_result');

        /* @var $method Mage_Shipping_Model_Rate_Result_Method */
        $method = Mage::getModel('shipping/rate_result_method');

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_method);
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($price);

        Mage::dispatchEvent(
            'laposte_colissimo_append_method_before',
            array(
                'method' => $method,
                'request' => $request
            )
        );

        if (!$method->getHideMethod()) {
            $result->append($method);
        }

        return $result;
    }

    /**
     * @return LaPoste_Colissimo_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('laposte_colissimo');
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array($this->_method => $this->getConfigData('name'));
    }

    /**
     * Retrieve price
     *
     * @param string $shippingCode
     * @param string $countryId
     * @param float $weight
     * @param int $storeId
     * @return float
     */
    public function getShippingPrice($shippingCode, $countryId, $weight, $storeId)
    {
        /** @var LaPoste_Colissimo_Model_Price $model */
        $model = Mage::getModel('laposte_colissimo/price');

        return $model->getShippingPrice($shippingCode, $countryId, $weight, $storeId);
    }

    /**
     * Check if carrier has shipping tracking option available
     *
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Retrieve Tracking Info
     *
     * @param string $tracking
     * @return Mage_Shipping_Model_Tracking_Result_Status $track
     */
    public function getTrackingInfo($tracking)
    {
        /** @var Mage_Shipping_Model_Tracking_Result_Status $track */
        $track = Mage::getModel('shipping/tracking_result_status');

        $track->setUrl($this->_trackingUrl . $tracking)
            ->setTracking($tracking)
            ->setCarrierTitle($this->getConfigData('name'));

        return $track;
    }

    /**
     * Check if carrier has shipping label option available
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        if ($helper->isShippingLabelEnabled()) {
            return true;
        }

        return false;
    }

    /**
     * Do request to shipment (LaPoste_Label module required)
     *
     * @param Mage_Shipping_Model_Shipment_Request $request
     * @return Varien_Object
     */
    public function requestToShipment(Mage_Shipping_Model_Shipment_Request $request)
    {
        /** @var LaPoste_Label_Model_Label $label */
        $label = Mage::getModel('laposte_label/label');

        return $label->doShipmentRequest($request);
    }

    /**
     * Check available Ship Countries
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return $this|bool|false|Mage_Core_Model_Abstract
     */
    public function checkAvailableShipCountries(Mage_Shipping_Model_Rate_Request $request)
    {
        $countryId = $this->getHelper()->getCountry($request->getDestCountryId(), $request->getDestPostcode());
        $request->setDestCountryId($countryId);

        return parent::checkAvailableShipCountries($request);
    }

}