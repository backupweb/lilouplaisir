<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Carrier_Colissimo_International extends LaPoste_Colissimo_Model_Carrier_Colissimo
{

    /**
     * @var string
     */
    protected $_code = 'international';

}