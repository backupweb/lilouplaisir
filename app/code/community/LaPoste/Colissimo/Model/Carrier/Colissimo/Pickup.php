<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Carrier_Colissimo_Pickup extends LaPoste_Colissimo_Model_Carrier_Colissimo
{

    /**
     * @var string
     */
    protected $_code = 'pickup';

    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (preg_match('/^AD/', $request->getDestPostcode())) { // Exclude Andorre
            return false;
        }

        return parent::collectRates($request);
    }
}