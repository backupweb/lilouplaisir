<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Directory_Resource_Region_Collection
    extends Mage_Directory_Model_Resource_Region_Collection
{

    /**
     * Get collection size
     *
     * @override : fix state/province required for Magento < 1.7
     *
     * @return int
     */
    public function getSize()
    {
        if (version_compare(Mage::getVersion(), '1.7', '<')) {
            return 0;
        }

        return parent::getSize();
    }

}
