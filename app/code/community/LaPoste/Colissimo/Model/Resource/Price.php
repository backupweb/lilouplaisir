<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Resource_Price extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('laposte_colissimo/price', 'price_id');
    }

    /**
     * Quick load model value
     *
     * @param int $entityId
     * @param string $field
     * @return mixed
     */
    public function loadValue($entityId, $field)
    {
        $adapter = $this->_getReadAdapter();

        return $adapter->fetchOne(
            $adapter->select()
                ->from($this->getMainTable(), array($field))
                ->where($this->getIdFieldName() . ' = ?', $entityId)
        );
    }

    /**
     * Mass import prices
     *
     * @param array $data
     * @param bool $erase
     * @return $this
     */
    public function massImport($data, $erase)
    {
        $adapter = $this->_getWriteAdapter();

        if ($erase) {
            $adapter->truncateTable($this->getMainTable());
        }

        $adapter->insertMultiple($this->getMainTable(), $data);

        $this->updateWeight();

        return $this;
    }

    /**
     * Retrieve price
     *
     * @param string $shippingCode
     * @param string $countryId
     * @param float $weight
     * @param int $storeId
     * @return float
     */
    public function getShippingPrice($shippingCode, $countryId, $weight, $storeId)
    {
        $adapter = $this->_getReadAdapter();

        return $adapter->fetchOne(
            $adapter->select()
                ->from($this->getMainTable(), array('price'))
                ->where('shipping_method = ?', $shippingCode)
                ->where('country_id = ?', $countryId)
                ->where('weight_from <= ' . $weight . ' OR weight_from IS NULL')
                ->where('weight_to > ' . $weight . ' OR weight_to IS NULL')
                ->where('store_id = ' . $storeId . ' OR store_id = 0')
                ->order('store_id DESC')
                ->order('weight_from ASC')
                ->limit(1)
        );
    }

    /**
     * Retrieve configured countries
     *
     * @return array
     */
    public function getCountries()
    {
        $adapter = $this->_getReadAdapter();

        return $adapter->fetchCol(
            $adapter->select()->from($this->getMainTable(), array('country_id'))->group('country_id')
        );
    }

    /**
     * Update weight data with NULL value
     *
     * @return $this
     */
    protected function updateWeight()
    {
        $adapter = $this->_getWriteAdapter();

        $columns = array('weight_from', 'weight_to');

        foreach ($columns as $olumn) {
            $adapter->update($this->getMainTable(), array($olumn => null), $olumn . ' = "0.0000"');
        }

        return $this;
    }

    /**
     * Perform actions after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return $this
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $this->updateWeight();

        return $this;
    }

}