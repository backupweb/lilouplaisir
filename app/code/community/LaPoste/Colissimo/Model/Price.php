<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Model_Price extends Mage_Core_Model_Abstract
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('laposte_colissimo/price');
    }

    /**
     * Quick load model value
     *
     * @param int $entityId
     * @param string $field
     * @return mixed
     */
    public function loadValue($entityId, $field)
    {
        /* @var $resource LaPoste_Colissimo_Model_Resource_Price */
        $resource = $this->_getResource();

        return $resource->loadValue($entityId, $field);
    }

    /**
     * Retrieve price
     *
     * @param string $shippingCode
     * @param string $countryId
     * @param float $weight
     * @param int $storeId
     * @return float
     */
    public function getShippingPrice($shippingCode, $countryId, $weight, $storeId)
    {
        /* @var $resource LaPoste_Colissimo_Model_Resource_Price */
        $resource = $this->_getResource();

        return $resource->getShippingPrice($shippingCode, $countryId, $weight, $storeId) ?: 0;
    }

    /**
     * Retrieve configured countries
     *
     * @return array
     */
    public function getCountries()
    {
        /* @var $resource LaPoste_Colissimo_Model_Resource_Price */
        $resource = $this->_getResource();

        return $resource->getCountries() ?: array();
    }

    /**
     * Mass import prices
     *
     * @param array $data
     * @param bool $erase
     */
    public function massImport($data, $erase = true)
    {
        /* @var $resource LaPoste_Colissimo_Model_Resource_Price */
        $resource = $this->_getResource();

        $resource->massImport($data, $erase);
    }

    /**
     * Import CSV file
     *
     * @param string $file
     * @return array
     */
    public function importCsv($file)
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        try {
            $csv = new Varien_File_Csv();
            $csv->setDelimiter(',');
            $csv->setEnclosure('"');

            $data = $csv->getData($file);

            if (count($data)) {
                $header = $data[0];
                $required = $helper->getImportColumnsRequired();

                foreach ($required as $column) {
                    if (!in_array($column, $header)) {
                        return array(
                            'error'   => true,
                            'message' => $helper->__('%s column is required', $column),
                        );
                    }
                }

                unset($data[0]);

                foreach ($data as $key => $row) {
                    $data[] = array_intersect_key(
                        array_combine($header, $row),
                        array_fill_keys($required, null)
                    );
                    unset($data[$key]);
                }

                $this->massImport($data);

                return array(
                    'error'   => false,
                    'message' => $helper->__('%s row(s) have been added', count($data)),
                );
            }
        } catch (Exception $e) {
            return array(
                'error'   => true,
                'message' => $e->getMessage(),
            );
        }

        return array(
            'error'   => true,
            'message' => $helper->__('Nothing to import'),
        );
    }

}