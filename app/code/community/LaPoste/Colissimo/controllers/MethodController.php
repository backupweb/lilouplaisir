<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_MethodController extends Mage_Core_Controller_Front_Action
{

    /**
     * Load Colissimo Simple form
     */
    public function formAction()
    {
        $this->_expireAjax();

        $this->loadLayout(false);

        /** @var LaPoste_Colissimo_Block_Frontend_Colissimo_Method_Form $block */
        $block = $this->getLayout()->getBlock('colissimo.method.form');

        if ($block) {
            $block->addData($this->_getData());
        }

        $this->renderLayout();
    }

    /**
     * Retrieve current shipping address
     *
     * @return array
     */
    protected function _getData()
    {
        $params  = $this->getRequest()->getParams();

        $data = array(
            'shipping_method' => isset($params['shipping_method']) ? $params['shipping_method'] : null,
            'context'         => isset($params['context']) ? $params['context'] : null,
        );

        return $data;
    }

    /**
     * Retrieve Onepage Session
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * Send error if session expire
     *
     * @return $this
     */
    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();

        return $this;
    }

    /**
     * Validate ajax request and redirect on failure
     *
     * @return bool
     */
    protected function _expireAjax()
    {
        if (!$this->_getCheckout()->getQuote()->hasItems()
            || $this->_getCheckout()->getQuote()->getHasError()
        ) {
            $this->_ajaxRedirectResponse();
            return true;
        }

        return false;
    }

}