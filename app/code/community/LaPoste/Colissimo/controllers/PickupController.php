<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_PickupController extends Mage_Core_Controller_Front_Action
{

    /**
     * Load Colissimo Pickup
     */
    public function listAction()
    {
        $this->_expireAjax();

        $this->loadLayout(false);

        /** @var LaPoste_Colissimo_Block_Frontend_Colissimo_Pickup_List $block */
        $block = $this->getLayout()->getBlock('colissimo.pickup.list');

        if ($block) {
            $block->addData($this->_getData());
        }

        $this->renderLayout();
    }

    /**
     * Retrieve current shipping address
     *
     * @return array
     */
    protected function _getData()
    {
        $params  = $this->getRequest()->getParams();
        $address = $this->_getCheckout()->getQuote()->getShippingAddress();

        $data = array(
            'address'    => isset($params['address']) ? $params['address'] : $address->getStreet1(),
            'postcode'   => isset($params['postcode']) ? $params['postcode'] : $address->getPostcode(),
            'city'       => isset($params['city']) ? $params['city'] : $address->getCity(),
            'country_id' => isset($params['country_id']) ? $params['country_id'] : $address->getCountryId(),
            'context'    => isset($params['context']) ? $params['context'] : null,
        );

        return $data;
    }

    /**
     * Retrieve Onepage Session
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * Send error if session expire
     *
     * @return $this
     */
    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();

        return $this;
    }

    /**
     * Validate ajax request and redirect on failure
     *
     * @return bool
     */
    protected function _expireAjax()
    {
        if (!$this->_getCheckout()->getQuote()->hasItems()
            || $this->_getCheckout()->getQuote()->getHasError()
        ) {
            $this->_ajaxRedirectResponse();
            return true;
        }

        return false;
    }

    /**
     * Save action, used only with specific checkout
     */
    public function saveAction()
    {
        Mage::dispatchEvent(
            'laposte_colissimo_save_method',
            array('controller_action' => $this)
        );
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }
}