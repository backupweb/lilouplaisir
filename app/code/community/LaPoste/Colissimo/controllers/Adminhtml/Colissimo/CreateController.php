<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Adminhtml_Colissimo_CreateController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Load Action
     */
    public function listAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    /**
     * Acl check for admin
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/create');
    }
}