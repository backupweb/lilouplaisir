<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Adminhtml_Colissimo_PriceController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Price Action
     */
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * New Action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit Action
     */
    public function editAction()
    {
        $priceId = $this->getRequest()->getParam('price_id');

        /** @var $model LaPoste_Colissimo_Model_Price */
        $model = Mage::getModel('laposte_colissimo/price');

        if ($priceId) {
            $model->load($priceId);
            if (!$model->getId()) {
                $this->_getAdminSession()->addError(
                    Mage::helper('laposte_colissimo')->__('This price no longer exists.')
                );
                $this->_redirect('*/*/');
                return false;
            }
        }

        Mage::register('colissimo_price', $model);

        $this->_initAction()->renderLayout();
    }

    /**
     * Save Action
     */
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        $data = new Varien_Object($postData);

        /* @var $model LaPoste_Colissimo_Model_Price */
        $model = Mage::getModel('laposte_colissimo/price');

        if ($data->getId()) {
            $model->load($data->getId());
        }

        if (isset($postData[$postData['shipping_method']])) {
            $postData['country_id'] = $postData[$postData['shipping_method']]['country_id'];
        }

        if (!$postData['weight_to']) {
            $postData['weight_to'] = new Zend_Db_Expr("NULL");
        }

        if (!$postData['weight_from']) {
            $postData['weight_from'] = new Zend_Db_Expr("NULL");
        }

        $model->setData($postData);

        try {
            $model->save();
            $this->_getAdminSession()->addSuccess(
                Mage::helper('laposte_colissimo')->__('Price has been saved')
            );
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e,
                Mage::helper('laposte_colissimo')->__(
                    'An error occurred while saving the price : %s', $e->getMessage()
                )
            );
        }

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('price_id' => $model->getId()));
            return;
        }

        $this->_redirect('*/*/');
    }

    /**
     * Delete Action
     */
    public function deleteAction()
    {
        $priceId = $this->getRequest()->getParam('price_id');

        if ($priceId) {
            try {
                /* @var $model LaPoste_Colissimo_Model_Price */
                $model = Mage::getModel('laposte_colissimo/price');

                $model->load($priceId);
                $model->delete();

                $this->_getAdminSession()->addSuccess(
                    Mage::helper('laposte_colissimo')->__(
                        'The price has been deleted.'
                    )
                );
                $this->_redirect('*/*/');
                return false;
            } catch (Exception $e) {
                $this->_getAdminSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('price_id' => $priceId));
                return false;
            }
        }

        $this->_getAdminSession()->addError(
            Mage::helper('laposte_colissimo')->__(
                'Unable to find a price to delete.'
            )
        );

        $this->_redirect('*/*/');
    }

    /**
     * Duplicate action
     *
     * @return bool
     */
    public function duplicateAction()
    {
        $priceId = $this->getRequest()->getParam('price_id');

        if ($priceId) {
            try {
                /* @var $model LaPoste_Colissimo_Model_Price */
                $model = Mage::getModel('laposte_colissimo/price');

                $model->load($priceId);
                $model->setId(null);
                $model->save();

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                $this->_getAdminSession()->addError($e->getMessage());
                $this->_redirect('*/*/');
                return false;
            }
        }
    }

    /**
     * Update Weight Action
     */
    public function saveGridDataAction()
    {
        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');

        $priceId = $this->getRequest()->getParam('price_id');
        $field   = $this->getRequest()->getParam('field');
        $value   = $this->getRequest()->getParam('value');

        if ($priceId && $field) {
            /* @var $model LaPoste_Colissimo_Model_Price */
            $model = Mage::getModel('laposte_colissimo/price');
            $model->load($priceId);

            $model->setData($field, $value)->save();

            $this->getResponse()->setBody(
                $helper->jsonEncode(
                    array(
                        'value' => $model->loadValue($model->getId(), $field)
                    )
                )
            );
        }
    }

    /**
     * Import Action
     */
    public function importAction()
    {
        /** @var LaPoste_Colissimo_Helper_Data $helper */
        $helper = Mage::helper('laposte_colissimo');

        if (!isset($_FILES['file'])) {
            $this->_getSession()->addError($helper->__('Please select file'));
            $this->_redirect('*/*/');
            return false;
        }

        $result = $this->_addFile($_FILES['file'], array('csv', 'txt'));

        if (!$result) {
            $this->_redirect('*/*/');
            return false;
        }

        if (!$result->getUploadedFileName()) {
            $this->_redirect('*/*/');
            return false;
        }

        $file = $helper->getImportDir() . DS . $result->getUploadedFileName();

        /** @var LaPoste_Colissimo_Model_Price $price */
        $price = Mage::getModel('laposte_colissimo/price');

        $result = $price->importCsv($file);

        if ($result['error']) {
            $this->_getSession()->addError($result['message']);
        } else {
            $this->_getSession()->addSuccess($result['message']);
        }

        $this->_redirect('*/*/');
    }

    /**
     * Mass Delete Action
     */
    public function massDeleteAction()
    {
        $priceIds = $this->getRequest()->getParam('price_ids');
        if (!is_array($priceIds)) {
            $this->_getSession()->addError(
                Mage::helper('laposte_colissimo')->__('Please select price(s).')
            );
        } else {
            if (!empty($priceIds)) {
                try {
                    foreach ($priceIds as $priceId) {
                        /* @var $model LaPoste_Colissimo_Model_Price */
                        $model = Mage::getModel('laposte_colissimo/price');

                        $model->load($priceId);
                        $model->delete();
                    }
                    $this->_getSession()->addSuccess(
                        Mage::helper('laposte_colissimo')->__(
                            'Total of %s record(s) have been deleted.', count($priceIds)
                        )
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout();

        $this->_title(Mage::helper('laposte_colissimo')->__('Colissimo'))
             ->_title(Mage::helper('laposte_colissimo')->__('Prices'));

        $this->_setActiveMenu('sales');

        return $this;
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        /* @var $grid LaPoste_Colissimo_Block_Adminhtml_Price_Grid */
        $grid = $this->getLayout()->createBlock('laposte_colissimo/adminhtml_colissimo_price_grid');
        $this->_prepareDownloadResponse('laposte_colissimo_prices.csv', $grid->getCsvFile());
    }

    /**
     * Export order grid to XML format
     */
    public function exportXmlAction()
    {
        /* @var $grid LaPoste_Colissimo_Block_Adminhtml_Price_Grid */
        $grid = $this->getLayout()->createBlock('laposte_colissimo/adminhtml_colissimo_price_grid');
        $this->_prepareDownloadResponse('laposte_colissimo_prices.xml', $grid->getXml());
    }

    /**
     * Export order grid to XML Excel format
     */
    public function exportExcelAction()
    {
        /* @var $grid LaPoste_Colissimo_Block_Adminhtml_Price_Grid */
        $grid = $this->getLayout()->createBlock('laposte_colissimo/adminhtml_colissimo_price_grid');
        $this->_prepareDownloadResponse('laposte_colissimo_prices.xml', $grid->getExcelFile());
    }

    /**
     * Add File
     *
     * @param array $data
     * @param array $allowed
     *
     * @return Mage_Core_Model_File_Uploader|bool
     */
    protected function _addFile($data, $allowed)
    {
        try {
            $file = new Mage_Core_Model_File_Uploader($data);

            $file->setAllowedExtensions($allowed);
            $file->setAllowRenameFiles(true);
            $file->setAllowCreateFolders(true);
            $file->setFilesDispersion(false);

            /* @var $helper LaPoste_Colissimo_Helper_Data */
            $helper = Mage::helper('laposte_colissimo');

            $fileName = 'colissimo-price-' . Mage::getModel('core/date')->date('YmdHis') . '.csv';

            $file->save($helper->getImportDir(), $fileName);

            return $file;

        } catch (Exception $e) {
            $this->_getAdminSession()->addError($e->getMessage());
        }

        return false;
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/colissimo/price');
    }

    /**
     * Retrieve Admin Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getAdminSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

}