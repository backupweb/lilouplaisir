<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Colissimo_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Retrieve configuration value
     *
     * @param string $path
     * @return array|string|bool
     */
    public function getConfig($path)
    {
        $config = array(
            'homecl' => array(
                'product_code' => 'DOM',
                'country' => array(
                    'FR' => array(
                        'max_weight' => 30000,
                    ),
                    'BE' => array(
                        'max_weight' => 30000,
                    ),
                    'CH' => array(
                        'max_weight' => 30000,
                    ),
                ),
            ),
            'homesi' => array(
                'product_code' => 'DOS',
                'country' => array(
                    'FR' => array(
                        'max_weight' => 30000,
                    ),
                    'BE' => array(
                        'max_weight' => 30000,
                    ),
                    'NL' => array(
                        'max_weight' => 30000,
                    ),
                    'DE' => array(
                        'max_weight' => 30000,
                    ),
                    'GB' => array(
                        'max_weight' => 30000,
                    ),
                    'LU' => array(
                        'max_weight' => 30000,
                    ),
                    'ES' => array(
                        'max_weight' => 30000,
                    ),
                    'AT' => array(
                        'max_weight' => 30000,
                    ),
                    'EE' => array(
                        'max_weight' => 30000,
                    ),
                    'HU' => array(
                        'max_weight' => 30000,
                    ),
                    'LV' => array(
                        'max_weight' => 30000,
                    ),
                    'LT' => array(
                        'max_weight' => 30000,
                    ),
                    'CZ' => array(
                        'max_weight' => 30000,
                    ),
                    'SK' => array(
                        'max_weight' => 30000,
                    ),
                    'SI' => array(
                        'max_weight' => 30000,
                    ),
                    'CH' => array(
                        'max_weight' => 30000,
                    ),
                    'PT' => array(
                        'max_weight' => 30000,
                    ),
                    'BG' => array(
                        'max_weight' => 30000,
                    ),
                    'CY' => array(
                        'max_weight' => 30000,
                    ),
                    'HR' => array(
                        'max_weight' => 30000,
                    ),
                    'DK' => array(
                        'max_weight' => 30000,
                    ),
                    'FI' => array(
                        'max_weight' => 30000,
                    ),
                    'GR' => array(
                        'max_weight' => 30000,
                    ),
                    'IE' => array(
                        'max_weight' => 30000,
                    ),
                    'IS' => array(
                        'max_weight' => 30000,
                    ),
                    'IT' => array(
                        'max_weight' => 30000,
                    ),
                    'MT' => array(
                        'max_weight' => 30000,
                    ),
                    'NO' => array(
                        'max_weight' => 30000,
                    ),
                    'PL' => array(
                        'max_weight' => 30000,
                    ),
                    'SE' => array(
                        'max_weight' => 30000,
                    ),
                    'RO' => array(
                        'max_weight' => 30000,
                    ),
                ),
            ),
            'domtomcl' => array(
                'product_code' => 'COM',
                'country' => '::_domtomCountries',
            ),
            'domtomsi' => array(
                'product_code' => 'CDS',
                'country' => '::_domtomCountries',
            ),
            'international' => array(
                'product_code' => 'COLI',
                'country' => '::_internationalCountries',
            ),
            'pickup' => array(
                'country' => array(
                    'FR' => array(
                        'option_inter' => '0',
                        'max_weight'   => 30000,
                    ),
                    'BE' => array(
                        'option_inter' => '2',
                        'max_weight'   => 20000,
                    ),
                    'NL' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'DE' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'GB' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'LU' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'ES' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'PT' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'AT' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'LT' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'LV' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'EE' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'SE' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                    'PL' => array(
                        'option_inter' => '1',
                        'max_weight'   => 20000,
                    ),
                ),
            ),
            'country_settings' => array(
                'FR' => array(
                    'phone_regex'   => '/^0(6|7)[0-9]{8}$/',
                    'phone_example' => '0610203040',
                    'phone_code'    => '',
                ),
                'BE' => array(
                    'phone_regex'   => '/^\+324[0-9]{8}$/',
                    'phone_example' => '+32 410203040',
                    'phone_code'    => '+32',
                ),
                'NL' => array(
                    'phone_regex'   => '/^\+316[0-9]{8}$/',
                    'phone_example' => '+31 610203040',
                    'phone_code'    => '+31',
                ),
                'DE' => array(
                    'phone_regex'   => '/^\+491[5-7]{1}[0-9]{7,9}$/',
                    'phone_example' => '+49 161020304',
                    'phone_code'    => '+49',
                ),
                'GB' => array(
                    'phone_regex'   => '/^\+447[3-9]{1}[0-9]{8}$/',
                    'phone_example' => '+44 7510203040',
                    'phone_code'    => '+44',
                ),
                'LU' => array(
                    'phone_regex'   => '/^\+3526[0-9]{8}$/',
                    'phone_example' => '+352 621203040',
                    'phone_code'    => '+352',
                ),
                'ES' => array(
                    'phone_regex'   => '/^\+346[0-9]{8}$/',
                    'phone_example' => '+34 610203040',
                    'phone_code'    => '+34',
                ),
                'AT' => array(
                    'phone_regex'   => '/^\+43/',
                    'phone_example' => '+43 Z XXX XXX',
                    'phone_code'    => '+43',
                ),
                'EE' => array(
                    'phone_regex'   => '/^\+372/',
                    'phone_example' => '+372 Z XXX XXX',
                    'phone_code'    => '+372',
                ),
                'HU' => array(
                    'phone_regex'   => '/^\+36/',
                    'phone_example' => '+36 Z XXX XXX',
                    'phone_code'    => '+36',
                ),
                'LV' => array(
                    'phone_regex'   => '/^\+371/',
                    'phone_example' => '+371 Z XXX XXX',
                    'phone_code'    => '+371',
                ),
                'LT' => array(
                    'phone_regex'   => '/^\+370/',
                    'phone_example' => '+370 Z XXX XXX',
                    'phone_code'    => '+370',
                ),
                'CZ' => array(
                    'phone_regex'   => '/^\+420/',
                    'phone_example' => '+420 Z XXX XXX',
                    'phone_code'    => '+420',
                ),
                'SK' => array(
                    'phone_regex'   => '/^\+421/',
                    'phone_example' => '+421 Z XXX XXX',
                    'phone_code'    => '+421',
                ),
                'SI' => array(
                    'phone_regex'   => '/^\+386/',
                    'phone_example' => '+386 Z XXX XXX',
                    'phone_code'    => '+386',
                ),
                'CH' => array(
                    'phone_regex'   => '/^\+41/',
                    'phone_example' => '+41 Z XXX XXX',
                    'phone_code'    => '+41',
                ),
                'PT' => array(
                    'phone_regex'   => '/^\+351/',
                    'phone_example' => '+351 Z XXX XXX',
                    'phone_code'    => '+351',
                ),
                'BG' => array(
                    'phone_regex'   => '/^\+359/',
                    'phone_example' => '+359 Z XXX XXX',
                    'phone_code'    => '+359',
                ),
                'CY' => array(
                    'phone_regex'   => '/^\+357/',
                    'phone_example' => '+357 Z XXX XXX',
                    'phone_code'    => '+357',
                ),
                'HR' => array(
                    'phone_regex'   => '/^\+385/',
                    'phone_example' => '+385 Z XXX XXX',
                    'phone_code'    => '+385',
                ),
                'DK' => array(
                    'phone_regex'   => '/^\+45/',
                    'phone_example' => '+45 Z XXX XXX',
                    'phone_code'    => '+45',
                ),
                'FI' => array(
                    'phone_regex'   => '/^\+358/',
                    'phone_example' => '+358 Z XXX XXX',
                    'phone_code'    => '+358',
                ),
                'GR' => array(
                    'phone_regex'   => '/^\+30/',
                    'phone_example' => '+30 Z XXX XXX',
                    'phone_code'    => '+30',
                ),
                'IE' => array(
                    'phone_regex'   => '/^\+353/',
                    'phone_example' => '+353 Z XXX XXX',
                    'phone_code'    => '+353',
                ),
                'IS' => array(
                    'phone_regex'   => '/^\+354/',
                    'phone_example' => '+354 Z XXX XXX',
                    'phone_code'    => '+354',
                ),
                'IT' => array(
                    'phone_regex'   => '/^\+39/',
                    'phone_example' => '+39 Z XXX XXX',
                    'phone_code'    => '+39',
                ),
                'MT' => array(
                    'phone_regex'   => '/^\+356/',
                    'phone_example' => '+356 Z XXX XXX',
                    'phone_code'    => '+356',
                ),
                'NO' => array(
                    'phone_regex'   => '/^\+47/',
                    'phone_example' => '+47 Z XXX XXX',
                    'phone_code'    => '+47',
                ),
                'PL' => array(
                    'phone_regex'   => '/^\+48/',
                    'phone_example' => '+48 Z XXX XXX',
                    'phone_code'    => '+48',
                ),
                'SE' => array(
                    'phone_regex'   => '/^\+46/',
                    'phone_example' => '+46 Z XXX XXX',
                    'phone_code'    => '+46',
                ),
                'RO' => array(
                    'phone_regex'   => '/^\+40/',
                    'phone_example' => '+40 Z XXX XXX',
                    'phone_code'    => '+40',
                ),
            ),
        );

        $object = new Varien_Object();
        $object->setData($config);

        Mage::dispatchEvent('laposte_colissimo_load_config_before', array('path' => $path, 'config' => $object));

        $config = $object->getData();

        $keys = explode('/', $path);

        $skip = false;
        foreach ($keys as $i => $key) {
            if ($skip) {
                $skip = false;
                continue;
            }
            if (isset($config[$key])) {
                $config = $config[$key];
                if (is_string($config)) {
                    if (preg_match('/^::/', $config)) {
                        $method = preg_replace('/^::/', '', $config);
                        $config = $this->$method();
                        $skip = true;
                    }
                }
            } else {
                $config = false;
                break;
            }
        }

        Mage::dispatchEvent('laposte_colissimo_load_config_after', array('path' => $path, 'config' => $config));

        return $config;
    }

    /**
     * Retrieve International country
     *
     * @return array
     */
    protected function _internationalCountries()
    {
        return array(
            'max_weight' => 30000,
        );
    }

    /**
     * Retrieve Dom-Tom country
     *
     * @return array
     */
    protected function _domtomCountries()
    {
        return array(
            'max_weight' => 30000,
        );
    }

    /**
     * Mobile phone number validation
     *
     * @param string $phone
     * @param string $countryId
     * @return bool
     */
    public function isValidMobileNumber($phone, $countryId)
    {
        $regex = $this->getConfig('country_settings/' . $countryId . '/phone_regex');

        $valid = true;

        if ($regex) {
            if (!preg_match($regex, $phone)) {
                $valid = false;
            }
        }

        return $valid;
    }

    /**
     * Retrieve phone number with or without code
     *
     * @param string $telephone
     * @param string $countryId
     * @param string $type
     * @return string
     */
    public function getTelephone($telephone, $countryId, $type)
    {
        $phoneCode = $this->getPhoneCode($countryId);

        if ($phoneCode) {
            if ($type == 'add') {
                $telephone = $phoneCode . ($phoneCode ? ltrim($telephone, 0) : $telephone);
            }
            if ($type == 'remove') {
                $telephone = str_replace($phoneCode, '', $telephone);
            }
        }

        return $telephone;
    }

    /**
     * Retrieve phone code
     *
     * @param string $countryId
     * @return string
     */
    public function getPhoneCode($countryId)
    {
        return $this->getConfig('country_settings/' . $countryId . '/phone_code');
    }

    /**
     * Retrieve country
     *
     * @param string $countryId
     * @param string $postcode
     * @return string
     */
    public function getCountry($countryId, $postcode = null)
    {
        if ($countryId == 'MC') { // Monaco
            $countryId = 'FR';
        }

        if ($countryId == 'AD') { // Andorre
            $countryId = 'FR';
        }

        if ($postcode) {
            if ($countryId == 'FR') {
                $countryId = $this->getDomTomCountry($postcode);
            }
        }

        return $countryId;
    }

    /**
     * Retrieve Dom Tom Country with postcode
     *
     * @param string $postcode
     * @return string
     */
    public function getDomTomCountry($postcode)
    {
        $countryId = 'FR';
        $postcodes = $this->getDomTomPostcodes();

        $postcode = preg_replace('/\s+/', '', $postcode);
        foreach ($postcodes as $code => $regex) {
            if (preg_match($regex, $postcode)) {
                $countryId = $code;
                break;
            }
        }
        return $countryId;
    }

    /**
     * Retrieve Dom-Tom countries code ISO-2
     *
     * @param bool $international
     * @return array
     */
    public function getHomeCountries($international = false)
    {
        $countries = array('FR', 'BE', 'CH');

        if ($international) {
            $countries = array(
                'FR', // France
                'BE', // Belgium
                'NL', // Netherlands
                'DE', // Germany
                'GB', // Great Britain
                'LU', // Luxembourg
                'ES', // Spain
                'AT', // Austria
                'EE', // Estonia
                'HU', // Hungary
                'LV', // Latvia
                'LT', // Lithuania
                'CZ', // Czech republic
                'SK', // Slovakia
                'SI', // Slovenia
                'CH', // Switzerland
                'PT', // Portugal
                'BG', // Bulgaria
                'CY', // Cyprus
                'HR', // Croatia
                'DK', // Denmark
                'FI', // Finland
                'GR', // Greece
                'IE', // Ireland
                'IS', // Iceland
                'IT', // Italy
                'MT', // Malta
                'NO', // Norway
                'PL', // Poland
                'SE', // Sweden
                'RO', // Romania
            );
        }

        return $countries;
    }

    /**
     * Retrieve Dom-Tom countries code ISO-2
     *
     * @return array
     */
    public function getDomTomCountries()
    {
        return array(
            'GP', // Guadeloupe
            'MQ', // Martinique
            'GF', // Guyane
            'RE', // La réunion
            'PM', // St-Pierre-et-Miquelon
            'YT', // Mayotte
            'TF', // Terres-Australes
            'WF', // Wallis-et-Futuna
            'PF', // Polynésie Française
            'NC', // Nouvelle-Calédonie
            'BL', // Saint-Barthélemy
            'MF', // Saint-Martin (partie française)
        );
    }

    /**
     * Retrieve Dom-Tom postcodes
     *
     * @return array
     */
    public function getDomTomPostcodes()
    {
        return array(
            'BL' => '/^97133$/', // Saint-Barthélemy
            'MF' => '/^97150$/', // Saint-Martin (partie française)
            'GP' => '/^971[0-9]{2}$/', // Guadeloupe
            'MQ' => '/^972[0-9]{2}$/', // Martinique
            'GF' => '/^973[0-9]{2}$/', // Guyane
            'RE' => '/^974[0-9]{2}$/', // La réunion
            'PM' => '/^975[0-9]{2}$/', // St-Pierre-et-Miquelon
            'YT' => '/^976[0-9]{2}$/', // Mayotte
            'TF' => '/^984[0-9]{2}$/', // Terres-Australes
            'WF' => '/^986[0-9]{2}$/', // Wallis-et-Futuna
            'PF' => '/^987[0-9]{2}$/', // Polynésie Française
            'NC' => '/^988[0-9]{2}$/', // Nouvelle-Calédonie
        );
    }

    /**
     * Retrieve API configuration
     *
     * @return array
     */
    public function getApiConfig()
    {
        return array(
            'wsdl'     => 'https://ws.colissimo.fr/pointretrait-ws-cxf/PointRetraitServiceWS/2.0?wsdl',
            'login'    => Mage::getStoreConfig('carriers/pickup/account_number'),
            'password' => Mage::getStoreConfig('carriers/pickup/account_password'),
        );
    }

    /**
     * Retrieve log file
     *
     * @return string
     */
    public function getLogFile()
    {
        return 'colissimo.log';
    }

    /**
     * Convert Weight with rate
     *
     * @param float $weight
     * @return int
     */
    public function convertWeight($weight)
    {
        $rate = Mage::getStoreConfig('shipping/colissimo/weight_rate') ?: 1;

        return $weight * $rate;
    }

    /**
     * Retrieve skin anme
     *
     * @return string
     */
    public function getSkinName()
    {
        return Mage::getStoreConfig('carriers/pickup/skin');
    }

    /**
     * Retrieve skin data
     *
     * @return array
     */
    public function getSkinData()
    {
        $skin = Mage::getConfig()->getNode('colissimo/skins/' . $this->getSkinName());

        $data = array();

        if ($skin) {
            $data = $skin->asArray();
        }

        return $data;
    }

    /**
     * Retrieve configured skins
     *
     * @return Mage_Core_Model_Config_Element
     */
    public function getSkins()
    {
        return Mage::getConfig()->getNode('colissimo/skins')->asArray();
    }

    /**
     * Check if we can show map
     *
     * @return int
     */
    public function canShowMap()
    {
        return Mage::getStoreConfigFlag('carriers/pickup/google_show_map') ? 1 : 0;
    }

    /**
     * Retrieve Google Map API key
     *
     * @return string
     */
    public function getMapApiKey()
    {
        return Mage::getStoreConfig('carriers/pickup/google_api_key');
    }

    /**
     * Retrieve filter relay
     *
     * @param string $countryId
     * @return string
     */
    public function getFilterRelay($countryId)
    {
        if ($countryId == 'FR') {
            return Mage::getStoreConfig('carriers/pickup/filter_relay');
        }

        return '1';
    }

    /**
     * Retrieve if method is active
     *
     * @param string $method
     * @return bool
     */
    public function isActive($method)
    {
        return Mage::getStoreConfigFlag('carriers/' . $method . '/active');
    }

    /**
     * Retrieve if phone number is required
     *
     * @param string $method
     * @return bool
     */
    public function showDeliverForm($method)
    {
        return Mage::getStoreConfigFlag('carriers/' . $method . '/deliver_form');
    }

    /**
     * Retrieve if phone number is required
     *
     * @param string $method
     * @return bool
     */
    public function phoneIsRequired($method)
    {
        return Mage::getStoreConfigFlag('carriers/' . $method . '/phone_required');
    }

    /**
     * Check store is secure
     *
     * @return bool
     */
    public function isSecure()
    {
        return Mage::getStoreConfigFlag('web/secure/use_in_frontend');
    }

    /**
     * Retrieve Shipping Method active countries
     *
     * @param string $shippingMethod
     * @return string
     */
    public function getSpecificCountry($shippingMethod)
    {
        return Mage::getStoreConfig('carriers/' . $shippingMethod . '/specificcountry');
    }

    /**
     * Retrieve product code for shipping method
     *
     * @param string $carrier
     * @return string
     */
    public function getProductCode($carrier)
    {
        return $this->getConfig($carrier . '/product_code');
    }

    /**
     * Check if Shipping label module from Magentix is enabled
     *
     * @return bool
     */
    public function isShippingLabelEnabled()
    {
        return Mage::helper('core')->isModuleEnabled('LaPoste_Label');
    }

    /**
     * Retrieve Import directory
     *
     * @return string
     */
    public function getImportDir()
    {
        $directory = Mage::getBaseDir('var') . DS . 'import' . DS . 'colissimo';

        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        return $directory;
    }

    /**
     * Retrieve CSV import columns required
     *
     * @return array
     */
    public function getImportColumnsRequired()
    {
        return array(
            'shipping_method',
            'country_id',
            'weight_from',
            'weight_to',
            'price',
            'store_id',
        );
    }

}