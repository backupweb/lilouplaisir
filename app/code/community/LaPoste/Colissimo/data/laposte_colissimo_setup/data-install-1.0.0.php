<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* Add country region */

$bind = array(
    array('country_id' => 'FR', 'code' => 'OM', 'default_name' => 'Outre-Mer'),
    array('country_id' => 'FR', 'code' => '98', 'default_name' => 'Monaco')
);

foreach ($bind as $data) {
    $this->getConnection()->insert(
        $this->getTable('directory/country_region'),
        $data
    );
}

/* Remove state required for region */

$connection = $installer->getConnection();

$requiredState = $connection->fetchOne(
    $connection->select()
        ->from($installer->getTable('core/config_data'), array('value'))
        ->where('scope_id = ?', 0)
        ->where('path = ?', 'general/region/state_required')
        ->limit(1)
);

if ($requiredState) {
    $countries = explode(',', $requiredState);

    $except = array('FR', 'BE', 'DE', 'GB', 'LU', 'ES');

    foreach ($except as $code) {
        if (($key = array_search($code, $countries))) {
            unset($countries[$key]);
        }
    }

    $installer->setConfigData('general/region/state_required', join(',', $countries));
}

$installer->endSetup();