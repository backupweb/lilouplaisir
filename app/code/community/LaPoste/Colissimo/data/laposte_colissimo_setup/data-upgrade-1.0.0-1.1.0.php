<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$insert = array();

/** @var LaPoste_Colissimo_Model_System_Carrier $carrier */
$carrier = Mage::getModel('laposte_colissimo/system_carrier');

if (Mage::app()->isSingleStoreMode()) {
    $stores = array(
        Mage::app()->getStore(0)
    );
} else {
    $stores = Mage::app()->getStores(true);
}

/** @var Mage_Core_Model_Store $store */
foreach ($stores as $store) {
    foreach ($carrier->toArray() as $code => $title) {
        $price = Mage::getStoreConfig('carriers/' . $code . '/price', $store->getId());
        if ($price) {
            $prices = unserialize($price);
            foreach ($prices as $data) {
                $insert[] = array(
                    'country_id' => $data['country'],
                    'store_id' => $store->getId(),
                    'price' => $data['price'],
                    'weight_from' => $data['weight_from'],
                    'weight_to' => $data['weight_to'],
                    'shipping_method' => $code
                );
            }
        }
    }
}

if (count($insert)) {
    /** @var LaPoste_Colissimo_Model_Price $model */
    $model = Mage::getModel('laposte_colissimo/price');
    $model->massImport($insert);
}

$installer->endSetup();