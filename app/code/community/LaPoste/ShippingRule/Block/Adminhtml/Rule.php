<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Block_Adminhtml_Rule extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_rule';
        $this->_blockGroup = 'laposte_shippingrule';
        $this->_headerText = Mage::helper('laposte_shippingrule')->__('Manage Rules');

        $this->_addButtonLabel = Mage::helper('laposte_shippingrule')->__('Add Rule');

        parent::__construct();
    }

}