<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('rule_id');
        $this->setDefaultDir('asc');
        $this->setId('rule_grid');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve Store Id
     *
     * @return Mage_Core_Model_Store
     */
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * Prepare Collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var LaPoste_ShippingRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getModel('laposte_shippingrule/rule')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        /** @var LaPoste_ShippingRule_Helper_Data $helper */
        $helper = Mage::helper('laposte_shippingrule');

        $this->addColumn('rule_id', array(
                'header' => $helper->__('Id'),
                'align'  => 'left',
                'width'  => '50px',
                'index'  => 'rule_id'
            ));

        $this->addColumn('name', array(
                'header' => $helper->__('Name'),
                'align'  => 'left',
                'index'  => 'name'
            )
        );

        $this->addColumn('shipping_method', array(
            'header'  => $helper->__('Method'),
            'align'   => 'left',
            'index'   => 'shipping_method',
            'type'    => 'options',
            'options' => Mage::getSingleton('laposte_shippingrule/system_carrier')->toArray(),
        ));

        $this->addColumn('from_date', array(
                'header' => $helper->__('From Date'),
                'align'  => 'left',
                'type'   => 'date',
                'index'  => 'from_date',
            )
        );

        $this->addColumn('to_date', array(
                'header' => $helper->__('To Date'),
                'align'  => 'left',
                'type'   => 'date',
                'index'  => 'to_date',
            )
        );

        $this->addColumn('shipping_action', array(
            'header'  => $helper->__('Action'),
            'align'   => 'left',
            'index'   => 'shipping_action',
            'type'    => 'options',
            'options' => Mage::getSingleton('laposte_shippingrule/system_action')->toArray(),
        ));

        $this->addColumn('shipping_amount', array(
                'header' => $helper->__('Shipping Amount'),
                'align'  => 'left',
                'type'   => 'number',
                'index'  => 'shipping_amount',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                    'header'        => $helper->__('Store View'),
                    'index'         => 'store_id',
                    'type'          => 'store',
                    'store_all'     => true,
                    'store_view'    => true,
                    'sortable'      => false,
                    'filter'        => false,
                    'skipEmptyStoresLabel' => true,
                )
            );
        }

        $this->addColumn('is_active', array(
            'header'  => $helper->__('Status'),
            'align'   => 'left',
            'index'   => 'is_active',
            'type'    => 'options',
            'options' => Mage::getSingleton('laposte_shippingrule/system_status')->toArray(),
        ));

        if (!$this->_isExport) {
            $this->addColumn('action',
                array(
                    'header'    => $helper->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                        array(
                            'caption'  => $helper->__('Edit'),
                            'url'      => array(
                                'base' =>'*/*/edit',
                            ),
                            'field'    => 'rule_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                ));
        }

        $this->addExportType('*/*/exportCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportXml', $helper->__('XML'));
        $this->addExportType('*/*/exportExcel', $helper->__('XML Excel'));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve Row Url
     *
     * @param object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('rule_id' => $row->getId()));
    }

    /**
     * Prepare Massaction
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        /** @var LaPoste_ShippingRule_Helper_Data $helper */
        $helper = Mage::helper('laposte_shippingrule');

        $this->setMassactionIdField('rule_id');
        $this->getMassactionBlock()->setFormFieldName('rule_ids');

        $this->getMassactionBlock()->addItem('delete', array(
                'label'   => $helper->__('Delete'),
                'url'     => $this->getUrl('*/*/massDelete'),
                'confirm' => $helper->__('Are you sure?')
            )
        );

        return $this;
    }

}