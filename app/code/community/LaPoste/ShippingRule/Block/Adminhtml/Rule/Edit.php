<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Block_Adminhtml_Rule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Construct
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_rule';
        $this->_blockGroup = 'laposte_shippingrule';
        $this->_objectId   = 'rule_id';

        parent::__construct();

        $this->_updateButton('save',   'label', Mage::helper('laposte_shippingrule')->__('Save Rule'));
        $this->_updateButton('delete', 'label', Mage::helper('laposte_shippingrule')->__('Delete Rule'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('laposte_shippingrule')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
    }

    /**
     * Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $title = Mage::registry('rule')->getId() ? 'Edit Shipping Rule' : 'Add Shipping Rule';
        return $this->__(Mage::helper('laposte_shippingrule')->__($title));
    }

}