<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Block_Adminhtml_Rule_Edit_Tabs_General
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_ShippingRule_Helper_Data $helper */
        $helper = Mage::helper('laposte_shippingrule');

        $model = Mage::registry('rule');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('General')
            )
        );

        $fieldset->addField(
            'name', 'text', array(
                'name'     => 'name',
                'label'    => $helper->__('Name'),
                'class'    => 'required-entry',
                'required' => true,
            )
        );

        $fieldset->addField(
            'description', 'textarea', array(
                'name'     => 'description',
                'label'    => $helper->__('Description'),
                'required' => false,
            )
        );

        $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
        $found = false;

        foreach ($customerGroups as $group) {
            if ($group['value'] == 0) {
                $found = true;
            }
        }
        if (!$found) {
            array_unshift($customerGroups, array(
                'value' => 0,
                'label' => $helper->__('NOT LOGGED IN'))
            );
        }

        $fieldset->addField('customer_group_ids', 'multiselect', array(
            'name'      => 'customer_group_ids[]',
            'label'     => $helper->__('Customer Groups'),
            'title'     => $helper->__('Customer Groups'),
            'required'  => true,
            'values'    => Mage::getResourceModel('customer/group_collection')->toOptionArray(),
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('from_date', 'date',
            array(
                'name'         => 'from_date',
                'label'        => $helper->__('From Date'),
                'title'        => $helper->__('From Date'),
                'image'        => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format'       => $dateFormatIso
            )
        );

        $fieldset->addField('to_date', 'date',
            array(
                'name'   => 'to_date',
                'label'  => $helper->__('To Date'),
                'title'  => $helper->__('To Date'),
                'image'  => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format'       => $dateFormatIso
            )
        );

        $fieldset->addField(
            'shipping_method', 'select', array(
                'name'     => 'shipping_method',
                'label'    => $helper->__('Shipping Method'),
                'class'    => 'required-entry',
                'required' => true,
                'values'   => Mage::getSingleton('laposte_shippingrule/system_carrier')->toOptionArray(),
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => $helper->__('Store View'),
                'title'     => $helper->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));

            /* @var $renderer Mage_Adminhtml_Block_Store_Switcher_Form_Renderer_Fieldset_Element */
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'store_id[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField(
            'is_active', 'select', array(
                'name'     => 'is_active',
                'label'    => $helper->__('Status'),
                'class'    => 'required-entry',
                'required' => true,
                'values'   => Mage::getSingleton('laposte_shippingrule/system_status')->toOptionArray(),
            )
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_shippingrule')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_shippingrule')->__('General');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

}