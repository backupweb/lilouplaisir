<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Block_Adminhtml_Rule_Edit_Tabs_Action
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var LaPoste_ShippingRule_Helper_Data $helper */
        $helper = Mage::helper('laposte_shippingrule');

        $model = Mage::registry('rule');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $helper->__('Action')
            )
        );

        $action = $fieldset->addField(
            'shipping_action', 'select', array(
                'name'     => 'shipping_action',
                'label'    => $helper->__('Action'),
                'class'    => 'required-entry',
                'required' => true,
                'values'   => Mage::getSingleton('laposte_shippingrule/system_action')->toOptionArray(),
            )
        );

        $shippingAmount = $fieldset->addField(
            'shipping_amount', 'text', array(
                'name'     => 'shipping_amount',
                'label'    => $helper->__('Shipping Amount'),
                'class'    => 'validate-not-negative-number',
                'required' => true,
            )
        );

        $form->setValues($model->getData());
        $this->setForm($form);
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap($action->getHtmlId(), $action->getName())
            ->addFieldMap($shippingAmount->getHtmlId(), $shippingAmount->getName())
            ->addFieldDependence(
                $shippingAmount->getName(),
                $action->getName(),
                'price'
            )
        );

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('laposte_shippingrule')->__('Action');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('laposte_shippingrule')->__('Action');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

}