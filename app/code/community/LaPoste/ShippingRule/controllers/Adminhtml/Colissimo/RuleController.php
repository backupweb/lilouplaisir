<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Adminhtml_Colissimo_RuleController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * New Action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit Action
     */
    public function editAction()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');

        /** @var $model LaPoste_ShippingRule_Model_Rule */
        $model = Mage::getModel('laposte_shippingrule/rule');

        if ($ruleId) {
            $model->load($ruleId);
            if (!$model->getId()) {
                $this->_getAdminSession()->addError(
                    Mage::helper('laposte_shippingrule')->__('This rule no longer exists.')
                );
                $this->_redirect('*/*/');
                return false;
            }
        }

        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');

        Mage::register('current_promo_shipping_rule', $model);

        Mage::register('rule', $model);

        $this->_initAction()->renderLayout();
    }

    /**
     * Save Action
     */
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        $postData = $this->_filterDates($postData, array('from_date', 'to_date'));

        $data = new Varien_Object($postData);

        /* @var $model LaPoste_ShippingRule_Model_Rule */
        $model = Mage::getModel('laposte_shippingrule/rule');

        if ($data->getId()) {
            $model->load($data->getId());
        }

        if (isset($postData['rule']['conditions'])) {
            $postData['conditions'] = $postData['rule']['conditions'];
        }
        unset($postData['rule']);

        $model->loadPost($postData);

        try {
            $model->save();
            $this->_getAdminSession()->addSuccess(
                Mage::helper('laposte_shippingrule')->__('Rule has been saved')
            );
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e,
                Mage::helper('laposte_shippingrule')->__(
                    'An error occurred while saving the rule : %s', $e->getMessage()
                )
            );
        }

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('rule_id' => $model->getId()));
            return;
        }

        $this->_redirect('*/*/');
    }

    /**
     * Delete Action
     */
    public function deleteAction()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');

        if ($ruleId) {
            try {
                /* @var $model LaPoste_ShippingRule_Model_Rule */
                $model = Mage::getModel('laposte_shippingrule/rule');

                $model->load($ruleId);
                $model->delete();

                $this->_getAdminSession()->addSuccess(
                    Mage::helper('laposte_shippingrule')->__(
                        'The rule has been deleted.'
                    )
                );
                $this->_redirect('*/*/');
                return false;
            } catch (Exception $e) {
                $this->_getAdminSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('rule_id' => $ruleId));
                return false;
            }
        }

        $this->_getAdminSession()->addError(
            Mage::helper('laposte_shippingrule')->__(
                'Unable to find a rule to delete.'
            )
        );

        $this->_redirect('*/*/');
    }

    /**
     * Mass Delete Action
     */
    public function massDeleteAction()
    {
        $ruleIds = $this->getRequest()->getParam('rule_ids');
        if (!is_array($ruleIds)) {
            $this->_getSession()->addError(
                Mage::helper('laposte_shippingrule')->__('Please select rule(s).')
            );
        } else {
            if (!empty($ruleIds)) {
                try {
                    foreach ($ruleIds as $ruleId) {
                        /* @var $model LaPoste_ShippingRule_Model_Rule */
                        $model = Mage::getModel('laposte_shippingrule/rule');

                        $model->load($ruleId);
                        $model->delete();
                    }
                    $this->_getSession()->addSuccess(
                        Mage::helper('laposte_shippingrule')->__(
                            'Total of %d record(s) have been deleted.', count($ruleIds)
                        )
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout();

        $this->_title(Mage::helper('laposte_shippingrule')->__('Sales'))
            ->_title(Mage::helper('laposte_shippingrule')->__('Shipping Price Rule'));

        $this->_setActiveMenu('sales');

        return $this;
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        /* @var $grid LaPoste_ShippingRule_Block_Adminhtml_Rule_Grid */
        $grid = $this->getLayout()->createBlock('laposte_shippingrule/adminhtml_rule_grid');
        $this->_prepareDownloadResponse('laposte_shippingrule.csv', $grid->getCsvFile());
    }

    /**
     * Export order grid to XML format
     */
    public function exportXmlAction()
    {
        /* @var $grid LaPoste_ShippingRule_Block_Adminhtml_Rule_Grid */
        $grid = $this->getLayout()->createBlock('laposte_shippingrule/adminhtml_rule_grid');
        $this->_prepareDownloadResponse('laposte_shippingrule.xml', $grid->getXml());
    }

    /**
     * Export order grid to XML Excel format
     */
    public function exportExcelAction()
    {
        /* @var $grid LaPoste_ShippingRule_Block_Adminhtml_Rule_Grid */
        $grid = $this->getLayout()->createBlock('laposte_shippingrule/adminhtml_rule_grid');
        $this->_prepareDownloadResponse('laposte_shippingrule.xml', $grid->getExcelFile());
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/colissimo/rule');
    }

    /**
     * Retrieve Admin Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getAdminSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

}