<?php
/**
 * Copyright © 2017 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Promo' . DS . 'QuoteController.php';

class LaPoste_ShippingRule_Adminhtml_Colissimo_QuoteController extends Mage_Adminhtml_Promo_QuoteController
{

    /**
     * Returns result of current user permission check on resource and privilege
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/colissimo/rule');
    }
}
