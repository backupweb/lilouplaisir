<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `{$installer->getTable('laposte_shippingrule/rule')}`
ADD COLUMN `customer_group_ids` VARCHAR(255) DEFAULT NULL
AFTER `is_active`
");

$installer->endSetup();