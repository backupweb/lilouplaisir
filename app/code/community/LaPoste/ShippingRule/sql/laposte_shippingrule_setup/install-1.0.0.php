<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('laposte_shippingrule/rule')}` (
  `rule_id`               INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `name`                  VARCHAR(255) DEFAULT NULL,
  `description`           TEXT,
  `from_date`             DATE DEFAULT NULL,
  `to_date`               DATE DEFAULT NULL,
  `is_active`             SMALLINT(6) NOT NULL DEFAULT '0',
  `conditions_serialized` MEDIUMTEXT,
  `shipping_method`       VARCHAR(255) DEFAULT NULL,
  `shipping_action`       VARCHAR(255) DEFAULT 'price',
  `shipping_amount`       DECIMAL(12,4) NOT NULL DEFAULT '0.0000',
  `store_id`              SMALLINT(5) unsigned NOT NULL,
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='Shippingrule'
");

$installer->endSetup();