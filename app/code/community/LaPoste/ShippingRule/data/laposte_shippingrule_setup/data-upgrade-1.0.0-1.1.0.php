<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Mage_Customer_Model_Group $group */
$group = Mage::getModel('customer/group');
$customerGroupIds = join(',', $group->getCollection()->getAllIds());

$installer->run("
UPDATE `{$installer->getTable('laposte_shippingrule/rule')}` SET `customer_group_ids` = '{$customerGroupIds}'
");

$installer->endSetup();