<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

$data = array(
    'name'                  => '[DEMO] Frais de port gratuit - France - Point de retrait - 100€ TTC',
    'description'           => '- Frais de port gratuit
- A partir de 100€ TTC avec remise
- Colissimo point de retrait
- Livraison en France uniquement
- Tous les stores',
    'is_active'             => 0,
    'conditions_serialized' => 'a:7:{s:4:"type";s:43:"laposte_shippingrule/rule_condition_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";s:10:"conditions";a:2:{i:0;a:5:{s:4:"type";s:43:"laposte_shippingrule/rule_condition_address";s:9:"attribute";s:31:"subtotal_incl_tax_with_discount";s:8:"operator";s:2:">=";s:5:"value";s:3:"100";s:18:"is_value_processed";b:0;}i:1;a:5:{s:4:"type";s:43:"laposte_shippingrule/rule_condition_address";s:9:"attribute";s:10:"country_id";s:8:"operator";s:2:"==";s:5:"value";s:2:"FR";s:18:"is_value_processed";b:0;}}}',
    'shipping_method'       => 'pickup',
    'shipping_action'       => 'price',
    'shipping_amount'       => 0,
    'store_id'              => 0
);

/** @var LaPoste_ShippingRule_Model_Rule $rule */
$rule = Mage::getModel('laposte_shippingrule/rule');

$rule->setData($data);

$rule->save();