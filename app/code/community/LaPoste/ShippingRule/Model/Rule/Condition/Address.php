<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_Rule_Condition_Address extends Mage_SalesRule_Model_Rule_Condition_Address
{

    /**
     * Load Attribute Options
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $attributes = array(
            'base_subtotal' => Mage::helper('laposte_shippingrule')->__('Subtotal (Excl. Tax)'),
            'subtotal_excl_tax_with_discount' => Mage::helper('laposte_shippingrule')->__('Subtotal With Discount (Excl. Tax)'),
            'subtotal_incl_tax' => Mage::helper('laposte_shippingrule')->__('Subtotal (Incl. Tax)'),
            'subtotal_incl_tax_with_discount' => Mage::helper('laposte_shippingrule')->__('Subtotal With Discount (Incl. Tax)'),
            'total_qty' => Mage::helper('laposte_shippingrule')->__('Total Items Quantity'),
            'weight' => Mage::helper('laposte_shippingrule')->__('Total Weight'),
            'payment_method' => Mage::helper('laposte_shippingrule')->__('Payment Method'),
            'shipping_method' => Mage::helper('laposte_shippingrule')->__('Shipping Method'),
            'postcode' => Mage::helper('laposte_shippingrule')->__('Shipping Postcode'),
            'region' => Mage::helper('laposte_shippingrule')->__('Shipping Region'),
            'region_id' => Mage::helper('laposte_shippingrule')->__('Shipping State/Province'),
            'country_id' => Mage::helper('laposte_shippingrule')->__('Shipping Country'),
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $address = $object;
        if (!$address instanceof Mage_Sales_Model_Quote_Address) {
            if ($object->getQuote()->isVirtual()) {
                $address = $object->getQuote()->getBillingAddress();
            }
            else {
                $address = $object->getQuote()->getShippingAddress();
            }
        }

        if ('payment_method' == $this->getAttribute() && ! $address->hasPaymentMethod()) {
            $address->setPaymentMethod($object->getQuote()->getPayment()->getMethod());
        }

        if ('subtotal_incl_tax_with_discount' == $this->getAttribute()) {
            $address->setSubtotalInclTaxWithDiscount($address->getSubtotalInclTax() + $address->getDiscountAmount());
        }

        if ('subtotal_excl_tax_with_discount' == $this->getAttribute()) {
            $address->setSubtotalExclTaxWithDiscount($address->getBaseSubtotal() + $address->getDiscountAmount());
        }

        return Mage_Rule_Model_Condition_Abstract::validate($address);
    }

}
