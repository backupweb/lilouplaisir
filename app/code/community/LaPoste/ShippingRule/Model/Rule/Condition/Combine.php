<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_Rule_Condition_Combine extends Mage_Rule_Model_Condition_Combine
{

    public function __construct()
    {
        parent::__construct();
        $this->setType('laposte_shippingrule/rule_condition_combine');
    }

    public function getNewChildSelectOptions()
    {
        $addressCondition = Mage::getModel('laposte_shippingrule/rule_condition_address');
        $addressAttributes = $addressCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();
        foreach ($addressAttributes as $code => $label) {
            $attributes[] = array('value' => 'laposte_shippingrule/rule_condition_address|' . $code, 'label' => $label);
        }

        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive($conditions, array(
            array('value' => 'salesrule/rule_condition_product_found', 'label' => Mage::helper('laposte_shippingrule')->__('Product attribute combination')),
            array('value' => 'salesrule/rule_condition_product_subselect', 'label' => Mage::helper('laposte_shippingrule')->__('Products subselection')),
            array('value' => 'salesrule/rule_condition_combine', 'label' => Mage::helper('laposte_shippingrule')->__('Conditions combination')),
            array('label' => Mage::helper('laposte_shippingrule')->__('Cart Attribute'), 'value' => $attributes),
        ));

        $additional = new Varien_Object();
        Mage::dispatchEvent('salesrule_rule_condition_combine', array('additional' => $additional));
        if ($additionalConditions = $additional->getConditions()) {
            $conditions = array_merge_recursive($conditions, $additionalConditions);
        }

        return $conditions;
    }

}
