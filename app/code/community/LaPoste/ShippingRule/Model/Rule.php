<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_Rule extends Mage_Rule_Model_Abstract
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('laposte_shippingrule/rule');
    }

    /**
     * Getter for rule conditions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('laposte_shippingrule/rule_condition_combine');
    }

    /**
     * Getter for rule actions collection
     *
     * @return bool
     */
    public function getActionsInstance()
    {
        return false;
    }

    /**
     * Reset rule actions
     *
     * @param null|Mage_Rule_Model_Action_Collection $actions
     *
     * @return Mage_Rule_Model_Abstract
     */
    protected function _resetActions($actions = null)
    {
        return $this;
    }

}