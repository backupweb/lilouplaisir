<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_System_Action
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array(
            array('value' => 'price', 'label' => Mage::helper('laposte_shippingrule')->__('Specific price')),
            array('value' => 'hide', 'label'  => Mage::helper('laposte_shippingrule')->__('Hide method')),
        );

        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'price' => Mage::helper('laposte_shippingrule')->__('Specific price'),
            'hide'  => Mage::helper('laposte_shippingrule')->__('Hide method'),
        );
    }

}
