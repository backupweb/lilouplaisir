<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_System_Status
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array(
            array('value' => 0, 'label' => Mage::helper('laposte_shippingrule')->__('Inactive')),
            array('value' => 1, 'label' => Mage::helper('laposte_shippingrule')->__('Active')),
        );

        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('laposte_shippingrule')->__('Inactive'),
            1 => Mage::helper('laposte_shippingrule')->__('Active'),
        );
    }

}
