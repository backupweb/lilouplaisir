<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_System_Carrier
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'homecl', 'label' => $this->getCarrierTitle('homecl')),
            array('value' => 'homesi', 'label' => $this->getCarrierTitle('homesi')),
            array('value' => 'pickup', 'label' => $this->getCarrierTitle('pickup')),
            array('value' => 'international', 'label' => $this->getCarrierTitle('international')),
            array('value' => 'domtomcl', 'label' => $this->getCarrierTitle('domtomcl')),
            array('value' => 'domtomsi', 'label' => $this->getCarrierTitle('domtomsi')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'homecl'        => $this->getCarrierTitle('homecl'),
            'homesi'        => $this->getCarrierTitle('homesi'),
            'pickup'        => $this->getCarrierTitle('pickup'),
            'international' => $this->getCarrierTitle('international'),
            'domtomcl'      => $this->getCarrierTitle('domtomcl'),
            'domtomsi'      => $this->getCarrierTitle('domtomsi'),
        );
    }

    /**
     * Retrieve carrier full name
     *
     * @param string $carrier
     * @return string
     */
    public function getCarrierTitle($carrier)
    {
        return Mage::getStoreConfig('carriers/' . $carrier . '/title') . ' - ' .
            Mage::getStoreConfig('carriers/' . $carrier . '/name');
    }

}
