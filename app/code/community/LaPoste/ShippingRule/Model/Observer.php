<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_Observer
{

    /**
     * Colissimo Price Rule
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function priceRule(Varien_Event_Observer $observer)
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $method */
        $method = $observer->getEvent()->getMethod();

        /** @var Mage_Shipping_Model_Rate_Request $request */
        $request = $observer->getEvent()->getRequest();

        $carrier = $method->getCarrier();

        /** @var LaPoste_ShippingRule_Model_Rule $shippingRule */
        $shippingRules = Mage::getModel('laposte_shippingrule/rule')->getCollection()
            ->addFieldToFilter('shipping_method', $carrier)
            ->addFieldToFilter('customer_group_ids', array(
                array(
                    'finset'=> array($this->_getCustomerGroupId()))
                )
            )
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('store_id', array(0, $request->getStoreId()))
            ->addfieldtofilter('from_date',
                array(
                    array('to' => Mage::getModel('core/date')->gmtDate()),
                    array('from_date', 'null' => '')
                )
            )
            ->addfieldtofilter('to_date',
                array(
                    array('gteq' => Mage::getModel('core/date')->gmtDate()),
                    array('to_date', 'null' => '')
                )
            );

        foreach ($shippingRules as $shippingRule) {
            if ($shippingRule->validate($this->_getAddress())) {
                if ($shippingRule->getShippingAction() == 'price') {
                    $method->setPrice($shippingRule->getShippingAmount());
                }
                if ($shippingRule->getShippingAction() == 'hide') {
                    $method->setHideMethod(true);
                }
            }
        }
    }

    /**
     * Retrieve current Shipping Address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function _getAddress()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            /* @var $create Mage_Adminhtml_Model_Sales_Order_Create */
            $create = Mage::getSingleton('adminhtml/sales_order_create');

            return $create->getShippingAddress();
        }

        /* @var $session Mage_Checkout_Model_Session */
        $session = Mage::getSingleton('checkout/session');

        $quote = $session->getQuote();

        if ($quote->getIsMultiShipping() && $session->getData('multishipping_address_id')) {
            $addresses = $quote->getAllShippingAddresses();
            foreach ($addresses as $address) {
                if ($address->getId() == $session->getData('multishipping_address_id')) {
                    return $address;
                }
            }
        }

        return $quote->getShippingAddress();
    }

    /**
     * Retrieve current customer group id
     *
     * @return int
     */
    protected function _getCustomerGroupId()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            /* @var $create Mage_Adminhtml_Model_Sales_Order_Create */
            $create = Mage::getSingleton('adminhtml/sales_order_create');

            return $create->getCustomerGroupId();
        }

        /** @var Mage_Customer_Model_Session $customer */
        $customer = Mage::getSingleton('customer/session');

        return $customer->getCustomerGroupId();
    }
}