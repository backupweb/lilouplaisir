<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_ShippingRule_Model_Resource_Rule extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('laposte_shippingrule/rule', 'rule_id');
    }

    /**
     * Perform actions before object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return $this
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (is_array($object->getCustomerGroupIds())) {
            $object->setCustomerGroupIds(join(',', $object->getCustomerGroupIds()));
        }

        return parent::_beforeSave($object);
    }

}