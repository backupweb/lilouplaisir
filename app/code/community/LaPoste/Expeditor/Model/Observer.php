<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Model_Observer
{

    /**
     * Set Product Code for Shipping Address
     *
     * @param Varien_Event_Observer $observer
     */
    public function setProductCode(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $address = $order->getShippingAddress();

        $address->setData('expeditor_product_code', $helper->getProductCode($address));
    }
}
