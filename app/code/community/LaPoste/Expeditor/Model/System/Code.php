<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Model_System_Code
{

    /**
     * Options getter
     *
     * @param bool $select
     *
     * @return array
     */
    public function toOptionArray($select = false)
    {
        $options = array(
            array('value' => 'DOM',  'label' => Mage::helper('laposte_expeditor')->__('Domicile sans signature')),
            array('value' => 'DOS',  'label' => Mage::helper('laposte_expeditor')->__('Domicile avec signature')),
            array('value' => 'COM',  'label' => Mage::helper('laposte_expeditor')->__('Outre-Mer sans signature')),
            array('value' => 'CDS',  'label' => Mage::helper('laposte_expeditor')->__('Outre-Mer avec signature')),
            array('value' => 'COLI', 'label' => Mage::helper('laposte_expeditor')->__('Expert International')),
        );

        if ($select) {
            array_unshift($options, array('value' => '', 'label' => Mage::helper('document')->__('Select')));
        }

        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'DOM' => Mage::helper('laposte_expeditor')->__('Domicile sans signature'),
            'DOS' => Mage::helper('laposte_expeditor')->__('Domicile avec signature'),
            'COM' => Mage::helper('laposte_expeditor')->__('Outre-Mer sans signature'),
            'CDS' => Mage::helper('laposte_expeditor')->__('Outre-Mer avec signature'),
            'COLI' => Mage::helper('laposte_expeditor')->__('Expert International'),
        );
    }
}
