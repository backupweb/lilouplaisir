<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Model_Resource_Export_Expeditor
    extends Mage_Sales_Model_Resource_Order_Address_Collection
{

    /**
     * @var bool
     */
    protected $_export = false;

    /**
     * Add order filter
     *
     * @param array|string $orders
     * @return $this
     */
    public function addOrderFilter($orders)
    {
        $this->addFieldToFilter('s.parent_id', array('in' => $orders));

        return $this;
    }

    /**
     * Add status filter
     *
     * @param array|string $status
     * @return $this
     */
    public function addStatusFilter($status)
    {
        $this->addFieldToFilter('o.status', array('in' => $status));

        return $this;
    }

    /**
     * Add is exported filter
     *
     * @param array|string $status
     * @return $this
     */
    public function addIsExportedFilter($status)
    {
        $this->addFieldToFilter('o.expeditor_is_exported', array('in' => $status));

        return $this;
    }

    /**
     * Add shipping method filter
     *
     * @param array|string $methods
     * @return $this
     */
    public function addMethodFilter($methods)
    {
        $this->addFieldToFilter('o.shipping_method', array('in' => $methods));

        return $this;
    }

    /**
     * Add order date filter
     *
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function addOrderDateFilter($from = null, $to = null)
    {
        if ($from) {
            $this->addFieldToFilter('o.created_at', array('gteq' => $from));
        }
        if ($to) {
            $this->addFieldToFilter('o.created_at', array('lteq' => $to));
        }

        return $this;
    }

    /**
     * Add exported date filter
     *
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function addExportedDateFilter($from = null, $to = null)
    {
        if ($from) {
            $this->addFieldToFilter('o.expeditor_exported_date', array('gteq' => $from));
        }
        if ($to) {
            $this->addFieldToFilter('o.expeditor_exported_date', array('lteq' => $to));
        }

        return $this;
    }

    /**
     * Add Increment Id filter
     *
     * @param string $incrementId
     * @return $this
     */
    public function addIncrementIdFilter($incrementId)
    {
        $this->addFieldToFilter('o.increment_id', array('like' => '%' . $incrementId . '%'));
    }

    /**
     * Mark loaded order as exported
     *
     * @param array $orderIds
     */
    public function markAsExported($orderIds)
    {
        $values = array(
            'expeditor_is_exported'   => 1,
            'expeditor_exported_date' => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
        );

        $where = array(
            'entity_id IN (?)' => $orderIds
        );

        $this->getConnection()->update($this->getTable('sales/order'), $values, $where);
    }

    /**
     * Add all columns for export
     */
    public function isExport()
    {
        $this->_export = true;
    }

    /**
     * Init collection select
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(
            array(
                's' => $this->getMainTable()
            )
        );
        return $this;
    }

    /**
     * Create all ids retrieving select with limitation
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getAllIdsSelect($limit = null, $offset = null)
    {
        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns('parent_id', 's');
        $idsSelect->limit($limit, $offset);
        return $idsSelect;
    }

    /**
     * Default collection columns
     */
    protected function defaultColumns()
    {
        $columns = array(
            'order_id'                  => $this->_pad('s.parent_id'),
            'order_increment_id'        => $this->_pad('o.increment_id'),
            'order_status'              => $this->_pad('o.status'),
            'order_created_at'          => $this->_pad('o.created_at'),
            'weight'                    => $this->_pad('ROUND(o.weight * ' . $this->getWeightRate() . ')'),
            'shipping_method'           => $this->_pad('o.shipping_method'),
            'shipping_description'      => $this->_pad('o.shipping_description'),
            'country_id'                => $this->_pad('s.country_id'),
            'postcode'                  => $this->_pad('s.postcode'),
            'expeditor_is_exported'     => $this->_pad('o.expeditor_is_exported'),
            'expeditor_exported_date'   => $this->_pad('o.expeditor_exported_date'),
            'expeditor_product_code'    => $this->_pad('s.expeditor_product_code'),
        );

        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if ($helper->isColissimoEnabled()) {
            $columns['colissimo_product_code'] = $this->_pad('s.colissimo_product_code');
            $columns['colissimo_network_code'] = $this->_pad('s.colissimo_network_code');
            $columns['colissimo_pickup_id']    = $this->_pad('s.colissimo_pickup_id');
        }

        $this->getSelect()->columns($columns);
    }

    /**
     * Export columns
     */
    protected function exportColumns()
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $origin = $helper->getOriginAddress();

        $productCode = 's.expeditor_product_code';
        $networkCode = '""';
        $pickupId    = '""';

        if ($helper->isColissimoEnabled()) {
            $productCode = 'IF(s.colissimo_product_code IS NOT NULL, s.colissimo_product_code, "")';
            $networkCode = 'IF(s.colissimo_network_code IS NOT NULL, s.colissimo_network_code, "")';
            $pickupId    = 'IF(s.colissimo_pickup_id IS NOT NULL, s.colissimo_pickup_id, "")';
        }

        $this->getSelect()->columns(
            array(
                'C01' => $this->_pad('"EXP"', 3),                                                                       // Type d'enregistrement
                'C02' => $this->_pad('IF(o.increment_id IS NOT NULL, o.increment_id, "")', 35),                         // Référence du colis pour l'expéditeur
                'C03' => $this->_pad('""', 8),                                                                          // Date d'expédition
                'C04' => $this->_pad($productCode, 4),                                                                             // Code produit
                'C05' => $this->_pad('""', 6),                                                                          // Code expéditeur
                'C06' => $this->_pad('""', 17),                                                                         // Code destinataire
                'C07' => $this->_pad('s.lastname', 35),                                                                 // Nom du destinataire
                'C08' => $this->_pad($this->_getStreet(1), 35),                                                               // Adresse 1 du destinataire
                'C09' => $this->_pad($this->_getStreet(2), 35),                                                               // Adresse 2 du destinataire
                'C10' => $this->_pad($this->_getStreet(3), 35),                                                               // Adresse 3 du destinataire
                'C11' => $this->_pad($this->_condition('postcode'), 9),                                                    // Code postal du destinataire
                'C12' => $this->_pad($this->_condition('city'), 35),                                                       // Commune du destinataire
                'C13' => $this->_pad($this->_condition('country_id'), 3),                                                  // Code pays du destinataire
                'C14' => $this->_pad('""', 70),                                                                         // Instruction de livraison
                'C15' => $this->_pad('""', 12),                                                                         // Filler
                'C16' => $this->_pad('ROUND(o.weight * ' . $this->getWeightRate() . ')', 15),                           // Poids (g)
                'C17' => $this->_pad('"0"', 15),                                                                        // Montant CRBT
                'C18' => $this->_pad('""', 3),                                                                          // Filler
                'C19' => $this->_pad('""', 15),                                                                         // Montant ADV (assurance)
                'C20' => $this->_pad('""', 3),                                                                          // Filler
                'C21' => $this->_pad('"N"', 1),                                                                         // Non mécanisable
                'C22' => $this->_pad('""', 17),                                                                         // Filler
                'C23' => $this->_pad('s.telephone', 25),                                                                // Téléphone
                'C24' => $this->_pad('""', 4),                                                                          // Filler
                'C25' => $this->_pad('"0"', 1),                                                                         // Recommandation (0 = aucun, 1 = R1, 2 = R2, 3 = R3)
                'C26' => $this->_pad('"N"', 1),                                                                         // Avis de réception
                'C27' => $this->_pad('"N"', 1),                                                                         // FTD (Franc de taxes et de droits)
                'C28' => $this->_pad('"2"', 1),                                                                         // Instruction de non livraison (Type de retour)
                'C29' => $this->_pad('IF(o.customer_email IS NOT NULL, o.customer_email, "")', 75),                     // Adresse E-mail du destinataire
                'C30' => $this->_pad('"5"', 1),                                                                         // Nature envoi
                'C31' => $this->_pad('""', 25),                                                                         // Numéro de licence
                'C32' => $this->_pad('""', 25),                                                                         // Numéro de certificat
                'C33' => $this->_pad('""', 25),                                                                         // Numéro de facture
                'C34' => $this->_pad('""', 4),                                                                          // Référence en douane
                'C35' => $this->_pad('""', 35),                                                                         // Adresse 4 du destinataire
                'C36' => $this->_pad('""', 35),                                                                         // Numéro d’affaire
                'C37' => $this->_pad('IF(o.increment_id IS NOT NULL, o.increment_id, "")', 35),                         // Numéro de commande
                'C38' => $this->_pad('""', 73),                                                                         // Filler
                'C39' => $this->_pad('""', 1),                                                                          // Civilité du destinataire
                'C40' => $this->_pad('s.firstname', 29),                                                                // Prénom du destinataire
                'C41' => $this->_pad($this->_condition('company'), 38),                                                    // Raison sociale du destinataire
                'C42' => $this->_pad('IF(s.country_id = "FR", s.telephone, "")', 10),                                   // Portable du destinataire
                'C43' => $this->_pad('""', 8),                                                                          // Code porte 1
                'C44' => $this->_pad('""', 8),                                                                          // Code porte 2
                'C45' => $this->_pad('""', 30),                                                                         // Interphone
                'C46' => $this->_pad($pickupId, 6),                                                                                // Code point de retrait
                'C47' => $this->_pad('""', 1),                                                                          // Filler
                'C48' => $this->_pad('"' . $helper->getCommercialName() . '"', 38),                                     // Nom commercial de l'expéditeur
                'C49' => $this->_pad('""', 30),                                                                         // Filler
                'C50' => $this->_pad('""', 15),                                                                         // Code avoir / promotion
                'C51' => $this->_pad('""', 75),                                                                         // Filler
                'C52' => $this->_pad($networkCode, 3),                                                                             // Code réseau
                'C53' => $this->_pad('IF(s.country_id <> "FR", s.telephone, "")', 20),                                  // Portable international
                'C54' => $this->_pad('"' . $origin['company'] . '"', 35),                                               // Nom destinataire retour
                'C55' => $this->_pad('"' . $origin['address_1'] . '"', 35),                                             // Adresse 1 retour
                'C56' => $this->_pad('"' . $origin['address_2'] . '"', 35),                                             // Adresse 2 retour
                'C57' => $this->_pad('""', 35),                                                                         // Adresse 3 retour
                'C58' => $this->_pad('""', 35),                                                                         // Adresse 4 retour
                'C59' => $this->_pad('"' . $origin['postcode'] . '"', 5),                                               // Code postal retour
                'C60' => $this->_pad('"' . $origin['city'] . '"', 35),                                                  // Commune retour
                'C61' => $this->_pad('""', 4),                                                                          // Longueur du colis en mm
                'C62' => $this->_pad('""', 4),                                                                          // Largeur du colis en mm
                'C63' => $this->_pad('""', 4),                                                                          // Hauteur du colis en mm
                'C64' => $this->_pad('"FR"', 2),                                                                        // Langue de notification
            )
        );
    }

    /**
     * Load data
     *
     * @param bool $printQuery
     * @param bool $logQuery
     *
     * @return Varien_Data_Collection_Db
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }

        $this->getSelect()->reset(Zend_Db_Select::COLUMNS);

        if ($this->_export) {
            $this->exportColumns();
        } else {
            $this->defaultColumns();
        }

        $this->getSelect()->joinInner(
            array('b' => $this->getTable('sales/order_address')),
            '`s`.`parent_id` = `b`.`parent_id` AND `b`.`address_type` = "billing"',
            array()
        );

        $this->getSelect()->joinInner(
            array('o' => $this->getTable('sales/order')),
            '`s`.`parent_id` = `o`.`entity_id`',
            array()
        );

        $this->getSelect()->where('`s`.`address_type` = ?', 'shipping');

        parent::load($printQuery, $logQuery);

        return $this;
    }

    /**
     * Retrieve available methods
     *
     * @return array
     */
    public function getAvailableMethods()
    {
        $connection = $this->getConnection();

        return $connection->fetchCol(
            $connection->select()
                ->distinct()
                ->from($this->getTable('sales/order'), array('shipping_method'))
        );
    }

    /**
     * Retrieve weight rate conversion
     *
     * @return int
     */
    public function getWeightRate()
    {
        return Mage::helper('laposte_expeditor')->getWeightRate();
    }

    /**
     * Retrieve street line number
     *
     * @param int $line
     * @return string
     */
    protected function _getStreet($line)
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if ($helper->isColissimoEnabled()) {
            $condition = 'IF (`s`.`colissimo_pickup_id` IS NULL,
                IF(' . $this->_countStreetLine('s') . ' > ' . ($line - 1) . ',
                    SUBSTRING_INDEX(SUBSTRING_INDEX(`s`.`street`, "\n", ' . $line . '), "\n", -1),
                    ""
                ),
                IF(' . $this->_countStreetLine('b') . ' > ' . ($line - 1) . ',
                    SUBSTRING_INDEX(SUBSTRING_INDEX(`b`.`street`, "\n", ' . $line . '), "\n", -1),
                    ""
                )
		    )';
        } else {
            $condition =
                'IF(' . $this->_countStreetLine('s') . ' > ' . ($line - 1) . ',
                    SUBSTRING_INDEX(SUBSTRING_INDEX(`s`.`street`, "\n", ' . $line . '), "\n", -1),
                    ""
                )';
        }

        return $condition;
    }

    /**
     * Retrieve count street lines request
     *
     * @param string $type
     * @return string
     */
    protected function _countStreetLine($type)
    {
        return '1 + LENGTH(`' . $type . '`.`street`) - LENGTH(REPLACE(`' . $type . '`.`street`, "\n", ""))';
    }

    /**
     * Retrieve column condition
     *
     * @param string $column
     * @return string
     */
    protected function _condition($column)
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if ($helper->isColissimoEnabled()) {
            $condition = 'IF(
                IF (`s`.`colissimo_pickup_id` IS NULL, `s`.`' . $column . '`, `b`.`' . $column . '`) IS NOT NULL,
                IF (`s`.`colissimo_pickup_id` IS NULL, `s`.`' . $column . '`, `b`.`' . $column . '`),
                ""
            )';
        } else {
            $condition = 'IF(`s`.`' . $column . '` IS NOT NULL, `s`.`' . $column . '`, "")';
        }

        return $condition;
    }

    /**
     * Pad Right
     *
     * @param string $expression
     * @param int    $length
     * @return Zend_Db_Expr
     */
    protected function _pad($expression, $length = null)
    {
        if ($length) {
            return new Zend_Db_Expr('RPAD(' . $expression . ',' . $length . '," ")');
        }

        return new Zend_Db_Expr($expression);
    }
}
