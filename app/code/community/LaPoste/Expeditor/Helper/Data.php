<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Helper_Data extends Mage_Core_Helper_Abstract
{


    /**
     * Retrieve origin address
     *
     * @return array
     */
    public function getOriginAddress()
    {
        return array(
            'company'   => $this->getCommercialName(),
            'address_1' => Mage::getStoreConfig('shipping/origin/street_line1'),
            'address_2' => Mage::getStoreConfig('shipping/origin/street_line2'),
            'postcode'  => Mage::getStoreConfig('shipping/origin/postcode'),
            'city'      => Mage::getStoreConfig('shipping/origin/city'),
        );
    }

    /**
     * Retrieve shipping method title
     *
     * @param $carrierCode
     * @return string
     */
    public function getShippingMethodTitle($carrierCode)
    {
        return Mage::getStoreConfig('carriers/' . $carrierCode . '/title');
    }

    /**
     * Retrieve order filter status
     *
     * @return string
     */
    public function getCommercialName()
    {
        return Mage::getStoreConfig('shipping/expeditor/commercial_name');
    }

    /**
     * Retrieve order filter status
     *
     * @return string
     */
    public function getFilterMethod()
    {
        return Mage::getStoreConfig('shipping/expeditor/methods');
    }

    /**
     * Retrieve order filter status
     *
     * @return string
     */
    public function getFilterStatus()
    {
        return Mage::getStoreConfig('shipping/expeditor/order_status');
    }

    /**
     * Retrieve weight rate conversion
     *
     * @return int
     */
    public function getWeightRate()
    {
        return Mage::getStoreConfig('shipping/expeditor/weight_rate') ?: 1;
    }

    /**
     * Retrieve default product code
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @return string
     */
    public function getProductCode($address)
    {
        $products = array(
            'FR' => 'DOM',
            'BE' => 'DOM',
            'CH' => 'DOM',
            'NL' => 'DOS',
            'DE' => 'DOS',
            'GB' => 'DOS',
            'LU' => 'DOS',
            'ES' => 'DOS',
            'AT' => 'DOS',
            'EE' => 'DOS',
            'HU' => 'DOS',
            'LV' => 'DOS',
            'LT' => 'DOS',
            'CZ' => 'DOS',
            'SK' => 'DOS',
            'SI' => 'DOS',
            'PT' => 'DOS',
            'BG' => 'DOS',
            'CY' => 'DOS',
            'HR' => 'DOS',
            'DK' => 'DOS',
            'FI' => 'DOS',
            'GR' => 'DOS',
            'IE' => 'DOS',
            'IS' => 'DOS',
            'IT' => 'DOS',
            'MT' => 'DOS',
            'NO' => 'DOS',
            'PL' => 'DOS',
            'SE' => 'DOS',
            'RO' => 'DOS',
        );

        if ($address->getCountryId() == 'FR' && $this->isDomTom($address->getPostcode())) {
            return 'COM';
        }

        if (isset($products[$address->getCountryId()])) {
            return $products[$address->getCountryId()];
        }

        return 'COLI';
    }

    /**
     * Retrieve Dom Tom Country with postcode
     *
     * @param string $postcode
     * @return bool
     */
    public function isDomTom($postcode)
    {
        $isDomTom = false;
        $postcodes = $this->getDomTomPostcodes();

        $postcode = preg_replace('/\s+/', '', $postcode);
        foreach ($postcodes as $code => $regex) {
            if (preg_match($regex, $postcode)) {
                $isDomTom = true;
                break;
            }
        }
        return $isDomTom;
    }

    /**
     * Retrieve Dom-Tom postcodes
     *
     * @return array
     */
    public function getDomTomPostcodes()
    {
        return array(
            'BL' => '/^97133$/', // Saint-Barthélemy
            'MF' => '/^97150$/', // Saint-Martin (partie française)
            'GP' => '/^971[0-9]{2}$/', // Guadeloupe
            'MQ' => '/^972[0-9]{2}$/', // Martinique
            'GF' => '/^973[0-9]{2}$/', // Guyane
            'RE' => '/^974[0-9]{2}$/', // La réunion
            'PM' => '/^975[0-9]{2}$/', // St-Pierre-et-Miquelon
            'YT' => '/^976[0-9]{2}$/', // Mayotte
            'TF' => '/^984[0-9]{2}$/', // Terres-Australes
            'WF' => '/^986[0-9]{2}$/', // Wallis-et-Futuna
            'PF' => '/^987[0-9]{2}$/', // Polynésie Française
            'NC' => '/^988[0-9]{2}$/', // Nouvelle-Calédonie
        );
    }

    /**
     * Create txt file
     * Return array with keys type and value
     *
     * @param Varien_Data_Collection $collection
     * @param bool $decode
     * @return array
     */
    public function getTxtFile($collection, $decode = false)
    {
        $io = new Varien_Io_File();

        $path = Mage::getBaseDir('var') . DS . 'export' . DS . 'expeditor' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.txt';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);

        foreach ($collection as $item) {
            if ($decode) {
                $io->streamWrite(utf8_decode(join('', $item->toArray()) . "\r\n"));
            } else {
                $io->streamWrite(join('', $item->toArray()) . "\r\n");
            }
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true
        );
    }

    /**
     * Retrieve Import directory
     *
     * @return string
     */
    public function getImportDir()
    {
        $directory = Mage::getBaseDir('var') . DS . 'import' . DS . 'expeditor';

        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        return $directory;
    }

    /**
     * Check if Colissimo module from Magentix is enabled
     *
     * @return bool
     */
    public function isColissimoEnabled()
    {
        return Mage::helper('core')->isModuleEnabled('LaPoste_Colissimo');
    }
}
