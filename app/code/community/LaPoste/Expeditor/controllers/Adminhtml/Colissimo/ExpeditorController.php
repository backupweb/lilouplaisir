<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Adminhtml_Colissimo_ExpeditorController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Import Action
     */
    public function importAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Export Action
     */
    public function exportAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Export Order to ColiShip
     */
    public function exportFileAction()
    {
        $orderIds = $this->getRequest()->getPost('order_id', array());
        $decode   = $this->getRequest()->getParam('decode', false);

        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if (!count($orderIds)) {
            $this->_getSession()->addError($helper->__('Please select order(s).'));
            $this->_redirect('adminhtml/sales_order/index');
        } else {
            try {
                /* @var $addresses LaPoste_Expeditor_Model_Resource_Export_Expeditor */
                $addresses = Mage::getResourceModel('laposte_expeditor/export_expeditor');
                $addresses->addOrderFilter($orderIds);
                $addresses->isExport();

                /* @var $helper LaPoste_Expeditor_Helper_Data */
                $helper = Mage::helper('laposte_expeditor');

                $addresses->markAsExported($orderIds);

                $this->_prepareDownloadResponse(
                    'coliship-' . date('YmdHis') . '.txt', $helper->getTxtFile($addresses, $decode)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
    }

    /**
     * Download FMT file
     */
    public function fmtAction()
    {
        $this->_prepareDownloadResponse(
            'SPECIFCOL.FMT',
            file_get_contents(Mage::getModuleDir('etc', 'LaPoste_Expeditor') . DS . 'fmt' . DS . 'SPECIFCOL.FMT')
        );
    }

    /**
     * Import Tracking to Magento
     */
    public function importFileAction()
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if (!isset($_FILES['file'])) {
            $this->_getSession()->addError($helper->__('Please select file'));
            $this->_redirect('*/*/import');
            return false;
        }

        $result = $this->_addFile($_FILES['file'], array('csv', 'txt'));

        if (!$result->getUploadedFileName()) {
            $this->_redirect('*/*/import');
            return false;
        }

        /* @var $helper LaPoste_Expeditor_Helper_Data */
        $helper = Mage::helper('laposte_expeditor');

        $file = $helper->getImportDir() . DS . $result->getUploadedFileName();

        $csv = new Varien_File_Csv();
        $csv->setDelimiter(';');
        $csv->setEnclosure('"');

        $data = $csv->getData($file);

        if ($data[0][0] !== 'NumeroColis' || $data[0][1] !== 'ReferenceExpedition') {
            $this->_getSession()->addError(
                $helper->__('File must have 2 columns: %s and %s', 'NumeroColis', 'ReferenceExpedition')
            );
            $this->_redirect('*/*/import');
            return false;
        }

        Mage::register('import_file', $result->getUploadedFileName());

        $this->_initAction()->renderLayout();
    }

    /**
     * Run Import
     */
    public function runImportAction()
    {
        $order = trim($this->getRequest()->getPost('order'));
        $track = trim($this->getRequest()->getPost('track'));
        $mail  = intval($this->getRequest()->getPost('send_mail'));

        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        if ($order && $track) {

            /* @var $model Mage_Sales_Model_Order */
            $model = Mage::getModel('sales/order');
            $model->loadByIncrementId($order);

            /**
             * Check order existing
             */
            if (!$model->getId()) {
                $this->_setBody('critical', $helper->__('Order not found'));
                return false;
            }

            /**
             * Check shipment is available to create separate from invoice
             */
            if ($model->getForcedDoShipmentWithInvoice()) {
                $this->_setBody('critical', $helper->__('Cannot do shipment'));
                return false;
            }

            /**
             * Check shipment create availability
             */
            if (!$model->canShip()) {
                $this->_setBody('critical', $helper->__('Cannot do shipment'));
                return false;
            }

            try {
                $qty = array();

                /* @var $item Mage_Sales_Model_Order_Item */
                foreach ($model->getAllItems() as $item) {
                    $qty[$item->getParentItemId() ?: $item->getId()] = $item->getQtyOrdered();
                }

                /* @var $service Mage_Sales_Model_Service_Order */
                $service = Mage::getModel('sales/service_order', $model);
                $shipment = $service->prepareShipment($qty);

                $shippingMethod = $model->getShippingMethod(true);

                $tracking = array(
                    'carrier_code' => $shippingMethod->getCarrierCode(),
                    'title'        => $helper->getShippingMethodTitle($shippingMethod->getCarrierCode()),
                    'number'       => $track,
                );

                /** @var Mage_Sales_Model_Order_Shipment_Track $track */
                $track = Mage::getModel('sales/order_shipment_track')->addData($tracking);
                $shipment->addTrack($track);

                $shipment->register();

                $shipment->setEmailSent($mail ? true : false);
                $shipment->getOrder()->setCustomerNoteNotify($mail ? true : false);

                $shipment->getOrder()->setIsInProcess(true);
                Mage::getModel('core/resource_transaction')
                    ->addObject($shipment)
                    ->addObject($shipment->getOrder())
                    ->save();

                $shipment->sendEmail($mail ? true : false);

                $this->_setBody('notice', $helper->__('Success'));
                return true;

            } catch(Exception $e) {
                $this->_setBody('critical', $helper->__('Error'));
                return false;
            }
        }
        $this->_setBody('critical', $helper->__('Error'));

        return false;
    }

    /**
     * Update Weight Action
     */
    public function updateWeightAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $weight  = $this->getRequest()->getParam('weight');

        /* @var $model Mage_Sales_Model_Order */
        $model = Mage::getModel('sales/order');
        $model->load($orderId);

        $weightRate = Mage::helper('laposte_expeditor')->getWeightRate();

        if ($model->hasData()) {
            $model->setWeight($weight / $weightRate)->save();
        }
    }

    /**
     * Update Code Action
     */
    public function updateCodeAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $code    = $this->getRequest()->getParam('code');

        /** @var LaPoste_Expeditor_Model_System_Code $data */
        $data  = Mage::getModel('laposte_expeditor/system_code');
        $codes = $data->toArray();

        if (isset($codes[$code])) {
            /* @var $model Mage_Sales_Model_Order */
            $model = Mage::getModel('sales/order');
            $model->load($orderId);

            if ($model->hasData()) {
                $address = $model->getShippingAddress();
                $address->setData('expeditor_product_code', $code);
                $address->save();
            }
        }
    }

    /**
     * Set body
     *
     * @param string $severity
     * @param string $message
     */
    protected function _setBody($severity, $message)
    {
        $this->getResponse()->setBody(
            Zend_Json::encode(
                array(
                    'message' => '<span class="grid-severity-' . $severity . '"><span>' . $message . '</span></span>'
                )
            )
        );
    }

    /**
     * Add File
     *
     * @param array $data
     * @param array $allowed
     *
     * @return Mage_Core_Model_File_Uploader
     */
    protected function _addFile($data, $allowed)
    {
        $file = new Mage_Core_Model_File_Uploader($data);

        try {
            $file->setAllowedExtensions($allowed);
            $file->setAllowRenameFiles(true);
            $file->setAllowCreateFolders(true);
            $file->setFilesDispersion(false);

            /* @var $helper LaPoste_Expeditor_Helper_Data */
            $helper = Mage::helper('laposte_expeditor');

            $fileName = 'import-' . Mage::getModel('core/date')->date('YmdHis') . '.soc';

            $file->save($helper->getImportDir(), $fileName);

        } catch (Exception $e) {
            $this->_getAdminSession()->addError($e->getMessage());
        }

        return $file;
    }

    /**
     * Retrieve Admin Session
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getAdminSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * Initialize action
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout();

        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $this->_title($helper->__('Sales'))
             ->_title($helper->__('ColiShip'));

        $this->_setActiveMenu('sales');

        return $this;
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        $actions = array(
            'import'       => 'import',
            'importFile'   => 'import',
            'runImport'    => 'import',
            'export'       => 'export',
            'exportFile'   => 'export',
            'updateWeight' => 'export',
            'updateCode'   => 'export',
            'fmt'          => 'export',
        );

        $actionName = $this->getRequest()->getActionName();

        if (!isset($actions[$actionName])) {
            return false;
        }

        return Mage::getSingleton('admin/session')->isAllowed(
            'sales/colissimo/expeditor/' . $actions[$actionName]
        );
    }
}