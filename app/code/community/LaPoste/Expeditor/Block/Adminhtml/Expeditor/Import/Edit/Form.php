<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Import_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare Form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'      => 'edit_form',
                'action'  => $this->getUrl('*/*/importFile'),
                'method'  => 'post',
                'enctype' => 'multipart/form-data',
            )
        );

        $fieldset = $form->addFieldset(
            'action_fieldset', array(
                'legend' => $this->__('Select file')
            )
        );

        $fieldset->addField(
            'File', 'file', array(
                'name'     => 'file',
                'label'    => $this->__('File'),
                'class'    => 'required-entry',
                'required' => true,
                'after_element_html' => $this->csvExample(),
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * CSV Example
     *
     * @return string
     */
    protected function csvExample()
    {
        return '<span class="expeditor-csv-title">' . $this->__('CSV Example:') . '</span>
                <div class="expeditor-csv-file">
                    "NumeroColis";"ReferenceExpedition"<br />
                    "6H000000028FR";"100000001"<br />
                    "6H000000031FR";"100000002"<br />
                    "6H000000076FR";"100000003"
                </div>';
    }

}