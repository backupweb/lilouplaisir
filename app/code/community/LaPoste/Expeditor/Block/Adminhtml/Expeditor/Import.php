<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Construct
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_expeditor_import';
        $this->_blockGroup = 'laposte_expeditor';
        $this->_objectId   = 'id';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('laposte_expeditor')->__('Import'));

        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    /**
     * Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__(Mage::helper('laposte_expeditor')->__('Import tracking number'));
    }

    /**
     * Header class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-adminhtml-expeditor-import';
    }

}