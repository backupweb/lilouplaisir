<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Export
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_expeditor_export';
        $this->_blockGroup = 'laposte_expeditor';
        $this->_headerText = Mage::helper('laposte_expeditor')->__('Export orders');

        parent::__construct();

        $this->removeButton('add');
    }

    /**
     * Prepare Layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->_addButton('fmt_file', array(
            'label'   => Mage::helper('laposte_expeditor')->__('Download FMT File'),
            'onclick' => "setLocation('{$this->getUrl('*/*/fmt')}')",
            'class'   => 'add'
        ));

        return parent::_prepareLayout();
    }
}