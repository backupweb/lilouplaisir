<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Import_File extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_expeditor_import_file';
        $this->_blockGroup = 'laposte_expeditor';
        $this->_headerText = Mage::helper('laposte_expeditor')->__('Import');

        parent::__construct();

        $this->removeButton('add');

        $this->_addButton('back', array(
                'label'   => Mage::helper('laposte_expeditor')->__('Back'),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/import') . '\')',
                'class'   => 'back'
            )
        );

        $this->_addButton('launch', array(
                'label'   => Mage::helper('laposte_expeditor')->__('Launch import'),
                'onclick' => 'importTracking.run()',
                'class'   => 'add'
            )
        );
    }

}