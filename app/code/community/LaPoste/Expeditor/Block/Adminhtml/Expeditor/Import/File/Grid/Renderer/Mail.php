<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Import_File_Grid_Renderer_Mail
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('laposte/expeditor/renderer/mail.phtml');
    }

    /**
     * Render HTML
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        return $this->toHtml();
    }

}