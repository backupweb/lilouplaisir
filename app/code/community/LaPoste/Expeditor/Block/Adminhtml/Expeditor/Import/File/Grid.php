<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Import_File_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('file_import_grid');
        $this->setSaveParametersInSession(false);
        $this->setDefaultLimit(10000);

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
        $this->setMessageBlockVisibility(false);
    }

    /**
     * Prepare Collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->_getCollection());

        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->setColumnRenderers(
            array(
                'expeditor_mail' => 'laposte_expeditor/adminhtml_expeditor_import_file_grid_renderer_mail',
            )
        );

        /* @var $helper LaPoste_Expeditor_Helper_Data */
        $helper = Mage::helper('laposte_expeditor');

        $this->addColumn('order', array(
                'header'   => $helper->__('Order'),
                'align'    => 'left',
                'index'    => 'order',
                'sortable' => false,
                'filter'   => false,
            )
        );

        $this->addColumn('tracking', array(
                'header'  => $helper->__('Tracking number'),
                'align'   => 'left',
                'index'   => 'tracking',
                'sortable' => false,
                'filter'   => false,
            )
        );

        $this->addColumn('mail', array(
                'header'   => $helper->__('Send mail'),
                'type'     => 'expeditor_mail',
                'width'    => 70,
                'sortable' => false,
                'filter'   => false,
            )
        );

        $options = array(
            1 => $helper->__('Success'),
            2 => $helper->__('Waiting...'),
            3 => $helper->__('Error'),
        );

        $this->addColumn(
            'status',
            array(
                'header'         => $helper->__('Status'),
                'align'          => 'left',
                'index'          => 'status',
                'width'          => '150px',
                'frame_callback' => array($this, 'decorateStatus'),
                'type'           => 'options',
                'options'        => $options,
                'sortable'       => false,
                'filter'         => false,
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Decorate status column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     * @return string
     */
    public function decorateStatus($value, $row, $column, $isExport)
    {
        $print = $value;

        if (!$isExport) {
            $class = '';
            switch ($row->getStatus()) {
                case 1 :
                    $class = 'grid-severity-notice';
                    break;
                case 2 :
                    $class = 'grid-severity-major';
                    break;
                case 3 :
                    $class = 'grid-severity-critical';
                    break;
            }

            $print = '<span class="' . $class . '"><span>' . $value . '</span></span>';
        }

        return $print;
    }

    /**
     * Retrieve Row Url
     *
     * @param object $row
     * @return bool
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Read CSV file and get collection
     *
     * @return Varien_Data_Collection
     */
    protected function _getCollection()
    {
        /* @var $helper LaPoste_Expeditor_Helper_Data */
        $helper = Mage::helper('laposte_expeditor');

        $file = $helper->getImportDir() . DS . Mage::registry('import_file');

        $collection = new Varien_Data_Collection();

        if (is_file($file)) {

            $csv = new Varien_File_Csv();
            $csv->setDelimiter(';');
            $csv->setEnclosure('"');

            $data = $csv->getData($file);

            if ($data[0][0] == 'NumeroColis' && $data[0][1] == 'ReferenceExpedition') {

                for ($i = 1; $i < count($data); $i++) {
                    $object = new Varien_Object();

                    $grid = array(
                        'tracking' => $data[$i][0],
                        'order'    => $data[$i][1],
                        'status'   => 2,
                    );
                    $object->setData($grid);
                    $collection->addItem($object);
                }

            }

        }

        return $collection;
    }

}