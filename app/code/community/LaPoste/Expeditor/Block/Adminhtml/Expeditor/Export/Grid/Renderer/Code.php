<?php
/**
 * Copyright © 2018 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Export_Grid_Renderer_Code
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('laposte/expeditor/renderer/code.phtml');
    }

    /**
     * Render HTML
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $this->setOrderId($row->getOrderId());
        $this->setExpeditorProductCode($this->_getValue($row));

        return $this->toHtml();
    }

    /**
     * Retrieve shipping type
     *
     * @return array
     */
    public function getCodes()
    {
        /** @var LaPoste_Expeditor_Model_System_Code $codes */
        $codes = Mage::getModel('laposte_expeditor/system_code');

        return $codes->toArray();
    }
}