<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

class LaPoste_Expeditor_Block_Adminhtml_Expeditor_Export_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('expeditor_grid');
        $this->setDefaultSort('order_created_at');
        $this->setDefaultDir('desc');
        $this->setDefaultFilter(
            array('expeditor_is_exported' => 0)
        );
    }

    /**
     * Prepare Collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /* @var $collection LaPoste_Expeditor_Model_Resource_Export_Expeditor */
        $collection = Mage::getResourceModel('laposte_expeditor/export_expeditor');

        if (count($this->_getStatusFilter())) {
            $collection->addStatusFilter($this->_getStatusFilter());
        }

        if (count($this->_getMethodFilter())) {
            $collection->addMethodFilter($this->_getMethodFilter());
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $this->setColumnRenderers(
            array(
                'expeditor_weight' => 'laposte_expeditor/adminhtml_expeditor_export_grid_renderer_weight',
                'expeditor_type'   => 'laposte_expeditor/adminhtml_expeditor_export_grid_renderer_code',
            )
        );

        $this->addColumn('order_created_at', array(
                'header'   => $helper->__('Date'),
                'index'    => 'order_created_at',
                'type'     => 'date',
                'filter_condition_callback' => array($this, '_filterCreatedDate'),
            )
        );

        $this->addColumn('order_increment_id', array(
                'header'   => $helper->__('Order'),
                'index'    => 'order_increment_id',
                'filter_condition_callback' => array($this, '_filterIncrementId'),
            )
        );

        /* @var $statuses Mage_Sales_Model_Resource_Order_Status_Collection */
        $statuses = Mage::getResourceModel('sales/order_status_collection');

        if (count($this->_getStatusFilter())) {
            $statuses->addFieldToFilter('status', $this->_getStatusFilter());
        }

        $this->addColumn('order_status', array(
                'header'   => $helper->__('Status'),
                'index'    => 'order_status',
                'type'     => 'options',
                'width'    => '70px',
                'options'  => $statuses->toOptionHash(),
                'filter_condition_callback' => array($this, '_filterStatus'),
            )
        );

        $this->addColumn('shipping_description', array(
                'header'   => $helper->__('Method'),
                'index'    => 'shipping_description',
            )
        );

        if ($helper->isColissimoEnabled()) {

            $this->addColumn('colissimo_product_code', array(
                    'header' => $helper->__('Code'),
                    'index' => 'colissimo_product_code',
                    'filter' => false,
                )
            );

            $this->addColumn('colissimo_pickup_id', array(
                    'header' => $helper->__('Pickup'),
                    'index' => 'colissimo_pickup_id',
                    'filter' => false,
                )
            );

            $this->addColumn('colissimo_network_code', array(
                    'header' => $helper->__('Network'),
                    'index' => 'colissimo_network_code',
                    'filter' => false,
                )
            );

        } else {

            $this->addColumn('expeditor_product_code', array(
                    'header' => $helper->__('Type'),
                    'index'  => 'expeditor_product_code',
                    'filter' => false,
                    'type'   => 'expeditor_type',
                )
            );

        }

        $this->addColumn('country_id', array(
                'header'   => $helper->__('Country'),
                'index'    => 'country_id',
                'filter'   => false,
            )
        );

        $this->addColumn('postcode', array(
                'header'   => $helper->__('Postcode'),
                'index'    => 'postcode',
                'filter'   => false,
            )
        );

        $this->addColumn('weight', array(
                'header'   => $helper->__('Weight'),
                'index'    => 'weight',
                'type'     => 'expeditor_weight',
                'filter'   => false,
            )
        );

        $options = array(
            0 => $helper->__('No'),
            1 => $helper->__('Yes'),
        );

        $this->addColumn(
            'expeditor_is_exported',
            array(
                'header'         => $helper->__('Is Exported'),
                'align'          => 'left',
                'index'          => 'expeditor_is_exported',
                'width'          => '100px',
                'frame_callback' => array($this, 'decorateIsExported'),
                'type'           => 'options',
                'options'        => $options,
                'filter_condition_callback' => array($this, '_filterIsExported'),
            )
        );

        $this->addColumn('expeditor_exported_date', array(
                'header'   => $helper->__('Exported date'),
                'index'    => 'expeditor_exported_date',
                'type'     => 'datetime',
                'filter_condition_callback' => array($this, '_filterExportedDate'),
            )
        );

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('laposte_expeditor')->__('Action'),
                'type'      => 'action',
                'getter'     => 'getOrderId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('laposte_expeditor')->__('View order'),
                        'url'     => array('base' => '*/sales_order/view'),
                        'field'   => 'order_id',
                        'popup'   => 1
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'width'     => '100px',
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * Filter Order Date Condition
     *
     * @param LaPoste_Expeditor_Model_Resource_Export_Expeditor $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterCreatedDate($collection, $column)
    {
        if (is_null($column->getFilter()->getValue())) {
            return;
        }

        $filter = $this->_processFilterDate($column->getFilter()->getValue());

        $collection->addOrderDateFilter($filter['from'], $filter['to']);
    }

    /**
     * Filter Exported Date Condition
     *
     * @param LaPoste_Expeditor_Model_Resource_Export_Expeditor $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterExportedDate($collection, $column)
    {
        if (is_null($column->getFilter()->getValue())) {
            return;
        }

        $filter = $this->_processFilterDate($column->getFilter()->getValue());

        $collection->addExportedDateFilter($filter['from'], $filter['to']);
    }

    /**
     * Retrieve date filter
     *
     * @param array $value
     * @return array
     */
    protected function _processFilterDate($value)
    {
        $from = null;
        $to   = null;

        if (isset($value['from'])) {
            /* @var $date Zend_date */
            $date = $value['from'];
            $date->add('1', Zend_Date::DAY);
            $from = $date->get('YYYY-MM-dd 00:00:00');
        }

        if (isset($value['to'])) {
            /* @var $date Zend_date */
            $date = $value['to'];
            $date->add('1', Zend_Date::DAY);
            $to = $date->get('YYYY-MM-dd 23:59:59');
        }

        return array(
            'from' => $from,
            'to'   => $to,
        );
    }

    /**
     * Filter Status Condition
     *
     * @param LaPoste_Expeditor_Model_Resource_Export_Expeditor $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterStatus($collection, $column)
    {
        if (is_null($column->getFilter()->getValue())) {
            return;
        }

        $collection->addStatusFilter($column->getFilter()->getValue());
    }

    /**
     * Filter Is Exported
     *
     * @param LaPoste_Expeditor_Model_Resource_Export_Expeditor $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterIsExported($collection, $column)
    {
        if (is_null($column->getFilter()->getValue())) {
            return;
        }

        $collection->addIsExportedFilter($column->getFilter()->getValue());
    }

    /**
     * Filter Increment Id Condition
     *
     * @param LaPoste_Expeditor_Model_Resource_Export_Expeditor $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterIncrementId($collection, $column)
    {
        if (is_null($column->getFilter()->getValue())) {
            return;
        }

        $collection->addIncrementIdFilter($column->getFilter()->getValue());
    }

    /**
     * Retrieve Row Url
     *
     * @param object $row
     *
     * @return bool
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Prepare Massaction
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('order_id');
        $this->getMassactionBlock()->setFormFieldName('order_id');

        $this->getMassactionBlock()->addItem('export_coliship', array(
            'label' => Mage::helper('laposte_expeditor')->__('Export to ColiShip'),
            'url' => $this->getUrl('*/*/exportFile', array('_current' => true)),
        ));

        $this->getMassactionBlock()->addItem('export_expeditor', array(
            'label' => Mage::helper('laposte_expeditor')->__('Export to Expeditor Inet (End of life)'),
            'url' => $this->getUrl('*/*/exportFile', array('_current' => true, '_query' => array('decode' => 1))),
        ));

        return $this;
    }

    /**
     * Retrieve status filter
     *
     * @return array
     */
    protected function _getStatusFilter()
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $config = $helper->getFilterStatus();

        $statuses = array();

        if ($config) {
            $statuses = explode(',', $config);
        }

        return $statuses;
    }

    /**
     * Retrieve methods filter
     *
     * @return array
     */
    protected function _getMethodFilter()
    {
        /** @var LaPoste_Expeditor_Helper_Data $helper */
        $helper = Mage::helper('laposte_expeditor');

        $config = $helper->getFilterMethod();

        $methods = array();

        if ($config) {
            $methods = explode(',', $config);
        }

        return $methods;
    }

    /**
     * Decorate expeditor_is_exported column values
     *
     * @param string $value
     * @param Mage_Index_Model_Process $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @param bool $isExport
     *
     * @return string
     */
    public function decorateIsExported($value, $row, $column, $isExport)
    {
        $print = $value;

        if (!$isExport) {
            $class = '';
            switch ($row->getExpeditorIsExported()) {
                case 0 :
                    $class = 'grid-severity-critical';
                    break;
                case 1 :
                    $class = 'grid-severity-notice';
                    break;
            }

            $print = '<span class="' . $class . '"><span>' . $value . '</span></span>';
        }

        return $print;
    }

}