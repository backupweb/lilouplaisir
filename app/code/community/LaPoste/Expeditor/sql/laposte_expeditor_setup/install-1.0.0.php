<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* Add order column */
$this->getConnection()->addColumn(
    $this->getTable('sales/order'), 'expeditor_is_exported', "INT(1) NULL DEFAULT 0"
);

$this->getConnection()->addColumn(
    $this->getTable('sales/order'), 'expeditor_exported_date', "DATETIME NULL DEFAULT NULL"
);

$installer->endSetup();