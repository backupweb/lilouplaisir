<?php
/**
 * Copyright © 2016 Magentix. All rights reserved.
 *
 * NOTICE OF LICENSE
 * This source file is subject to commercial licence, do not copy or distribute without authorization
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* Add order column */
$this->getConnection()->addColumn(
    $this->getTable('sales/order_address'), 'expeditor_product_code', "VARCHAR(10) NULL DEFAULT 'DOM'"
);

$installer->endSetup();