<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */


class Amasty_Giftwrap_Block_Checkout_Total_Amgiftwrap extends Mage_Checkout_Block_Total_Default
{
    protected $_template = 'amasty/amgiftwrap/checkout/total/amgiftwrap.phtml';
}