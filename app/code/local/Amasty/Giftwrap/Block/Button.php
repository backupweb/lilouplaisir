<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */

/**
 * Copyright © 2016 Amasty. All rights reserved.
 */
class Amasty_Giftwrap_Block_Button extends Mage_Core_Block_Template
{
    protected $_template = 'amasty/amgiftwrap/cart_button.phtml';

    public function getButtonOnly()
    {
        $buttonOnly = parent::getButtonOnly();

        return is_null($buttonOnly) ? true : $buttonOnly;
    }

    protected function _prepareLayout()
    {
        $quote                  = Mage::getSingleton('checkout/session')->getQuote();
        $amgiftwrapCardId       = $quote->getAmgiftwrapCardId() ? $quote->getAmgiftwrapCardId() : '';
        $amgiftwrapDesignId     = $quote->getAmgiftwrapDesignId() ? $quote->getAmgiftwrapDesignId() : '';
        $amgiftwrapSeparateWrap = $quote->getAmgiftwrapSeparateWrap() ? $quote->getAmgiftwrapSeparateWrap() : '';
        if ($amgiftwrapDesignId || $amgiftwrapCardId) {
            $design = Mage::getModel('amgiftwrap/design')->load($amgiftwrapDesignId);
            $card   = Mage::getModel('amgiftwrap/cards')->load($amgiftwrapCardId);

            $this
                ->setDesign($design)
                ->setCard($card)
                ->setSeparateWrap($amgiftwrapSeparateWrap);
        }
        return parent::_prepareLayout();
    }


}
