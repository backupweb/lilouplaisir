<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */


class Amasty_Giftwrap_Model_Sales_Quote_Address_Total_Tax_Amgiftwrap extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'amgiftwrap_tax';

    /**
     * @var Mage_Tax_Model_Calculation
     */
    protected $_taxCalculationModel;

    public function __construct()
    {
        $this->_taxCalculationModel = Mage::getSingleton('tax/calculation');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        /*
         * check if only address type shipping coming through
         */
        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $store = $address->getQuote()->getStore();
        $billingAddress = $address->getQuote()->getBillingAddress();
        $custTaxClassId = $address->getQuote()->getCustomerTaxClassId();
        $request = $this->_taxCalculationModel->getRateRequest(
            $address,
            $billingAddress,
            $custTaxClassId,
            $store
        );
        $request->setProductClassId(Mage::helper('amgiftwrap')->getWrappingTaxClass($store));
        $rate = $this->_taxCalculationModel->getRate($request);

        $baseAmount = $address->getBaseAmgiftwrapAmount();
        $amount = $address->getAmgiftwrapAmount();

        $baseTaxAmount = $this->_taxCalculationModel->calcTaxAmount($baseAmount, $rate);
        $taxAmount = $this->_taxCalculationModel->calcTaxAmount($amount, $rate);

        $address->setBaseAmgiftwrapTaxAmount($baseTaxAmount);
        $address->setAmgiftwrapTaxAmount($taxAmount);

        $address->setBaseTaxAmount($address->getBaseTaxAmount() + $baseTaxAmount);
        $address->setTaxAmount($address->getTaxAmount() + $taxAmount);
        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $baseTaxAmount);
        $address->setGrandTotal($address->getGrandTotal() + $taxAmount);

        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amgiftwrapDesignId = $address->getQuote()->getAmgiftwrapDesignId();
        $amgiftwrapCardId = $address->getQuote()->getAmgiftwrapCardId();
        if (!$amgiftwrapCardId && !$amgiftwrapDesignId) {
            return $this;
        }
        $displayType = Mage::getStoreConfig('amgiftwrap/tax/cart_display');
        if($displayType == Mage_Tax_Model_Config::DISPLAY_TYPE_BOTH || $displayType == Mage_Tax_Model_Config::DISPLAY_TYPE_INCLUDING_TAX) {
            $address->addTotal(
                array(
                    "code" => $this->getCode(),
                    "title" => Mage::helper('amgiftwrap')->__("Gift Wrap (Incl. Tax)"),
                    "value" => $address->getAmgiftwrapAmount() + $address->getAmgiftwrapTaxAmount()
                )
            );
        }

        return $this;
    }
}
