<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */


class Amasty_Giftwrap_Model_Sales_Order_Total_Creditmemo_Tax_Amgiftwrap extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $order                = $creditmemo->getOrder();
        $amgiftwrapTaxAmountLeft = $order->getAmgiftwrapTaxAmountInvoiced() - $order->getAmgiftwrapTaxAmountRefunded();
        $baseAmgiftwrapTaxAmountLeft = $order->getBaseAmgiftwrapTaxAmountInvoiced() - $order->getBaseAmgiftwrapTaxAmountRefunded();

        $creditmemo->setAmgiftwrapTaxAmount($amgiftwrapTaxAmountLeft);
        $creditmemo->setBaseAmgiftwrapTaxAmount($baseAmgiftwrapTaxAmountLeft);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $amgiftwrapTaxAmountLeft);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseAmgiftwrapTaxAmountLeft);
        $creditmemo->setTaxAmount($creditmemo->getTaxAmount() + $amgiftwrapTaxAmountLeft);
        $creditmemo->setBaseTaxAmount($creditmemo->getBaseTaxAmount() + $baseAmgiftwrapTaxAmountLeft);

        return $this;
    }
}
