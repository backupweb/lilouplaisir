<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */


class Amasty_Giftwrap_Model_Sales_Order_Total_Invoice_Tax_Amgiftwrap extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order                = $invoice->getOrder();
        $amgiftwrapTaxAmountLeft = $order->getAmgiftwrapTaxAmount() - $order->getAmgiftwrapTaxAmountInvoiced();
        $baseAmgiftwrapTaxAmountLeft = $order->getBaseAmgiftwrapTaxAmount() - $order->getBaseAmgiftwrapTaxAmountInvoiced();

        $invoice->setAmgiftwrapTaxAmount($amgiftwrapTaxAmountLeft);
        $invoice->setBaseAmgiftwrapTaxAmount($baseAmgiftwrapTaxAmountLeft);

        if (!$invoice->isLast()) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $amgiftwrapTaxAmountLeft);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseAmgiftwrapTaxAmountLeft);

            $invoice->setTaxAmount($invoice->getTaxAmount() + $amgiftwrapTaxAmountLeft);
            $invoice->setBaseTaxAmount($invoice->getBaseTaxAmount() + $baseAmgiftwrapTaxAmountLeft);
        }


        return $this;
    }
}
