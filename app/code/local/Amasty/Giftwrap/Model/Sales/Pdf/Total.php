<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */
class Amasty_Giftwrap_Model_Sales_Pdf_Total extends Mage_Sales_Model_Order_Pdf_Total_Default
{
    public function getTotalsForDisplay()
    {
        $order = $this->getOrder();
        $amount = $order->formatPriceTxt($this->getAmount());
        $amountInclTax = $order->formatPriceTxt($this->getAmount() + $this->getSource()->getAmgiftwrapTaxAmount());

        $helper   = Mage::helper('amgiftwrap');
        $store    = $order->getStore();
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $title    = $this->getTitle();
        $displayType = Mage::getStoreConfig('amgiftwrap/tax/sales_display', $store);

        $totals = array();
        if($displayType == Mage_Tax_Model_Config::DISPLAY_TYPE_BOTH || $displayType == Mage_Tax_Model_Config::DISPLAY_TYPE_INCLUDING_TAX) {
            if($displayType == Mage_Tax_Model_Config::DISPLAY_TYPE_BOTH) {
                $totals[] = array(
                    'amount'    => $this->getAmountPrefix() . $amount,
                    'label'     => $helper->__($title . ' (Excl. Tax)') . ':',
                    'font_size' => $fontSize
                );
            }
            $totals[] = array(
                'amount'    => $this->getAmountPrefix() . $amountInclTax,
                'label'     => $helper->__($title . ' (Incl. Tax)') . ':',
                'font_size' => $fontSize
            );
        } else {
            $totals[] = array(
                'amount'    => $this->getAmountPrefix() . $amount,
                'label'     => $helper->__($title) . ':',
                'font_size' => $fontSize
            );
        }

        return $totals;
    }
}
