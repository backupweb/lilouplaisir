<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */
class Amasty_Giftwrap_Model_Cards extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('amgiftwrap/cards');
    }

    public function massDelete($ids)
    {
        return $this->getResource()->massDelete($ids);
    }

    public function massEnable($ids)
    {
        return $this->getResource()->massEnable($ids);
    }

    public function massDisable($ids)
    {
        return $this->getResource()->massDisable($ids);
    }
}