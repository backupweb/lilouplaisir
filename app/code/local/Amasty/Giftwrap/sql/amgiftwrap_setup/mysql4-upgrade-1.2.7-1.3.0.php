<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */
$installer = $this;
$installer->startSetup();

$entityAttributesCodes = array(
    'amgiftwrap_tax_amount' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'base_amgiftwrap_tax_amount' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
);
foreach ($entityAttributesCodes as $code => $type) {
    $installer->addAttribute('quote_address', $code, array('type' => $type, 'visible' => false));
    $installer->addAttribute('creditmemo', $code, array('type' => $type, 'visible' => false));
    $installer->addAttribute('order', $code, array('type' => $type, 'visible' => false));
    $installer->addAttribute('invoice', $code, array('type' => $type, 'visible' => false));
}



$entityAttributesCodes = array(
    'amgiftwrap_tax_amount_refunded' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'amgiftwrap_tax_amount_invoiced' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'base_amgiftwrap_tax_amount_refunded' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'base_amgiftwrap_tax_amount_invoiced' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
);
foreach ($entityAttributesCodes as $code => $type) {
    $installer->addAttribute('order', $code, array('type' => $type, 'visible' => false));
}


$installer->endSetup();
