<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Giftwrap
 */

$this->startSetup();

$this->getConnection()->addColumn(
    $this->getTable('amgiftwrap/design'),
    'sort',
    "INT(11) COMMENT 'Sort position'"
);


$this->getConnection()->addColumn(
    $this->getTable('amgiftwrap/cards'),
    'sort',
    "INT(11) COMMENT 'Sort position'"
);

$this->endSetup();