<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */
class Amasty_Paction_Model_Command_Addcategory extends Amasty_Paction_Model_Command_Abstract 
{ 
    public function __construct($type)
    {
        parent::__construct($type);
        $this->_label      = 'Assign Category';
        $this->_fieldLabel = 'Category IDs'; 
    }
    
    /**
     * Executes the command
     *
     * @param array $ids product ids
     * @param int $storeId store id
     * @param string $val field value
     * @return string success message if any
     */    
    public function execute($ids, $storeId, $val)
    {
        $success = parent::execute($ids, $storeId, $val);

        $hlp = Mage::helper('ampaction');
        
        $catIds = explode(',', trim($val));
        if (!is_array($catIds)) {
            throw new Exception($hlp->__('Please provide comma separated category IDs'));
        }
        
        if ('replacecategory' == $this->_type) { // remove product(s) from all categories
            foreach ($ids as $productId) {
                $this->categoryUrlRedirect($productId, $storeId, $val);
            }

            $db    = Mage::getSingleton('core/resource')->getConnection('core_write');
            $table = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
            $db->delete($table, array('product_id IN(?)' => $ids));
            $this->_type = 'addcategory';
        }
        
        $numAffectedCats  = 0;
        $allAffectedProducts = array();
        
        foreach ($catIds as $categoryId) {
            $category = Mage::getModel('catalog/category')
                ->setStoreId($storeId)
                ->load($categoryId);
                
            if (!$category->getId()){
                $this->_errors[] = $hlp->__('ID = `%s` has been skipped', $categoryId);
                continue;
            }
            
            $positions = $category->getProductsPosition();
            $currentAffectedProducts = array();
            foreach ($ids as $productId){
                $has = isset($positions[$productId]);

                if ('addcategory' == $this->_type && !$has){ // add only new
                    $positions[$productId] = 0;
                    $currentAffectedProducts[] = $productId;
                }
                elseif('removecategory' == $this->_type && $has){ //remove only existing
                    unset($positions[$productId]);
                    $currentAffectedProducts[] = $productId;
                }
            }
                
            $category->setPostedProducts($positions); 
            try {  
                $_FILES['image'] = array();
                $_FILES['thumbnail'] = array();
                $category->save();
                ++$numAffectedCats;
                $allAffectedProducts = array_merge($allAffectedProducts, $currentAffectedProducts);
                $allAffectedProducts = array_unique($allAffectedProducts);
            } 
            catch (Exception $e) {
                $this->_errors[] = $hlp->__('Can not handle the category ID=%s, the error is: %s', 
                    $categoryId, $e->getMessage());
            }     
        }

        if ($numAffectedCats){
            $success = $hlp->__('Total of %d categories(s) and %d products(s) have been successfully updated.', 
                $numAffectedCats, count($allAffectedProducts));
        }

        if (version_compare(Mage::getVersion(), '1.4.1.0') > 0) {
            if (Mage::getStoreConfigFlag('ampaction/general/reindex')) {
                if (method_exists('Mage', 'getEdition')
                    && 'enterprise' == strtolower(Mage::getEdition())
                ) {
                    $event = Mage::getModel('index/event');
                    $event->setNewData(array(
                        'product_ids' => $ids
                    ));
                    Mage::getResourceSingleton('catalog/category_indexer_product')->catalogProductMassAction($event);
                    Mage::getResourceModel('catalog/product_indexer_price')->reindexProductIds($ids);
                    Mage::getResourceModel('cataloginventory/indexer_stock')->reindexProducts($ids);
                    Mage::getResourceModel('catalog/product_indexer_eav_source')->reindexEntities($ids);
                    $catalogSearchIndexer = Mage::getResourceModel('catalogsearch/fulltext');
                    $catalogSearchIndexer->rebuildIndex(null, $ids);
                    Mage::app()->getCacheInstance()->clean('full_page');
                    Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => 'full_page'));
                } else {
                    $process = Mage::getModel('index/indexer')->getProcessByCode('catalog_category_product');
                    $process->reindexAll();
                }
            } else {
                $process = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_product');
                $process->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
                $process->save();
            }
        }

        return $success; 
    }

    /**
     * @param int $productId
     * @param int $storeId
     * @param int $categoryId
     */
    protected function categoryUrlRedirect ($productId, $storeId, $categoryId)
    {
        $rewriteClass = 'core/url_rewrite';

        if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
            $rewriteClass = 'enterprise_urlrewrite/redirect';
        }
        $rewrite = Mage::getModel($rewriteClass);
        $product = Mage::getModel('catalog/product')->load($productId);
        $category = Mage::getModel('catalog/category');
        $categoryIds = $product->getCategoryIds();
        $productUrl = $product->getUrlPath();
        $oldUrls = array();

        foreach ($categoryIds as $catId) {
            $oldCategory = $category->setStoreId($storeId)->load($catId);
            $oldUrl = substr($oldCategory->getUrlPath(), 0, strpos($oldCategory->getUrlPath(), '/'))
                . '/'
                . $oldCategory->getUrlKey();
            array_push($oldUrls, $oldUrl);
        }
        $newCategory = $category->setStoreId($storeId)->load($categoryId);
        $categoryUrl = substr($newCategory->getUrlPath(), 0, strpos($newCategory->getUrlPath(), '/'))
            . '/'
            . $newCategory->getUrlKey();

        foreach ($oldUrls as $oldUrl) {
            $fromUrl = $oldUrl . '/' . $productUrl;
            $toUrl = $categoryUrl . '/' . $productUrl;

            if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
                $rewriteId = $rewrite->getCollection()
                    ->addFieldToFilter('target_path', $fromUrl)
                    ->getFirstItem()
                    ->getData('redirect_id');

                if ($rewriteId) {
                    $rewrite->load($rewriteId);
                } else {
                    $rewrite->loadByRequestPath($fromUrl, $storeId);
                }
                $rewrite->setStoreId($storeId)
                    ->setOptions('RP')
                    ->setIdentifier($fromUrl)
                    ->setTargetPath($toUrl)
                    ->setEntityType(Mage_Core_Model_Url_Rewrite::TYPE_CUSTOM);
                $rewrite->save();
            } else {
                $rewriteId = $rewrite->getCollection()
                    ->addFieldToFilter('target_path', $fromUrl)
                    ->addFieldToFilter('product_id', $productId)
                    ->getFirstItem()
                    ->getData('url_rewrite_id');

                if ($rewriteId) {
                    $rewrite->load($rewriteId);
                } else {
                    $rewrite->loadByIdPath($fromUrl);
                }

                $rewrite->setStoreId($storeId)
                    ->setOptions('RP')
                    ->setIdPath($fromUrl)
                    ->setRequestPath($fromUrl)
                    ->setIsSystem(0)
                    ->setProductId($productId)
                    ->setTargetPath($toUrl);
                $rewrite->save();
            }
        }
    }

    /**
     * Returns value field options for the mass actions block
     *
     * @param string $title field title
     * @return array
     */
    protected function _getValueField($title)
    {
        $field = parent::_getValueField($title);
        
        $storeId    = (int)Mage::app()->getRequest()->getParam('store', 0);
        if (Mage::getStoreConfig('ampaction/general/categories', $storeId)) {
            $rootId = Mage::app()->getStore($storeId)->getRootCategoryId();
            $field['ampaction_value']['label']    = Mage::helper('ampaction')->__('Category');
            $field['ampaction_value']['values']   = $this->getTree($rootId);
            if (Mage::getStoreConfig('ampaction/general/cat_type', $storeId)) {
                $field['ampaction_value']['type'] = 'multiselect';
            } else {
                $field['ampaction_value']['type'] = 'select';
            }
        } 
        
        return $field;      
    } 
       
    /**
     * Genarates tree of all categories
     *
     * @param int $rootId root category id
     * @return array sorted list category_id=>title
     */
    protected function getTree($rootId)
    {
        // BEGIN: Base Algorithm
        $tree = array();
        
        $collection = Mage::getModel('catalog/category')
            ->getCollection()->addNameToResult();
        
        $pos = array();
        foreach ($collection as $cat){
            $path = explode('/', $cat->getPath());
            if ((!$rootId || in_array($rootId, $path)) && $cat->getLevel() && $cat->getName()){
                $tree[$cat->getId()] = array(
                    'label' => str_repeat('--', $cat->getLevel()) . $cat->getName(), 
                    'value' => $cat->getId(),
                    'path'  => $path,
                );
            }
            $pos[$cat->getId()] = $cat->getPosition();
        }
        
        foreach ($tree as $catId => $cat){
            $order = array();
            foreach ($cat['path'] as $id){
		$order[] = isset($pos[$id]) ? $pos[$id] : 0;
            }
            $tree[$catId]['order'] = $order;
        }
        
        usort($tree, array($this, 'compare'));
        // END: Base Algorithm

        // BEGIN: Alternative Algorithm
        /*$categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            //->addFieldToFilter('is_active', array('eq'=>'1'))
            ->load()
            ->toArray();

        $tree = array();
        foreach ($categoriesArray as $categoryId => $category) {
            if (array_key_exists('name', $category) && $category['name']) {
                $path = explode('/', $category['path']);
                $tree[] = array(
                    'label' => str_repeat('--', $category['level']) . $category['name'], // in case with broken levels can use count($path) instead $category['level']
                    'value' => $categoryId,
                    'path'  => $path,
                );
            }
        }*/
        // END: Alternative Algorithm
        
        return $tree;
    }
    
    /**
     * Compares category data. Must be public as used as a callback value
     *
     * @param array $a
     * @param array $b
     * @return int 0, 1 , or -1
     */
    public function compare($a, $b)
    {
        foreach ($a['path'] as $i => $id){
            if (!isset($b['path'][$i])){ 
                // B path is shorther then A, and values before were equal
                return 1;
            }
            if ($id != $b['path'][$i]){
                // compare category positions at the same level
                return ($a['order'][$i] < $b['order'][$i]) ? -1 : 1;
            }
        }
        // B path is longer or equal then A, and values before were equal
        return ($a['value'] == $b['value']) ? 0 : -1;
    }      
}
