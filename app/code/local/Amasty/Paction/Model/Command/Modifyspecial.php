<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */
class Amasty_Paction_Model_Command_Modifyspecial extends Amasty_Paction_Model_Command_Modifyprice
{
    public function __construct($type, $dateFrom = '', $dateTo = '')
    {
        parent::__construct($type, $dateFrom, $dateTo);
        $this->_label = 'Update Special Price';
        $this->_fieldLabel = 'Value';
    }
    
    protected function _getAttrCode()
    {
        return 'special_price';
    }

    protected function _getValueField($title)
    {
        $field = parent::_getValueField($title);
        $field = $field; // prvents Zend Studio validation error
        $field['ampaction_from']['label'] = 'From';
        $field['ampaction_from']['type'] = 'datetime';
        $field['ampaction_from']['name'] = 'ampaction_from';
        $field['ampaction_from']['format'] = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $field['ampaction_from']['image'] = $this->getSkinUrl('images/grid-cal.gif');

        $field['ampaction_to']['label'] = 'To';
        $field['ampaction_to']['type'] = 'datetime';
        $field['ampaction_to']['name'] = 'ampaction_to';
        $field['ampaction_to']['format'] = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $field['ampaction_to']['image'] = $this->getSkinUrl('images/grid-cal.gif');

        return $field;
    }

    public function getSkinUrl($file = null, array $params = array())
    {
        return Mage::getDesign()->getSkinUrl($file, $params);
    }
}
