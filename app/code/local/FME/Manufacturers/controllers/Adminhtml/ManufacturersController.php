<?php
/**
 * Manufacturers extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Manufacturers
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Manufacturers_Adminhtml_ManufacturersController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('manufacturers/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manufacturers Manager'), Mage::helper('adminhtml')->__('Manufacturer Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	protected function _initManufacturerProducts() {
		
		$manufacturers = Mage::getModel('manufacturers/manufacturers');
        $manufacturerId  = (int) $this->getRequest()->getParam('id');
		if ($manufacturerId) {
        	$manufacturers->load($manufacturerId);
		}
		Mage::register('current_manufacturer_products', $manufacturers);
		return $manufacturers;
		
	}


	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('manufacturers/manufacturers')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('manufacturers_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('manufacturers/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_edit'))
				->_addLeft($this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manufacturers')->__('Manufacturer does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		
		if ($data = $this->getRequest()->getPost()) {
			
			//Upload Logo 
			$files = $this->uploadFiles( $_FILES ); 
            if( $files && is_array($files) ){
                for( $f=0; $f<count($files); $f++ ){
                    if( $files[$f] ){
                        $fieldname = str_replace('_uploader','',$files[$f]['fieldname']);
                        if( array_key_exists($fieldname, $data) ){
                            $data['m_logo'] = $files[$f]['url'];
                        } 
                    }  
                }  
            }
			
			//Save Image for Grid Thumbnail
			$config = Mage::getModel('manufacturers/image_config');
			$image = Mage::getModel('media/image');
			$image->setConfig($config);
			
			try {
				$imageFile =  Mage::getBaseUrl('media').$data['m_logo'];
				$str = $data['m_logo'];
				$aryimg = explode("/",$str);
				$img = $aryimg[2].'/'.$aryimg[3].'/'.$aryimg[4];
				$img = ltrim(rtrim($img));
				$imgPath = $image->getSpecialLink($img,'100');
				$data['m_logo_thumb'] = '<img src="'.$imgPath.'" border="0" />';
				//End of save Grid image
			} catch(Exception $e) {}
				
			//Set Related Products
			 /*$links = $this->getRequest()->getPost('links');
			 if (isset($links['related'])) {
					$productIds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']);
			 }
			 $productString = "";
			 foreach ($productIds as $_product) {
				$productString .= $_product.",";
			 }*/
			 
			 //Save Products ID"s that is realted to this video!
			$data['m_product_ids'] = $_POST['prid']; 
				
			$model = Mage::getModel('manufacturers/manufacturers');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('manufacturers')->__('Manufacturer was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manufacturers')->__('Unable to find Manufacturer to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				
			    $id     = $this->getRequest()->getParam('id');
				$model = Mage::getModel('manufacturers/manufacturers');
				$object  = Mage::getModel('manufacturers/manufacturers')->load($id);
				$model->setId($this->getRequest()->getParam('id'));
		
				//Delete All Associated Products Links
				Mage::getModel('manufacturers/manufacturers')->deleteManufacturerStores($id);
				
				//Delete All Associated Store Links
				Mage::getModel('manufacturers/manufacturers')->deleteManufacturerProductLinks($id);
				
				//Delete Main Table data
				$model->delete();
				 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Manufacturer was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	/**
     * Get related products grid and serializer block
     */
    public function productsAction()
    {
		$this->_initManufacturerProducts();
		$this->loadLayout();
        $this->getLayout()->getBlock('manufacturers.edit.tab.products')
		 				  ->setManufacturerProductsRelated($this->getRequest()->getPost('products_related', null));
        $this->renderLayout();
    }
	
	/**
     * Get related products grid
     */
    public function productsGridAction()
    {
        $this->_initManufacturerProducts();
		//Push Existing Values in Array
		$productsarray = array();
		$manufacturerId  = (int) $this->getRequest()->getParam('id');
		foreach (Mage::registry('current_manufacturer_products')->getManufacturerRelatedProducts($manufacturerId) as $products) {
           $productsarray = $products["product_id"];
        }
		array_push($_POST["products_related"],$productsarray);
		Mage::registry('current_manufacturer_products')->setManufacturerProductsRelated($productsarray);
		
		$this->loadLayout();
        $this->getLayout()->getBlock('manufacturers.edit.tab.products')
            			  ->setManufacturerProductsRelated($this->getRequest()->getPost('products_related', null));
        $this->renderLayout();
    }

    public function massDeleteAction() {
        $manufacturersIds = $this->getRequest()->getParam('manufacturers');
        if(!is_array($manufacturersIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select Manufacturer(s)'));
        } else {
            try {
                foreach ($manufacturersIds as $manufacturersId) {
                    
					$manufacturers = Mage::getModel('manufacturers/manufacturers')->load($manufacturersId);
					/*$pathImg = BP . DS . 'media' . DS . $manufacturers->getMLogo();
					if ($pathImg) {	
						unlink($pathImg); 
					}
					*/
					//Delete All Associated Products Links
					Mage::getModel('manufacturers/manufacturers')->deleteManufacturerStores($manufacturersId);
					
					//Delete All Associated Store Links
					Mage::getModel('manufacturers/manufacturers')->deleteManufacturerProductLinks($manufacturersId);
					
                    $manufacturers->delete();
					
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($manufacturersIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $manufacturersIds = $this->getRequest()->getParam('manufacturers');
        if(!is_array($manufacturersIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Manufacturer(s)'));
        } else {
            try {
                foreach ($manufacturersIds as $manufacturersId) {
                    $manufacturers = Mage::getSingleton('manufacturers/manufacturers')
                        ->load($manufacturersId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($manufacturersIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    /**
     * Import and export Page
     *
     */
  	public function importExportAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manufacturers/import')
            ->_addContent($this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_importExport'))
            ->renderLayout();
    }
	
	/**
     * export action from import/export Manufacturers
     *
     */
    public function exportPostAction()
    {
        $fileName   = 'manufacturers.csv';
        $content    = $this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_ExportGrid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	/**
     * import action from import/export Manufacturers
     *
     */
    public function importPostAction()
    {
        if ($this->getRequest()->isPost() && !empty($_FILES['import_manufacturers_file']['tmp_name'])) {
            try {
                $number = $this->_importManufacturersAdmin();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('manufacturers')->__('%d new item(s) were imported',$number));
            }
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manufacturers')->__('Invalid file upload attempt'));
            }
        }
        else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manufacturers')->__('Invalid file upload attempt'));
        }
        $this->_redirect('*/*/importExport');
    }
  
    protected function _importManufacturersAdmin()
    {
        $fileName   = $_FILES['import_manufacturers_file']['tmp_name'];
        $csvObject  = new Varien_File_Csv();
        $csvData = $csvObject->getData($fileName);
		$number = 0;
        /** checks columns */
        $csvFields  = array(
            0    => Mage::helper('manufacturers')->__('Name'),
			1    => Mage::helper('manufacturers')->__('Identifier'),
            2    => Mage::helper('manufacturers')->__('Website'),
            3    => Mage::helper('manufacturers')->__('Address'),
            4    => Mage::helper('manufacturers')->__('Logo'),
			5    => Mage::helper('manufacturers')->__('Featured'),
			6    => Mage::helper('manufacturers')->__('Contact Name'),
			7    => Mage::helper('manufacturers')->__('Contact Phone'),
			8    => Mage::helper('manufacturers')->__('Contact Fax'),
			9    => Mage::helper('manufacturers')->__('Contact Email'),
			10    => Mage::helper('manufacturers')->__('Contact Address'),
			11   => Mage::helper('manufacturers')->__('Manufacturer Details'),
			12   => Mage::helper('manufacturers')->__('Status'),
			13   => Mage::helper('manufacturers')->__('Products')
        );
		

        if ($csvData[0] == $csvFields) {
            foreach ($csvData as $k => $v) {
                if ($k == 0) {
                    continue;
                }

                //end of file has more then one empty lines
                if (count($v) <= 1 && !strlen($v[0])) {
                    continue;
                }

                if (count($csvFields) != count($v)) {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manufacturers')->__('Invalid file upload attempt'));
                }
				
				
				
				if (!empty($v[0])) {
					
				    $v[0] = trim(preg_replace('/[^\w\s-]/','',$v[0]));					
					$v[3] = trim(preg_replace('/[^\w\s-]/','',$v[3]));
					$v[5] = trim(preg_replace('/[^\w\s-]/','',$v[5]));
					$v[6] = trim(preg_replace('/[^\w\s-]/','',$v[6]));
					$v[10] = trim(preg_replace('/[^\w\s-]/','',$v[10]));
					$v[11] = trim(preg_replace('/[^\w\s-]/','',$v[11]));
					$v[12] = trim(preg_replace('/[^\w\s-]/','',$v[12]));
					$v[13] = trim(preg_replace('/[^\w\s-]/','|',$v[13]));						
					
					$resource = Mage::getSingleton('core/resource');
					$read= $resource->getConnection('core_read');				
					$mmnadminTable = $resource->getTableName('manufacturers/manufacturers');
					$select = $read->select()
											->from($mmnadminTable,array('manufacturers_id'))
											->where("m_name=?",$v[0])									
											->limit(1);
									
					if($read->fetchOne($select)){
						 continue;
					}

					
					//Manufacturer Products
				    $productidsString  = trim($v[13], '|');
					$productIds = explode("|", $productidsString);		
					$result = array_unique($productIds);
					$comma_separated = implode(",", $result);
					$_POST['productIds'] = $comma_separated;					
					
					//Set Store ID
					$storesArray = array();
					$storeID = Mage_Core_Model_App::ADMIN_STORE_ID;
					$storesArray[] = $storeID;
					$_POST['stores'] =  $storesArray;
					
					//Upload Image
					$data["m_logo"] = $this->importLogo(false,$v[4]);
					
					//Save Image for Grid Thumbnail
					$config = Mage::getModel('manufacturers/image_config');
					$image = Mage::getModel('media/image');
					$image->setConfig($config);
					
					try {
						$imageFile =  Mage::getBaseUrl('media').$data['m_logo'];
						$str = $data['m_logo'];
						$aryimg = explode("/",$str);
						$img = $aryimg[2].'/'.$aryimg[3].'/'.$aryimg[4];
						$img = ltrim(rtrim($img));
						$imgPath = $image->getSpecialLink($img,'100');
						$data['m_logo_thumb'] = '<img src="'.$imgPath.'" border="0" />';
						//End of save Grid image
					} catch(Exception $e) {}
					
					
					
					$data  = array(
						'm_name'=>$v[0],
						'identifier'=>$v[1],
						'm_website' => $v[2],
						'm_address' => $v[3],
						'm_logo'  => $data["m_logo"],
						'm_logo_thumb'=>$data['m_logo_thumb'],
						'm_featured' => $v[5],
						'm_contact_name' => $v[6],
						'm_contact_phone'  => $v[7],
						'm_contact_fax'=>$v[8],
						'm_contact_email' => $v[9],
						'm_contact_address' => $v[10],
						'm_details'  => $v[11],
						'status'=>$v[12]
					);
														
					$model = Mage::getModel('manufacturers/manufacturers');		
					$model->setData($data)
						  ->setId($this->getRequest()->getParam('id'));
			
					try {
						if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
							$model->setCreatedTime(now())
								->setUpdateTime(now());
						} else {
							$model->setUpdateTime(now());
						}	
						$model->save();
					} catch (Exception $e) {
						Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
						Mage::getSingleton('adminhtml/session')->setFormData($data);
						$this->_redirect('*/*/importExport');
						return;
					}
					$number++;
                }
            }  
        }
        else {
            Mage::throwException(Mage::helper('manufacturers')->__('Invalid file format upload attempt'));
        }	
		return $number;
    }
  
  
    public function exportCsvAction()
    {
        $fileName   = 'manufacturers.csv';
        $content    = $this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'manufacturers.xml';
        $content    = $this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
	protected function uploadFiles( $files ){
        if( !empty($files) && is_array($files) ){
            $result = array();
            foreach( $files as $file=>$info ){
                $result[] = $this->uploadFile( $file );
            }
            return $result;
        }
    }
	
	protected function importLogo($move = false, $logoFile) {
				
		$file = Mage::getBaseDir('media') . DS . $logoFile;
		$baseMediaPath = Mage::getBaseDir('media') . DS .  'manufacturers' . DS . 'files';
		$dynamicScmsURL = 'manufacturers' . DS . 'files';
		
		$pathinfo = pathinfo($file);
		$fileName       = Varien_File_Uploader::getCorrectFileName($pathinfo['basename']);
		$dispretionPath = Varien_File_Uploader::getDispretionPath($fileName);
		$fileName       = $dispretionPath . DS . $fileName;
		$destinationFilePath = $baseMediaPath;
		
		$fileName = $dispretionPath . DS . Varien_File_Uploader::getNewFileName($file);
		$destinationDIR = $baseMediaPath . $fileName;

		$ioAdapter = new Varien_Io_File();
		$ioAdapter->setAllowCreateFolders(true);
		$distanationDirectory = dirname($destinationDIR);
		
		try {		
			$ioAdapter->open(array(
				'path'=>$distanationDirectory
			));

			if ($move) {
				$ioAdapter->mv($file, $destinationDIR);
			} else {
				$ioAdapter->cp($file, $destinationDIR);
				$ioAdapter->chmod($destinationDIR, 0777);
			}
		}
		catch (Exception $e) {
			Mage::throwException(Mage::helper('catalog')->__('Failed to move file: %s', $e->getMessage()));
		}
		
		return $dynamicScmsURL.$fileName;
	}
	
	protected function uploadFile( $file_name ){

        if( !empty($_FILES[$file_name]['name']) ){
            $result = array();
            $dynamicScmsURL = 'manufacturers' . DS . 'files';
            $baseScmsMediaURL = Mage::getBaseUrl('media') . DS . 'manufacturers' . DS . 'files';
            $baseScmsMediaPath = Mage::getBaseDir('media') . DS .  'manufacturers' . DS . 'files';
            
            $uploader = new Varien_File_Uploader( $file_name );
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf','xls','xlsx','doc','docx'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save( $baseScmsMediaPath );
       
            $file = str_replace(DS, '/', $result['file']);
            if( substr($baseScmsMediaURL, strlen($baseScmsMediaURL)-1)=='/' && substr($file, 0, 1)=='/' )    $file = substr($file, 1);
						
            $ScmsMediaUrl = $dynamicScmsURL.$file;
            
            $result['fieldname'] = $file_name;
            $result['url'] = $ScmsMediaUrl;
            $result['file'] = $result['file'];
            return $result;
        } else {
            return false;
        }
    } 
		
	/**
     * Get faqs products grid and serializer block
     */
    public function productAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * Get faqs products grid
     */
    public function productGridAction()
    {
	echo 'Function ===> productgridaction';
        $this->_initProduct();
        $this->loadLayout();
		$data=$this->getRequest()->getPost();
        $this->renderLayout();
    }
     
    public function gridAction()
	{
		 
	    $this->getResponse()->setBody(
            $this->getLayout()->createBlock('manufacturers/adminhtml_manufacturers_edit_tab_product')->toHtml()
        );
	
	}

    /**
     * Get specified tab grid
     */
    public function gridOnlyAction()
    {
        echo 'Function ===> GridOnlyAction';
		$this->_initProduct();
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('adminhtml/manufacturers_edit_tab_product')
                ->toHtml()
        );
    }
	

	
}