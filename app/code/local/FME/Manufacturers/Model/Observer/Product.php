<?php

class FME_Manufacturers_Model_Observer_Product extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the manufacturers_id refers to the key field in your database table.
        $this->_init('manufacturers/manufacturers', 'manufacturers_id');
    }
	
	/**
	 * Inject one tab into the product edit page in the Magento admin
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function injectTabs(Varien_Event_Observer $observer)
	{
		$block = $observer->getEvent()->getBlock();
		
		if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs) {
			if ($this->_getRequest()->getActionName() == 'edit' || $this->_getRequest()->getParam('type')) {
				$block->addTab('custom-product-tab-01', array(
					'label'     => 'Manufacturers',
					'content'   => $block->getLayout()->createBlock('adminhtml/template', 'custom-tab-content', array('template' => 'manufacturers/content.phtml'))->toHtml(),
				));
			}
		}
	}

	/**
	 * This method will run when the product is saved
	 * Use this function to update the product model and save
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function saveTabData(Varien_Event_Observer $observer)
	{
		if ($post = $this->_getRequest()->getPost()) {

			try {
				
			 	// Load the current product model	
				$product = Mage::registry('product');
				$condition = $this->_getWriteAdapter()->quoteInto('product_id = ?', $product["entity_id"]);
				$this->_getWriteAdapter()->delete($this->getTable('manufacturers_products'), $condition);	
	
				$productsArray = array();
				$productsArray['manufacturers_id'] = $post["manufacturers_id"];
				$productsArray['product_id'] = $product["entity_id"];
				
				$this->_getWriteAdapter()->insert($this->getTable('manufacturers_products'), $productsArray);
								
			}
			 catch (Exception $e) {
			 	Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			 }
		}
	}
	
	/**
	 * Shortcut to getRequest
	 */
	protected function _getRequest()
	{
		return Mage::app()->getRequest();
	}
}
