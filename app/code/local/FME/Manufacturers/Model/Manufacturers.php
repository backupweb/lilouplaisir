<?php
/**
 * Manufacturers extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Manufacturers
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Manufacturers_Model_Manufacturers extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('manufacturers/manufacturers');
    }
	
	/**
     * Retrieve related products
     *
     * @return array
     */
    public function getManufacturerRelatedProducts($manufacturerId)
    {
					
			$collection = Mage::getModel('manufacturers/manufacturers')->getCollection()
					  ->addManufacturerFilter($manufacturerId);
					  
			$collection->getSelect()
            ->joinLeft(array('related' => 'manufacturers_products'),
                        'main_table.manufacturers_id = related.manufacturers_id'
                )
			->order('main_table.manufacturers_id');
			
			return $collection->getData();

    }
	
	public function checkManufacturer($id)
    {
        return $this->_getResource()->checkManufacturer($id);
    }
	
	/*
     * Delete Manufacturer Stores
     * @return Array
     */
	public function deleteManufacturerStores($id){
		return $this->getResource()->deleteManufacturerStores($id);
		
	}
	
	/*
     * Delete Manufacturer Product Links
     * @return Array
     */
	public function deleteManufacturerProductLinks($id){
		return $this->getResource()->deleteManufacturerProductLinks($id);
		
	}
	
	/**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param   string $identifier
     * @param   int $storeId
     * @return  int
     */
    public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
	
	
}