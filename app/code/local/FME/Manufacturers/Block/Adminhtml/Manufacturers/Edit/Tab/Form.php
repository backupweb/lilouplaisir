<?php
/**
 * Manufacturers extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Manufacturers
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Manufacturers_Block_Adminhtml_Manufacturers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('manufacturers_form', array('legend'=>Mage::helper('manufacturers')->__('Manufacturer information')));
		
		$fieldset->addField('m_name', 'text', array(
		  'label'     => Mage::helper('manufacturers')->__('Name'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'm_name',
		));
		
		$fieldset->addField('identifier', 'text', array(
            'name'      => 'identifier',
            'label'     => Mage::helper('manufacturers')->__('SEF URL Identifier'),
            'title'     => Mage::helper('manufacturers')->__('SEF URL Identifier'),
            'required'  => true,
            'class'     => 'validate-identifier',
            'after_element_html' => '<p class="nm"><small>' . Mage::helper('manufacturers')->__('(eg: domain.com/identifier)') . '</small></p>',
        ));
		
		$fieldset->addField('m_website', 'text', array(
		  'label'     => Mage::helper('manufacturers')->__('Website'),
		  'required'  => false,
		  'name'      => 'm_website',
		));
		
		
		$fieldset->addField('m_featured', 'select', array(
		  'label'     => Mage::helper('manufacturers')->__('Make this Featured'),
		  'name'      => 'm_featured',
		  'values'    => array(
			  array(
				  'value'     => '1',
				  'label'     => Mage::helper('manufacturers')->__('Yes'),
			  ),
		
			  array(
				  'value'     => '0',
				  'label'     => Mage::helper('manufacturers')->__('No'),
			  ),
		  ),
		));
		
		$fieldset->addField('m_address', 'editor', array(
		  'name'      => 'm_address',
		  'label'     => Mage::helper('manufacturers')->__('Address'),
		  'title'     => Mage::helper('manufacturers')->__('Address'),
		  'style'     => 'width:275px; height:100px;',
		  'wysiwyg'   => false,
		  'required'  => false,
		));
		
		
		
		$fieldset->addField('store_id','multiselect',array(
		'name'      => 'stores[]',
		'label'     => Mage::helper('manufacturers')->__('Store View'),
		'title'     => Mage::helper('manufacturers')->__('Store View'),
		'required'  => true,
		'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
		));
		
		$fieldset->addField('status', 'select', array(
		  'label'     => Mage::helper('manufacturers')->__('Status'),
		  'name'      => 'status',
		  'values'    => array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('manufacturers')->__('Enabled'),
			  ),
		
			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('manufacturers')->__('Disabled'),
			  ),
		  ),
		));
		
		$fieldset->addField('prid', 'hidden', array(
          	'label'     => Mage::helper('manufacturers')->__(''),
          	'name'      => 'prid',
	  		'id'      => 'prid',
          
      	));
		
		$fieldset->addField('m_details', 'editor', array(
		  'name'      => 'm_details',
		  'label'     => Mage::helper('manufacturers')->__('About Manufacturer'),
		  'title'     => Mage::helper('manufacturers')->__('About Manufacturer'),
		  'style'     => 'width:500px; height:300px;',
		  'wysiwyg'   => false,
		  'required'  => false,
		));
		
		try {
			$object = Mage::getModel('manufacturers/manufacturers')->load( $this->getRequest()->getParam('id') );
			$note = false;
			$img = '';
			if( $object->getMLogo() ){
			$config = Mage::getModel('manufacturers/image_config');
			$image = Mage::getModel('media/image');
			$image->setConfig($config);
			$imageFile =  Mage::getBaseUrl('media').$object->getMLogo();
				$strimg = $object->getMLogo();
				$aryimg = explode("/",$strimg);
				$imge = $aryimg[2].'/'.$aryimg[3].'/'.$aryimg[4];
				$imge = ltrim(rtrim($imge));					
				If ($imageFile) {
					$img = '<img src="' . $image->getSpecialLink($imge,'50,50') . '" border="0" align="center"/>';
				}		
			}
		} catch (Exception $e) {
			$img = "";
		}
		$fieldset->addField('my_file_uploader', 'file', array(
			'label'        => Mage::helper('manufacturers')->__('Logo'),
			'note'      => $note,
			'name'        => 'my_file_uploader',
			'class'     => (($object->getMLogo()) ? '' : 'required-entry'),
			'required'  => (($object->getMLogo()) ? false : true),
			'after_element_html' => $img,
   		 )); 
		
		$fieldset->addField('m_logo_thumb', 'hidden', array(
		  'label'     => Mage::helper('manufacturers')->__(''),
		  'name'      => 'm_logo_thumb',
		  'id'      => 'm_logo_thumb',
		  
		));
		
		$fieldset->addField('my_file', 'hidden', array(
        	'name'        => 'my_file',
    	));
     
      if ( Mage::getSingleton('adminhtml/session')->getManufacturersData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getManufacturersData());
          Mage::getSingleton('adminhtml/session')->setManufacturersData(null);
      } elseif ( Mage::registry('manufacturers_data') ) {
          $form->setValues(Mage::registry('manufacturers_data')->getData());
      }
      return parent::_prepareForm();
  }
}