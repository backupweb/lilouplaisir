<?php
/**
 * Manufacturers extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Manufacturers
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */
 
 
class FME_Manufacturers_Block_Manufacturers_Toolbar extends Mage_Page_Block_Html_Pager
{
    
    protected function _construct()
    {
        parent::_construct();
        
        $this->setTemplate('manufacturers/manufacturers/toolbar.phtml');
    }
	
	

    /**
     * Retrieve current limit per page
     *
     * @return string
     */
    public function getLimit()
    {
	
	
		if (Mage::getStoreConfig('manufacturers/manufacturers/items_per_page'))
			$limit = Mage::getStoreConfig('manufacturers/manufacturers/items_per_page');
		else		
			$limit = 12;
		
		return $limit; 
    }

    /**
     * Retrieve Limit Pager URL
     *
     * @param int $limit
     * @return string
     */
    public function getLimitUrl($limit)
    {
        return $this->getPagerUrl(array(
            $this->getLimitVarName() => $limit,
            $this->getPageVarName() => null
        ));
    }
	
	
}
