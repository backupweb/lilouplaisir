<?php
/**
 * Manufacturers extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Manufacturers
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Manufacturers_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_PATH_LIST_PAGE_TITLE				=	'manufacturers/manufacturers/page_title';
	const XML_PATH_LIST_MAIN_HEADING		    =	'manufacturers/manufacturers/main_heading';
	const XML_PATH_LIST_PRODUCT_PAGE_LOGO_HEADING	=	'manufacturers/productpagelogo/product_page_logo_heading';
	const XML_PATH_LIST_IDENTIFIER				=	'manufacturers/manufacturers/identifier';
	const XML_PATH_LIST_ITEMS_PER_PAGE			=	'manufacturers/manufacturers/items_per_page';
	const XML_PATH_LIST_LIMIT_DESCRIPTION		=	'manufacturers/manufacturers/limit_description';
	const XML_PATH_LIST_META_DESCRIPTION		=	'manufacturers/manufacturers/meta_description';
	const XML_PATH_LIST_META_KEYWORDS			=	'manufacturers/manufacturers/meta_keywords';
	const XML_PATH_SEO_URL_SUFFIX				=	'manufacturers/seo/url_suffix';
	
	
	public function getListPageTitle()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_PAGE_TITLE);
	}
	
	public function getMainHeadingText()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_MAIN_HEADING);
	}
	
	public function getProductPageLogoText()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_PRODUCT_PAGE_LOGO_HEADING);
	}
	
	public function getListIdentifier()
	{
		$identifier = Mage::getStoreConfig(self::XML_PATH_LIST_IDENTIFIER);
		if ( !$identifier ) {
			$identifier = 'manufacturers';
		}
		return $identifier;
	}
	
	public function getListItemsPerPage()
	{
		return (int)Mage::getStoreConfig(self::XML_PATH_LIST_ITEMS_PER_PAGE);
	}
	
	public function getListLimitDescription()
	{
		return (int)Mage::getStoreConfig(self::XML_PATH_LIST_LIMIT_DESCRIPTION);
	}
	
	public function getListMetaDescription()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_META_DESCRIPTION);
	}
	
	public function getListMetaKeywords()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_META_KEYWORDS);
	}
	
	public function getUrl($identifier = null)
	{
		
		if ( is_null($identifier) ) {
			$url = Mage::getUrl('') . self::getListIdentifier() . self::getSeoUrlSuffix();
		} else {
			$url = Mage::getUrl('') . $identifier . self::getSeoUrlSuffix();
		}

		return $url;
		
	}
	public function getSeoUrlSuffix()
	{
		return Mage::getStoreConfig(self::XML_PATH_SEO_URL_SUFFIX);
	}
	
	public function recursiveReplace($search, $replace, $subject)
    {
        if(!is_array($subject))
            return $subject;

        foreach($subject as $key => $value)
            if(is_string($value))
                $subject[$key] = str_replace($search, $replace, $value);
            elseif(is_array($value))
                $subject[$key] = self::recursiveReplace($search, $replace, $value);

        return $subject;
    }

    public function extensionEnabled($extension_name)
	{
		$modules = (array)Mage::getConfig()->getNode('modules')->children();
		if (!isset($modules[$extension_name])
			|| $modules[$extension_name]->descend('active')->asArray()=='false'
			|| Mage::getStoreConfig('advanced/modules_disable_output/'.$extension_name)
		) return false;
		return true;
	}
}