<?php
/* 
 * Author : Dee
 */

?>
<?php
class Dee_NewByCategory_Block_FourCategories extends Mage_Catalog_Block_Product_Abstract
//class Dee_NewByCategory_Block_FourCategories extends Mage_Catalog_Model_Layer
{

    //Attributs
    protected $_sextoysProducts = array();
    protected $_massageProducts = array();
    protected $_accessoiresProducts = array();
    protected $_jeuxProducts = array();

    //Constructeur
    public function __construct() {
        parent::__construct();

        $storeId    = Mage::app()->getStore()->getId();
        $product    = Mage::getModel('catalog/product');
        /* @var $product Mage_Catalog_Model_Product */
        $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
        $cat = $category->getId();
        $cat2 = 5;
        var_dump(Mage::app()->getStore()->getCurrentCategoryId());
        $todayDate  = $product->getResource()->formatDate(time());
        $this->_sextoysProducts   = $product->setStoreId($storeId)->getCollection()
            //->addAttributeToFilter('lilou_selection', array('Yes'=>true))
            ->addAttributeToFilter('category_ids',array('finset'=>$cat2))
            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left')
        ;
       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($this->_sextoysProducts);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_sextoysProducts);

        $this->_sextoysProducts->setOrder('hot_deals')->setPageSize(3)->setCurPage(2);

        $this->setProductCollection($this->_sextoysProducts);
        

    }

    /****************************************************************/
    protected $_productsCount = null;

    const DEFAULT_PRODUCTS_COUNT = 5;

    protected function _beforeToHtml()
    {
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('special_price', array('notnull'=>true))
            //->setPageSize($this->getProductsCount())
            //product limited to 5
            ->setPageSize(5)
            ->setCurPage(3)
        ;
//        $this->setProductCollection($collection);
//
//        return parent::_beforeToHtml();
    }

    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
    /******************************************************************/

    //Methodes
    public function getSextoysProducts(){
        return $this->setProductCollection($this->_sextoysProducts);
    }

    public function getMassageProducts(){
        return $this->setProductCollection($this->_massageProducts);
    }

    public function getAccessoiresProducts(){
        return $this->setProductCollection($this->_accessoiresProducts);
    }

    public function getJeuxProducts(){
        return $this->setProductCollection($this->_jeuxProducts);
    }

    public function HelloWorld(){
        return "Hello World!";
    }
}