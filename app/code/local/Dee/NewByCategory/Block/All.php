<?php

class Dee_NewByCategory_Block_All extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productsCount = null;
    protected $_rowCount = null;

    const DEFAULT_PRODUCTS_COUNT = 21;
    const DEFAULT_PRODUCTS_ROW_LIMIT = 3;

    protected function _construct()
    {
          $this->addData(array(
               'cache_lifetime' => 86400,
               'cache_tags'     => array(Mage_Catalog_Model_Category::CACHE_TAG,
                   Mage_Catalog_Model_Product::CACHE_TAG),
               'cache_key'      => 'ALL_NEW_PROD_BLOCK_'.Mage::app()->getStore()->getCode(),
          ));
     }

    protected function _beforeToHtml()
    {
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                            'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                            'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                        ))
            ->addAttributeToFilter('news_from_date', array('date' => true, 'to' => $todayDate))
            ->addAttributeToFilter('news_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToSort('news_from_date', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1)
        ;
        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }

    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }

    public function getRowLimit()
    {
        if (null === $this->_rowCount) {
            $this->_rowCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_rowCount;
    }
}