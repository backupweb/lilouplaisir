<?php

/**
 * Account box block
 *
 * @category   Dee
 * @package    Dee_AccountBox
 * @author     Dee
 */
class Dee_AccountBox_Block_Connect extends Mage_Core_Block_Template
{
    private $_username = -1;

//    protected function _prepareLayout()
//    {
//        //to not modify the <title> with prefix Custpmer login
//        //$this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('Customer Login'));
//        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__(''));
//        return parent::_prepareLayout();
//    }

    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->helper('customer')->getLoginPostUrl();
    }

    /**
     * Retrieve create new account url
     *
     * @return string
     */
    public function getCreateAccountUrl()
    {
        $url = $this->getData('create_account_url');
        if (is_null($url)) {
            $url = $this->helper('customer')->getRegisterUrl();
        }
        return $url;
    }

    /**
     * Retrieve password forgotten url
     *
     * @return string
     */
    public function getForgotPasswordUrl()
    {
        return $this->helper('customer')->getForgotPasswordUrl();
    }

    /**
     * Retrieve username for form field
     *
     * @return string
     */
    public function getUsername()
    {
        if (-1 === $this->_username) {
            $this->_username = Mage::getSingleton('customer/session')->getUsername(true);
        }
        return $this->_username;
    }

    /**
     * Returns a welcome message after the client login
     * @return string
     */
    public function getWelcome()
    {
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_data['welcome'] = $this->__('Welcome, %s!', $this->htmlEscape(Mage::getSingleton('customer/session')->getCustomer()->getName()));
            } else {
                $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
            }
        }

        return $this->_data['welcome'];
    }
}
