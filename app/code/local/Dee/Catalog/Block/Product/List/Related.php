<?php

/**
 * Do not diplay out of stock products
 */
class Dee_Catalog_Block_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    protected $_itemCollection;

    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getRelatedProductCollection()
            ->addAttributeToSelect('required_options')
            ->addAttributeToSort('position', 'asc')
            ->addStoreFilter()
                
            //hide out of stock products
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
            ))
        ;
        Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
            Mage::getSingleton('checkout/session')->getQuoteId()
        );
        $this->_addProductAttributesAndPrices($this->_itemCollection);

//        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }
}