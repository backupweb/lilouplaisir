<?php
/* 
 * @author : Dee
 * Displays the menu like patagonia
 */

?>
<?php
class Dee_MenuExplore_Block_Navigation extends Mage_Core_Block_Template
{
    protected $_categoryInstance = null;

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime'    => 86400,
            'cache_tags'        => array(Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG),
        ));
    }

    /**
     * Retrieve Key for caching block content
     *
     * @return string
     */
    public function getCacheKey()
    {
        return 'CATALOG_NAVIGATION_' . Mage::app()->getStore()->getId()
            . '_' . Mage::getDesign()->getPackageName()
            . '_' . Mage::getDesign()->getTheme('template')
            . '_' . Mage::getSingleton('customer/session')->getCustomerGroupId()
            . '_' . md5($this->getTemplate() . $this->getCurrenCategoryKey());
    }

    public function getCurrenCategoryKey()
    {
        if ($category = Mage::registry('current_category')) {
            return $category->getPath();
        } else {
            return Mage::app()->getStore()->getRootCategoryId();
        }
    }

    /**
     * Get catagories of current store
     *
     * @return Varien_Data_Tree_Node_Collection
     */
    public function getStoreCategories()
    {
        $helper = Mage::helper('catalog/category');
        return $helper->getStoreCategories();
    }

    /**
     * Retrieve child categories of current category
     *
     * @return Varien_Data_Tree_Node_Collection
     */
    public function getCurrentChildCategories()
    {
        $layer = Mage::getSingleton('catalog/layer');
        $category   = $layer->getCurrentCategory();
        /* @var $category Mage_Catalog_Model_Category */
        $categories = $category->getChildrenCategories();
        $productCollection = Mage::getResourceModel('catalog/product_collection');
        $layer->prepareProductCollection($productCollection);
        $productCollection->addCountToCategories($categories);
        return $categories;
    }

    /**
     * Checkin activity of category
     *
     * @param   Varien_Object $category
     * @return  bool
     */
    public function isCategoryActive($category)
    {
        if ($this->getCurrentCategory()) {
            return in_array($category->getId(), $this->getCurrentCategory()->getPathIds());
        }
        return false;
    }

    protected function _getCategoryInstance()
    {
        if (is_null($this->_categoryInstance)) {
            $this->_categoryInstance = Mage::getModel('catalog/category');
        }
        return $this->_categoryInstance;
    }

    /**
     * Get url for category data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_getCategoryInstance()
                ->setData($category->getData())
                ->getUrl();
        }

        return $url;
    }

    /**
     * Enter description here...
     *
     * @param Mage_Catalog_Model_Category $category
     * @param int $level
     * @param boolean $last
     * @return string
     */
    public function drawItem($category, $level=0, $last=false)
    {
        $html = '';
        if (!$category->getIsActive()) {
            return $html;
        }
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = $category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = $children && $childrenCount;
        $html.= '<li';
        if ($hasChildren && $level==0) {
             $html.= ' onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"';
        }

        $html.= ' class="level'.$level;
        $html.= ' nav-'.str_replace('/', '-', Mage::helper('catalog/category')->getCategoryUrlPath($category->getRequestPath()));
        if ($this->isCategoryActive($category)) {
            $html.= ' active';
        }
        if ($last) {
            $html .= ' last';
        }
        if ($hasChildren) {
            $cnt = 0;
            foreach ($children as $child) {
                if ($child->getIsActive()) {
                    $cnt++;
                }
            }
            if ($cnt > 0) {
                $html .= ' parent';
            }
        }
        $html.= '">'."\n";
        $spanleft='';
        $spanright='';
        if ($level==0) {
             $spanleft='<span class="left"></span>';
             $spanright='<span class="right"></span>';
        }
        $html.= '<a href="'.$this->getCategoryUrl($category).'">'.$spanleft.'<span>'.$this->htmlEscape($category->getName()).'</span>'.$spanright.'</a>'."\n";
        

        if ($hasChildren){

            $j = 0;
            $htmlChildren = '';
            foreach ($children as $child) {
                if ($child->getIsActive()) {
                    $htmlChildren.= $this->drawItem($child, $level+1, ++$j >= $cnt);
                }
            }

            if (!empty($htmlChildren)) {
                //will put the render of a random prod here
                $prodrender='';
                if ($level==0){$prodrender=$this->drawRandomProduct($category);}
                $html.= '<ul class="level' . $level . '">'."\n"
                        .$htmlChildren
                        .$prodrender
                        .'</ul>';
            }

        }
        
        $html.= '</li>'."\n";
        return $html;
    }

    /**
     * Enter description here...
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if (Mage::getSingleton('catalog/layer')) {
            return Mage::getSingleton('catalog/layer')->getCurrentCategory();
        }
        return false;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getCurrentCategoryPath()
    {
        if ($this->getCurrentCategory()) {
            return explode(',', $this->getCurrentCategory()->getPathInStore());
        }
        return array();
    }

    /**
     * Enter description here...
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function drawOpenCategoryItem($category) {
        $html = '';
        if (!$category->getIsActive()) {
            return $html;
        }

        $html.= '<li';

        if ($this->isCategoryActive($category)) {
            $html.= ' class="active"';
        }

        $html.= '>'."\n";
        $html.= '<a href="'.$this->getCategoryUrl($category).'"><span>'.$this->htmlEscape($category->getName()).'</span></a>'."\n";

        if (in_array($category->getId(), $this->getCurrentCategoryPath())){
            $children = $category->getChildren();
            $hasChildren = $children && $children->count();

            if ($hasChildren) {
                $htmlChildren = '';
                foreach ($children as $child) {
                    $htmlChildren.= $this->drawOpenCategoryItem($child);
                }

                if (!empty($htmlChildren)) {
                    $html.= '<ul>'."\n"
                            .$htmlChildren
                            .'</ul>';
                }
            }
        }
        $html.= '</li>'."\n";
        return $html;
    }

    /*public function drawRandomProduct($category){
        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getModel('catalog/layer')->prepareProductCollection($collection);
        $collection->getSelect()->order('rand()');
        $collection->addStoreFilter();
        $numProducts = $this->getNumProducts() ? $this->getNumProducts() : 3;
        $collection->setPage(1, $numProducts);
        foreach ($collection as $_product){
            return $_product->getProductUrl();
        }
    }*/
    public function drawRandomProduct($category){

        $html = '';
        //ajouter une entrée ici si ajout de categorie principale il y a
        $tabMenu['Sextoys']='menu_sextoys';
        $tabMenu['Massages & Gourmandises']='menu_massage';
        $tabMenu['Hygiène & Stimulants']='menu_hygiene';
        $tabMenu['Accessoires érotiques']='menu_accessoires';
        $tabMenu['Jeux coquins']='menu_jeux';
        $tabMenu['Livres']='menu_livres';
        $tabMenu['Cadeaux sexy']='menu_cadeaux';
        $tabMenu['Les utiles !']='menu_utiles';
        //ajouter une entrée ici si ajout de categorie principale il y a

        //on arrete si il n'y a pas de la categorie n'est pas dans $tabMenu
        $drawProduct = false;
        foreach($tabMenu as $k => $v){
            if($k == $category->getName()){
                $drawProduct = true;
            }
        }
        if(!$drawProduct) return;
        //cela peut se produire dans le cas d'une creation d'une nouvelle categorie

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addStoreFilter()
            ->addAttributeToSelect(array('name', 'price', 'small_image'))
            //get the id of the category
            ->addAttributeToFilter($tabMenu[$category->getName()], array('Yes'=>true))
            ->setPageSize(1)//choisir le nombre de produit
            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);


        /*
        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getModel('catalog/layer')->prepareProductCollection($collection);
        $collection->getSelect()->order('rand()');
        $collection->addStoreFilter();
        $numProducts = $this->getNumProducts() ? $this->getNumProducts() : 3;
        $collection->setPage(1, $numProducts);
         * $_product->getSmallImageUrl()
         */
        foreach ($collection as $_product){
            $html.= '<li class="level1 menu_product">';
                $html.= '<div class="menu_product_image">';
                    $html.= '<a href="'.$_product->getProductUrl().'" title="">';
                        $html.= '<img src="'.$this->helper('catalog/image')->init($_product, 'small_image')->resize(80).'" alt="'.$_product->getName().'" title="'.$_product->getName().'"/>';
                    $html.= '</a>';
                $html.= '</div>';
                $html.= '<div class="menu_product_name">';
                    $html.= '<a href="'.$_product->getProductUrl().'" title="">';
                        $html.= $_product->getName();
                    $html.= '</a>';
                    $html.= '<span class="price">'.Mage::getBlockSingleton('catalog/product')->getPriceHtml($_product, true, '-new').'</span>';
                $html.= '</div>';
            $html.= '</li>';
        }

        return $html;
    }

}
