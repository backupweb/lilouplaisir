<?php
/* 
 * Author : Dee
 */
?>
<?php
class Dee_PromoProd_Block_OnePromoProd extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productsCount = null;

    const DEFAULT_PRODUCTS_COUNT = 5;

    protected function _construct()
    {
        $this->addData(array(
                'cache_lifetime' => 86400,
                'cache_tags'     => array(Mage_Catalog_Model_Category::CACHE_TAG,
                   Mage_Catalog_Model_Product::CACHE_TAG),
                'cache_key'      => 'PROMO_PROD_BLOCK_'.Mage::app()->getStore()->getCode(),
	));
    }

    protected function _beforeToHtml()
    {
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                    ))
            ->addAttributeToFilter('special_price', array('notnull'=>true))
            //->setPageSize($this->getProductsCount())
            //product limited to 5
            ->setPageSize(self::DEFAULT_PRODUCTS_COUNT)
            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');
        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }

    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
}
