<?php

/*
 * Author : Dee
 * Comment : No need to copy paste code ;
 * just extend from the class you want to use and modify the function you need
 */

/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dee_PromoProd_Block_ListAllPromoProducts extends Mage_Catalog_Block_Product_List
{
    protected function _construct()
    {
          $this->addData(array(
               'cache_lifetime' => 86400,
               'cache_tags'     => array(Mage_Catalog_Model_Category::CACHE_TAG,
                   Mage_Catalog_Model_Product::CACHE_TAG),
               'cache_key'      => 'ALL_PROMO_PROD_BLOCK_'.Mage::app()->getStore()->getCode(),
          ));
    }

    protected function _getProductCollection()
    {
        $now = new Zend_Date(Mage::getModel('core/date')->gmtTimestamp());
                
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                    ))
            ->addAttributeToFilter('special_price', array('notnull'=>true))
                
            ->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $now->get('MM/dd/yy')))
            ->addAttributeToFilter('special_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $now->get('MM/dd/yy')),
                1 => array('is' => new Zend_Db_Expr('null')))
                ), 'left')
                
//            ->addAttributeToFilter('manufacturer', $manufacturer)
            //get the id of the category
//            ->addAttributeToFilter('lilou_selection', array('Yes'=>true))
//            ->setPageSize(4)
//            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->_productCollection=$collection;

        return $this->_productCollection;
    }
}