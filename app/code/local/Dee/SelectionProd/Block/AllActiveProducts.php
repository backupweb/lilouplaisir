<?php

class Dee_SelectionProd_Block_AllActiveProducts extends Mage_Catalog_Block_Product_Abstract 
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->addData(array(
            'cache_lifetime' => 86400,
            'cache_tags' => array(Mage_Catalog_Model_Category::CACHE_TAG,
                Mage_Catalog_Model_Product::CACHE_TAG),
            'cache_key' => 'ALL_ACTIVE_PROD_BLOCK_' . Mage::app()->getStore()->getCode(),
        ));
    }
    
    public function getTotalInStockProducts()
    {
        $total = 0;
        
        $products = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('status')
                ->addStoreFilter()
                ->addWebsiteFilter()
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                    'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                    'website_id' => Mage::app()->getWebsite()->getWebsiteId()));
        
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        
        if ($products->getSize()) {
            $total = $products->getSize();
        }
        
        return $total;
    }
}