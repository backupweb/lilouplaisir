<?php
/* 
 * Author : Dee
 */

?>
<?php
class Dee_SelectionProd_Block_ToysSelection extends Mage_Catalog_Block_Product_Abstract {

    public function __construct() {
        parent::__construct();

        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');


        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                    ))
            //get the id of the category
            ->addAttributeToFilter('lilou_selection', array('Yes'=>true))
            ->setPageSize(4)
            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->setProductCollection($collection);


/****** celui-ci n'affiche pas la qty ******/
//        $storeId    = Mage::app()->getStore()->getId();
//        $product    = Mage::getModel('catalog/product');
//        /* @var $product Mage_Catalog_Model_Product */
//        $todayDate  = $product->getResource()->formatDate(time());
//        $products   = $product->setStoreId($storeId)->getCollection()
//            ->addStoreFilter()
//            ->addAttributeToFilter('lilou_selection', array('Yes'=>true))
//            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
//            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left')
//        ;
//       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
//        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
//        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
//
//        $products->setOrder('hot_deals')->setPageSize(4)->setCurPage(1);
//
//        $this->setProductCollection($products);


        $this->addData(array(
               'cache_lifetime' => 86400,
               'cache_tags'     => array(Mage_Catalog_Model_Category::CACHE_TAG,
                   Mage_Catalog_Model_Product::CACHE_TAG),
               'cache_key'      => 'SELECTION_PROD_BLOCK_'.Mage::app()->getStore()->getCode(),
          ));


    }
}