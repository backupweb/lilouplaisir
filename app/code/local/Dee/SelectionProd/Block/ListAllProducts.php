<?php

/*
 * Author : Dee
 * Comment : No need to copy paste code ;
 * just extend from the class you want to use and modify the function you need
*/

/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dee_SelectionProd_Block_ListAllProducts extends Mage_Catalog_Block_Product_List {
    protected function _getProductCollection() {
        $manufacturer=$this->getRequest()->getQuery('manufacturer');

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection = $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                    ))
                ->addAttributeToFilter('manufacturer', $manufacturer)
                //get the id of the category
//            ->addAttributeToFilter('lilou_selection', array('Yes'=>true))
//            ->setPageSize(4)
//            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->_productCollection=$collection;

        return $this->_productCollection;
    }

    public function getBrandNameById($id) {
        $resource = Mage::getSingleton('core/resource');
        $read= $resource->getConnection('core_read');

        $select = $read->select()->from('eav_attribute_option_value')->where('option_id = ?', $id);

        $manufacturer = $select->query();
        $row = $manufacturer->fetchAll();
        
        for($i=0; $i<count($row); $i++) {
            echo $row[$i]['value']."<br/>";
        }
    }
}