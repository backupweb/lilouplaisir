<?php

/*
 * Author : Dee
 */

/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dee_SelectionProd_Block_ListAllProducts extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Default product amount per row in grid display mode
     *
     * @var int
     */
    protected $_defaultColumnCount = 3;

    /**
     * Product amount per row in grid display mode depending
     * on custom page layout of category
     *
     * @var array
     */
    protected $_columnCountLayoutDepend = array();


    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
//        if (is_null($this->_productCollection)) {
//            $layer = Mage::getSingleton('catalog/layer');
//            /* @var $layer Mage_Catalog_Model_Layer */
//            if ($this->getShowRootCategory()) {
//                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
//            }
//
//            // if this is a product view page
//            if (Mage::registry('product')) {
//                // get collection of categories this product is associated with
//                $categories = Mage::registry('product')->getCategoryCollection()
//                    ->setPage(1, 1)
//                    ->load();
//                // if the product is associated with any category
//                if ($categories->count()) {
//                    // show products from this category
//                    $this->setCategoryId(current($categories->getIterator()));
//                }
//            }
//
//            $origCategory = null;
//            if ($this->getCategoryId()) {
//                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
//                if ($category->getId()) {
//                    $origCategory = $layer->getCurrentCategory();
//                    $layer->setCurrentCategory($category);
//                }
//            }
//            $this->_productCollection = $layer->getProductCollection();
//
//            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());
//
//            if ($origCategory) {
//                $layer->setCurrentCategory($origCategory);
//            }
//        }

//        $storeId    = Mage::app()->getStore()->getId();
//        $product    = Mage::getModel('catalog/product');
//        /* @var $product Mage_Catalog_Model_Product */
//        $todayDate  = $product->getResource()->formatDate(time());
//        $this->_productCollection   = $product->setStoreId($storeId)->getCollection();
//       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
//        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($this->_productCollection);
//        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_productCollection);
//
//        $this->setProductCollection($this->_productCollection);
//
//        return $this->_productCollection;

        $manufacturer=$this->getRequest()->getQuery('manufacturer');

//        $_product = Mage::getModel('catalog/product')->load(2);
//        var_dump($_product->getAttributeText('manufacturer'));

//        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
//        ->addFieldToFilter('attribute_code', 'manufacturer');
//        var_dump($attributes);
//        $attribute = $attributes->getFirstItem()->setEntity($_product->getResource());
//        echo '<p> Manufacturer :',$attribute->getSource()->getOptionText($cModel->getData('manufacturer')),'</p>';

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('manufacturer', $manufacturer)
            //get the id of the category
//            ->addAttributeToFilter('lilou_selection', array('Yes'=>true))
//            ->setPageSize(4)
//            ->setCurPage(1)
        ;
        $collection->getSelect()->order('rand()');

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $this->_productCollection=$collection;

        return $this->_productCollection;
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        /*$toolbar = $this->getLayout()->createBlock('catalog/product_list_toolbar', microtime());
        if ($toolbarTemplate = $this->getToolbarTemplate()) {
            $toolbar->setTemplate($toolbarTemplate);
        }*/
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to tollbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection'=>$this->_getProductCollection(),
        ));

        $this->_getProductCollection()->load();
        Mage::getModel('review/review')->appendSummary($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }


        return $this;
    }

    /**
     * Retrieve product amount per row in grid display mode
     *
     * @return int
     */
    public function getColumnCount()
    {
        if (!$this->_getData('column_count')) {
            $pageLayout = $this->getPageLayout();
            if ($pageLayout && $this->getColumnCountLayoutDepend($pageLayout->getCode())) {
                $this->setData(
                    'column_count',
                    $this->getColumnCountLayoutDepend($pageLayout->getCode())
                );
            } else {
                $this->setData('column_count', $this->_defaultColumnCount);
            }
        }

        return (int) $this->_getData('column_count');
    }

    /**
     * Add row size depends on page layout
     *
     * @param string $pageLayout
     * @param int $rowSize
     * @return Mage_Catalog_Block_Product_List
     */
    public function addColumnCountLayoutDepend($pageLayout, $columnCount)
    {
        $this->_columnCountLayoutDepend[$pageLayout] = $columnCount;
        return $this;
    }

    /**
     * Remove row size depends on page layout
     *
     * @param string $pageLayout
     * @return Mage_Catalog_Block_Product_List
     */
    public function removeColumnCountLayoutDepend($pageLayout)
    {
        if (isset($this->_columnCountLayoutDepend[$pageLayout])) {
            unset($this->_columnCountLayoutDepend[$pageLayout]);
        }

        return $this;
    }

    /**
     * Retrieve row size depends on page layout
     *
     * @param string $pageLayout
     * @return int|boolean
     */
    public function getColumnCountLayoutDepend($pageLayout)
    {
        if (isset($this->_columnCountLayoutDepend[$pageLayout])) {
            return $this->_columnCountLayoutDepend[$pageLayout];
        }

        return false;
    }

    /**
     * Retrieve current page layout
     *
     * @return Varien_Object
     */
    public function getPageLayout()
    {
        return $this->helper('page/layout')->getCurrentPageLayout();
    }
}

//class Dee_SelectionProd_Block_ListAllProducts extends Mage_Catalog_Block_Product_Abstract {
//
//    protected $_productCollection;
//
//    public function __construct() {
//        parent::__construct();
//
//        $storeId    = Mage::app()->getStore()->getId();
//        $product    = Mage::getModel('catalog/product');
//        /* @var $product Mage_Catalog_Model_Product */
//        $todayDate  = $product->getResource()->formatDate(time());
//        $products   = $product->setStoreId($storeId)->getCollection();
//       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
//        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
//        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
//
//        $this->setProductCollection($products);
//
//        $this->_productCollection = $products;
//    }
//
//    public function getLoadedProductCollection()
//    {
//        return $this->_productCollection;
//    }
//}