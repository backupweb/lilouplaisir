<?php
/* 
 * Author : Dee
 */

?>
<?php
class Dee_BestSeller_Block_BestSeller extends Mage_Catalog_Block_Product_Abstract
{
    public function __construct()
    {
        parent::__construct();

        $storeId    = Mage::app()->getStore()->getId();

//Real best seller
//        $products = Mage::getResourceModel('reports/product_collection')
//            ->addOrderedQty()
//            ->addAttributeToSelect('*')
//            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
//            ->setStoreId($storeId)
//            ->addStoreFilter($storeId)
//            ->setOrder('ordered_qty', 'desc'); //best sellers on top

        $collection = Mage::getResourceModel('catalog/product_collection');
        $products = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => Mage::app()->getWebsite()->getWebsiteId(),
                    ))
            //get the id of the category
            ->addAttributeToFilter('best_seller', array('Yes'=>true))
        ;

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

        $products->setPageSize(2);//->setCurPage(1);

        $products->getSelect()->order('rand()');

        $this->setProductCollection($products);

        $this->addData(array(
               'cache_lifetime' => 86400,
               'cache_tags'     => array(Mage_Catalog_Model_Category::CACHE_TAG,
                   Mage_Catalog_Model_Product::CACHE_TAG),
               'cache_key'      => 'BESTSELLER_PROD_BLOCK_'.Mage::app()->getStore()->getCode(),
        ));
    }
}