OrderExport = Class.create();

OrderExport.prototype = {

    initialize: function(url) {
        $$('.colissimo-weight').each(function(item) {
            item.observe('change', function() {
                new Ajax.Request(url + 'updateWeight', {
                    parameters: {
                        'weight': item.value,
                        'order_id': item.readAttribute('rel')
                    }
                });
            });
        });

        $$('.colissimo-code').each(function(item) {
            item.observe('change', function() {
                new Ajax.Request(url + 'updateCode', {
                    parameters: {
                        'code': item.value,
                        'order_id': item.readAttribute('rel')
                    }
                });
            });
        });
    }

};