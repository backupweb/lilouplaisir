ImportTracking = Class.create();

ImportTracking.prototype = {

    initialize: function(url) {
        this.request = null;
        this.url = url;

        var lines = $$('#file_import_grid_table tr');
        var tracking = [];

        lines.each(function(item) {
            if (typeof item !== 'undefined') {
                var columns = item.select('td');
                if (columns.length) {
                    var info = {
                        'order':     columns[0].innerHTML.trim(),
                        'track':     columns[1].innerHTML.trim(),
                        'mail':      columns[2],
                        'success':   columns[3],
                        'send_mail': 1,
                    };
                    tracking.push(info);
                }
            }
        });

        this.tracking = tracking;
    },

    run: function() {
        if (!this.request && this.tracking.length) {
            this.tracking[0].send_mail = this.tracking[0].mail.down('input').checked ? 1 : 0;
            this.request = new Ajax.Request(this.url, {
                onComplete: function (transport) {
                    // Update status
                    var data = transport.responseText.evalJSON();
                    this.tracking[0].success.update(data.message);

                    // Cancel request
                    this.request = null;

                    // Next
                    this.tracking.splice(0, 1);
                    this.run();
                }.bind(this),
                parameters: this.tracking[0]
            });
        }
    }

};