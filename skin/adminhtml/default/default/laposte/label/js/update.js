OrderUpdate = Class.create();

OrderUpdate.prototype = {

    initialize: function(url) {
        $$('.colissimo-field-update').each(function(item) {
            item.observe('change', function() {
                new Ajax.Request(url, {
                    onSuccess: function(response) {
                        var data = response.responseText.evalJSON();
                        if (item.type == 'checkbox') {
                            item.value = data.value ^ 1;
                        } else {
                            item.value = data.value;
                        }
                    },
                    parameters: {
                        'order_id': item.readAttribute('rel'),
                        'field':    item.name,
                        'value':    item.value
                    }
                });
            });
        });
    }

};