AdminOrder.prototype.setShippingMethod = function(method) {
    var data = {};
    data['order[shipping_method]'] = method;
    if(method == 'pickup_colissimo') {
        var list = $('colissimo-list');
        if (list) {
            list.remove();
        }
        this.pickupLoad();
    } else {
        this.loadArea(['shipping_method', 'totals', 'billing_method'], true, data);
    }
};

/**
 * New Methods to AdminOrder Class
 */
AdminOrder.addMethods({
    pickupInit: function(url) {
        this.PickupAction = url;
    },

    pickupLoad: function() {
        var pickup = this;
        new Ajax.Request(this.PickupAction, {
            onComplete: function(response) {
                var element = $('s_method_pickup_colissimo').up('li');
                element.insert(response.responseText);
                element.select('input').each(function(item) {
                    item.observe('click', function() {
                        var data = {
                            'order[shipping_method]':'pickup_colissimo',
                            'pickup':item.value,
                        };
                        pickup.loadArea(['shipping_method', 'totals', 'billing_method', 'shipping_address'], true, data);
                    });
                });
            }
        });
    }

});