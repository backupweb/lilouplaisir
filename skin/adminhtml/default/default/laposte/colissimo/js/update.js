PriceUpdate = Class.create();

PriceUpdate.prototype = {

    initialize: function(url) {
        $$('.colissimo-price-update').each(function(item) {
            item.observe('change', function() {
                new Ajax.Request(url, {
                    onSuccess: function(response) {
                        var data = response.responseText.evalJSON();
                        if (data.value) {
                            item.value = data.value;
                        } else {
                            item.value = '';
                        }
                    },
                    parameters: {
                        'price_id': item.readAttribute('rel'),
                        'field':    item.name,
                        'value':    item.value
                    }
                });
            });
        });
    }

};