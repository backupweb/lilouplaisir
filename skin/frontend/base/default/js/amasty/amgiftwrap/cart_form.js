$j(document).ready(function () {
    var clicked = false;

    $j('body').on('click', 'a.ca-choose', function (e) {
        e.preventDefault();
        e.stopPropagation();
        try {

            /*
             check if selected item clicked again
             in that case - disable selection
             */
            if ($j(this).parents('.ca-item-main').hasClass('selected')) {
                $j(this).parents('.ca-item-main').removeClass('selected');
                if ($j(this).data('type') == 'design') {
                    amgiftwrap_design_id = 0;
                    $j('#amgiftwrap_design_id').attr('value', 0);
                } else {
                    amgiftwrap_card_id = 0;
                    $j('#amgiftwrap_card_id').attr('value', 0);
                    //$j('.tabs li[code="flip-5"]:visible').animate({width: 'toggle'}, "500");
                    $j('#gift-message-whole-enabled').attr('value', "0");
                }
                return false;
            }

            /*
             set "selected" effects on current slide
             */
            $j('#' + $j(this).data('type') + '-container .ca-item-main').removeClass('selected');
            $j(this).parent().addClass('selected');

            /*
             save selected card\design data
             */
            if ($j(this).data('type') == 'design') {
                amgiftwrap_design_id = $j(this).data('value');
                $j('#amgiftwrap_design_id').attr('value', $j(this).data('value'));
            } else {
                amgiftwrap_card_id = $j(this).data('value');
                $j('#amgiftwrap_card_id').attr('value', $j(this).data('value'));
                //$j('.tabs li[code="flip-5"]:hidden').animate({width: 'toggle'}, "500");
                $j('#gift-message-whole-enabled').attr('value', "1");
            }
        } catch (err) {
        }
    });

    $j('body').on('click', '#amgiftwrap_popup_form_save_button', function (e) {
        e.preventDefault();
        try {

            // disable all "fake" forms with data to minimize POST data sent
            $j('input[name="gift-message-whole-from-hook"]').attr('disabled', 'disabled');
            $j('input[name="gift-message-whole-to-hook"]').attr('disabled', 'disabled');
            $j('textarea[name="gift-message-whole-message-hook"]').attr('disabled', 'disabled');

            // perform AJAX call on save
            $j.ajax({
                type: "POST",
                url: amgiftwrap_save_form_data_url,
                data: $j('#amgiftwrap_popup_form').serialize(),
                beforeSend: function () {
                    // disable button to prevent spam clicking on the button
                    $j('#amgiftwrap_popup_form_save_button').prop('disabled', true);
                    $j('#amgiftwrap_popup_form_save_button').css('opacity', '0.2');
                    $j('#amgiftwrap_popup_form_save_button:hover').css('opacity', '0.2');
                }
            }).done(function (data) {
                $j('#amgiftwrap_popup_form_answer').html(data).fadeIn(500);
                if (amgiftwrap_only_button == '0') {
                    setTimeout('window.location.reload();', 500);
                } else {
                    $j.fancybox.close();
                    if(typeof updateReview != undefined) {
                        updateReview();
                    }
                }
            }).fail(function (error) {
                console.log(error);
                $j.fancybox.close();
            });
        } catch (err) {
        }
    });

    $j('body').on('click', '.amgiftwrap-disabled-product', function (e) {
        e.preventDefault();
    });

    $j('body').on('click', '.content-wrap .section-flip-last', function (e) {
        $j('.tabs nav ul li:last').click();
    });

    $j('body').on('click', '.ca-item-main:not(.ca-choose)', function (e) {
        e.preventDefault();
        e.stopPropagation();
        try {
            if ($j(this).parents('.ca-item').find('.ca-content-wrapper').width() == 0) {
                $j(this).find('a.ca-more').click();
                if (!$j(this).hasClass('selected')) {
                    $j(this).find('a.ca-choose').click();
                }
            } else {
                $j(this).parents('.ca-item').find('a.ca-close').click();
            }
        } catch (err) {
        }
    });

    /*
     * set Message Card form setting hooks
     * because thare are a fake forms for each card
     * and only one real form is valueable after submit
     */
    $j('body').on('change', 'input[name="gift-message-whole-from-hook"]', function () {
        $j('input[name="gift-message-whole-from-hook"]').val($j(this).val());
        $j('#gift-message-whole-from').val($j(this).val());
    });
    $j('body').on('change', 'input[name="gift-message-whole-to-hook"]', function () {
        $j('input[name="gift-message-whole-to-hook"]').val($j(this).val());
        $j('#gift-message-whole-to').val($j(this).val());
    });
    $j('body').on('change', 'textarea[name="gift-message-whole-message-hook"]', function () {
        $j('textarea[name="gift-message-whole-message-hook"]').val($j(this).val());
        $j('#gift-message-whole-message').val($j(this).val());
    });


    /*
     *  Clear Selection buttons
     */
    $j('body').on('click', '#amgiftwrap_design_clear', function (e) {
        e.preventDefault();
        e.stopPropagation();
        try {
            amgiftwrap_design_id = 0;
            $j('#design-container .ca-item-main.selected')[0].up().down('a.ca-close').click();
            $j('#design-container .ca-item-main').removeClass('selected');
            $j('#amgiftwrap_design_id').attr('value', 0);
        } catch (err) {
        }
    });
    $j('body').on('click', '#amgiftwrap_message_cards_clear', function (e) {
        e.preventDefault();
        e.stopPropagation();
        try {
            amgiftwrap_card_id = 0;
            $j('#card-container .ca-item-main.selected')[0].up().down('a.ca-close').click();
            $j('#card-container .ca-item-main').removeClass('selected');
            $j('#amgiftwrap_card_id').attr('value', 0);
            $j('#gift-message-whole-enabled').attr('value', "0");
        } catch (err) {
        }
    });
});
