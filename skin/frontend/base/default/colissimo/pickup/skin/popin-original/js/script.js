ShippingMethodClass.addMethods({
    pickupInit: function(url, showMap)
    {
        this.PickupController = url;
        this.PickupContainer = 'colissimo-popup-inner';
        this.PickupMaps = showMap ? new ColissimoMaps : null;
        this.PickupActions = {'load':'list'};
        this.PickupPopUp = new ColissimoPopup();
    },

    pickupLaunch: function()
    {
        this.pickupRun();
    },

    pickupNext: function()
    {
        var pickup = this;
        checkout.setLoadWaiting('shipping-method');
        new Ajax.Request(
            this.saveUrl, {
                method: 'post',
                onComplete: function() {
                    checkout.setLoadWaiting(false);
                    checkout.reloadStep('shipping');
                    pickup.pickupRemoveFormData();
                },
                onSuccess: this.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(this.form)
            }
        );
    },

    pickupAddContainer: function()
    {
        this.PickupPopUp.open(940, 680);
    },

    pickupFailure: function()
    {
        checkout.ajaxFailure.bind(checkout)
    },

    pickupBeforeLoadContent: function()
    {
        checkout.setLoadWaiting('shipping-method');
    },

    pickupAfterLoadContent: function()
    {
        checkout.setLoadWaiting(false, false);

        $('colissimo-next').observe('click', function(event) {
            this.pickupAddFormData();
            this.PickupPopUp.close();
            this.pickupNext();
            Event.stop(event);
        }.bind(this));
    },

    pickupAddFormData: function()
    {
        var formId = 'colissimo-popup-form';

        if ($(formId)) {
            $(formId).remove();
        }

        var form = new Element('div');
        form.setAttribute('id', formId);
        form.hide();
        form.insert($$('input[name=pickup]:checked').first());
        if ($('colissimo-customer')) {
            $('colissimo-customer').select('input').each(function (item) {
                form.insert(item);
            });
        }

        $('checkout-shipping-method-load').insert(form);
    },

    pickupRemoveFormData: function()
    {
        var formId = 'colissimo-popup-form';

        if ($(formId)) {
            $(formId).remove();
        }
    }

});

ColissimoPopup = Class.create();

ColissimoPopup.prototype = {

    initialize: function() {
        this.PopupContainer = null;
        this.PopupInner = null;
        this.PopupMessage = null;
        this.PopupLoader = null;
        this.PopupIsMobile = false;
    },

    open: function(width, height) {
        if (this.PopupContainer && this.PopupInner) {
            return;
        }

        var body = $$('body').first();

        this.PopupContainer = new Element('div');
        this.PopupContainer.addClassName('colissimo-popup-container');
        this.PopupContainer.setAttribute('id', 'colissimo-popup-container');
        this.PopupContainer.setStyle(
            {'height':(document.viewport.getDimensions()).height, 'cursor':'pointer'}
        );
        this.PopupContainer.observe('click', function(event){
            this.close();
        }.bind(this));

        this.PopupInner = new Element('div');
        this.PopupInner.addClassName('colissimo-popup-inner');
        this.PopupInner.setAttribute('id', 'colissimo-popup-inner');
        this.PopupInner.setStyle(
            {'height':height+'px', 'marginTop':(-height/2)+'px'}
        );

        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth;

        if (x <= width) {
            this.PopupIsMobile = true;
            window.scrollTo(0, 0);
            this.PopupInner.setStyle({'marginTop':0,'top':0});
        }

        new Insertion.Top(body,this.PopupContainer);
        new Insertion.Top(body,this.PopupInner);
    },

    update: function(content) {
        this.closeLoader();
        if(this.PopupInner) { this.PopupInner.update(content); }

        if (this.PopupIsMobile) {
            window.scrollTo(0, 0);
        }
    },

    close: function() {
        if (this.PopupInner) {
            this.PopupInner.remove();
            this.PopupInner = null;
        }
        if (this.PopupContainer) {
            this.PopupContainer.remove();
            this.PopupContainer = null;
        }
    },

    error: function(content) {
        this.message('<span class="warning">' + content + '</span>', true);
    },

    message: function(message, close) {
        this.closeLoaderWithEffect();
        this.closeMessage();
        this.PopupMessage = new Element('div');
        this.PopupMessage.addClassName('colissimo-message');
        this.PopupMessage.update(message);

        new Insertion.Before(this.PopupInner.firstChild,this.PopupMessage);

        var height = parseInt(this.PopupMessage.getHeight())+5;
        this.PopupMessage.setStyle({'marginTop':'-'+height+'px'});

        if (this.PopupIsMobile) {
            window.scrollTo(0, 0);
        }

        if(close) {
            this.PopupMessage.observe('click', function(event){
                this.closeMessageWithEffect();
            }.bind(this));
        }

        new Effect.Morph(this.PopupMessage, {style: 'margin-top:0',duration: 0.5});
    },

    closeMessage: function() {
        if(this.PopupMessage) { this.PopupMessage.remove(); this.PopupMessage = null; }
    },

    closeMessageWithEffect: function() {
        if(this.PopupMessage) {
            var height = parseInt(this.PopupMessage.getHeight())+5;
            new Effect.Morph(this.PopupMessage,{
                style: 'margin-top:-'+height+'px',
                duration: 0.5,
                afterFinish: function(){this.closeMessage();}.bind(this)
            });
        }
    },

    loader: function(content) {
        this.closeLoader();

        this.PopupLoader = new Element('div');
        this.PopupLoader.addClassName('colissimo-load');
        this.PopupLoader.update('<span class="colissimo-load-inner">'+content+'</span>');

        this.PopupInner.insert({'top':this.PopupLoader});

        this.PopupInner.select('input[type="submit"]', 'button').each(function(item) {
            item.disabled = true;
        });
    },

    closeLoader: function() {
        if(this.PopupLoader) {
            this.PopupLoader.remove(); this.PopupLoader = null;
            this.PopupInner.select('input[type="submit"]', 'button').each(function(item) {
                item.disabled = false;
            });
        }
    },

    closeLoaderWithEffect: function() {
        if(this.PopupLoader) {
            new Effect.Fade(this.PopupLoader,{
                duration: 0.3,
                afterFinish: function(){this.closeLoader();}.bind(this)
            });
        }
    }

};