ShippingMethodClass.addMethods({
    pickupInit: function(url, showMap)
    {
        this.PickupController = url;
        this.PickupContainer = 'checkout-shipping-method-load';
        this.PickupMaps = showMap ? new ColissimoMaps : null;
        this.PickupActions = {'load':'list'};
    },

    pickupLaunch: function()
    {
        var pickup = $$('input[name=pickup]');

        if (!pickup.length) {
            this.pickupRun();
        } else {
            this.pickupNext();
        }
    },

    pickupNext: function()
    {
        checkout.setLoadWaiting('shipping-method');
        new Ajax.Request(
            this.saveUrl, {
                method: 'post',
                onComplete: function() {
                    checkout.setLoadWaiting(false);
                    checkout.reloadStep('shipping');
                },
                onSuccess: this.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(this.form)
            }
        );
    },

    pickupAddContainer: function()
    {

    },

    pickupFailure: function()
    {
        checkout.ajaxFailure.bind(checkout)
    },

    pickupBeforeLoadContent: function()
    {
        checkout.setLoadWaiting('shipping-method');
    },

    pickupAfterLoadContent: function()
    {
        checkout.setLoadWaiting(false, false);
    }

});