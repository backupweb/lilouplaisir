var ShippingMethodClass = ShippingMethod;

/* Chronopost module compatibility */
if(typeof ShippingMethodChrono !== "undefined") {
    ShippingMethodClass = ShippingMethodChrono;
}

/* Overload save method */
ShippingMethodClass.prototype.save = function()
{
    if (checkout.loadWaiting != false) return;
    if (this.validate()) {
        var shipping = $$('input[name=shipping_method]:checked');

        var value = shipping.first().value;

        if (value == 'pickup_colissimo') { // Pickup
            this.pickupLaunch();
        } else if(value == 'homecl_colissimo') { // Home without signature
            this.methodLaunch();
        } else if(value == 'homesi_colissimo') { // Home with signature
            this.methodLaunch();
        } else if(value == 'international_colissimo') { // Expert International
            this.methodLaunch();
        } else if(value == 'domtomcl_colissimo') { // Dom-Tom without signature
            this.methodLaunch();
        } else if(value == 'domtomsi_colissimo') { // Dom-Tom with signature
            this.methodLaunch();
        } else {
            if (checkout.loadWaiting!=false) return;
            if (this.validate()) {
                checkout.setLoadWaiting('shipping-method');
                new Ajax.Request(
                    this.saveUrl,
                    {
                        method:'post',
                        onComplete: function() {
                            checkout.setLoadWaiting(false);
                            checkout.reloadStep('shipping');
                        },
                        onSuccess: this.onSave,
                        onFailure: checkout.ajaxFailure.bind(checkout),
                        parameters: Form.serialize(this.form)
                    }
                );
            }
        }
    }
};