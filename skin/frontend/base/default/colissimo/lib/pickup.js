ShippingMethodClass.addMethods({

    /**
     * Run
     *
     * @param {object} params
     */
    pickupRun: function(params)
    {
        if (typeof params == 'undefined') {
            params = {};
        }

        this.pickupAddContainer();
        this.pickupLoadContent(this.PickupActions.load, params);
    },

    /**
     * Load pop-up content with Ajax Request
     *
     * @param {string} action
     * @param {Object} data
     */
    pickupLoadContent: function(action, data)
    {
        this.pickupBeforeLoadContent();
        new Ajax.Request(this.PickupController + action, {
            onComplete: function(transport) {
                $(this.PickupContainer).update(transport.responseText);
                this.pickupAfterLoadContent();
            }.bind(this),
            onFailure: this.pickupFailure(),
            parameters: data
        });
    },

    /**
     * Init list action
     *
     * @param {Object.<number, Object>} locations
     */
    pickuplistInit: function(locations)
    {
        var maps = this.PickupMaps;

        var elements = {
            'map':'colissimo-map',
            'list':'colissimo-list',
            'address':'colissimo-address',
            'update_address':'colissimo-current-address'
        };

        if (locations.length) {
            if (maps) {
                maps.run(elements.map, elements.list);
                maps.locations(locations);
                var address = '';
                $(elements.address).select('input').each(function (input) {
                    address += input.value + ' ';
                });
                if (address) {
                    maps.address(address);
                }
            }

            /* Select pickup */
            $(elements.list).select('input').each(function (input) {
                input.observe('click', function() {
                    this.pickuplistSelect(input, elements, maps);
                }.bind(this));
            }.bind(this));
            $(elements.list).select('button').each(function (button) {
                button.observe('click', function(event) {
                    var input = button.up('li').select('input').first();
                    this.pickuplistSelect(input, elements, maps);
                    Event.stop(event);
                }.bind(this));
            }.bind(this));
        }

        /* Update address */
        var updateAddress = $(elements.update_address);

        if (updateAddress) {
            updateAddress.select('a').first().observe('click', function(event) {
                $(elements.address).toggle();
                Event.stop(event);
            });
        }

        /* Address form */
        $(elements.address).select('button').first().observe('click', function(event) {
            var parameters = {};
            $(elements.address).select('input').each(function(input) {
                parameters[input.name] = input.value;
            });
            $(elements.address).select('select').each(function(select) {
                parameters[select.name] = select.value;
            });
            this.pickupLoadContent(this.PickupActions.load, parameters);
            Event.stop(event);
        }.bind(this));
    },

    pickuplistSelect: function(input, elements, maps)
    {
        input.checked = true;

        $(elements.list).select('li').each(function (element) {
            element.removeClassName('active');
        });
        input.up('li').addClassName('active');

        $$('.colissimo-info').each(function(element) {
            element.hide();
        });
        $('colissimo-info-' + input.value).show();

        if (maps) {
            maps.update(input.id);
        }
    }

});