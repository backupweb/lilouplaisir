ColissimoMaps = Class.create();

ColissimoMaps.prototype = {

    /**
     * Initialize variables
     */
    initialize: function() {
        this.mapId = null;
        this.listId = null;
        this.infowindow = null;
        this.markers = [];
    },

    /**
     * Load map
     *
     * @param {string} mapId - Id of Map element
     * @param {string} listId - Id of location list
     */
    run: function (mapId, listId) {
        this.mapId = mapId;
        this.listId = listId;
        this.infowindow = new google.maps.InfoWindow();
        this.map = new google.maps.Map(document.getElementById(this.mapId), {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    },

    /**
     * Add locations to map
     *
     * @param {Object.<number, Object>} locations - All points on the map
     */
    locations: function (locations) {
        var googleMap = this;
        var bounds = new google.maps.LatLngBounds();
        var marker, i;

        for (i = 0; i < locations.length; i++) {
            if (typeof locations[i] !== 'undefined') {
                var LatLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
                bounds.extend(LatLng);
                var markerInfo = {position: LatLng, map: googleMap.map};
                if (locations[i][4]) {
                    markerInfo.icon = locations[i][4];
                }
                marker = new google.maps.Marker(markerInfo);
                googleMap.markers[locations[i][3]] = {content: locations[i][0], marker: marker};
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        googleMap.infowindow.setContent(locations[i][0]);
                        googleMap.infowindow.open(googleMap.map, marker);
                        googleMap.select(locations[i][3]);
                    }
                })(marker, i));
            }
        }

        googleMap.map.fitBounds(bounds);
    },

    /**
     * Add specific address on map
     *
     * @param {string} address - Address to show on the map
     */
    address: function (address) {
        var googleMap = this;
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({address: address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                new google.maps.Marker({
                    map: googleMap.map,
                    position: results[0].geometry.location
                });
            }
        });

    },

    /**
     * Select location in the list
     *
     * @param {string} inputId - Id of input element
     */
    select: function (inputId) {
        var input = $(inputId);
        if (input) {
            var list = $(this.listId);

            if (list) {
                list.select('li').each(function (item) {
                    item.removeClassName('active');
                });
                input.checked = 'checked';
                input.up('li').addClassName('active');

                $$('.colissimo-info').each(function(element) {
                    element.hide();
                });
                $('colissimo-info-' + input.value).show();

                var offsetTopUl = list.offsetTop;
                var offsetTopLi = input.up('li').offsetTop;

                list.scrollTop = (parseInt(offsetTopLi) - parseInt(offsetTopUl));
            }
        }
    },

    /**
     * Show marker on map
     *
     * @param {string} locationId - Id of the marker
     */
    update: function (locationId) {
        if (this.markers[locationId]) {
            this.infowindow.setContent(this.markers[locationId].content);
            this.infowindow.open(this.map, this.markers[locationId].marker);
        }
    }

};