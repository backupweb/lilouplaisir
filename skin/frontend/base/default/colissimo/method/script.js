ShippingMethodClass.addMethods({

    methodInit: function(url, options) {
        this.MethodController = url;
        this.MethodOptions = options;
        this.MethodContainer = 'checkout-shipping-method-load';
        this.MethodActions = {'load':'form'};
    },

    methodLaunch: function() {
        var telephone = $$('input[name=telephone]');

        if (this.MethodOptions[this.methodGetShippingMethod()].phone && !telephone.length) {
            this.methodRun();
        } else {
            this.methodNext();
        }
    },

    methodRun: function() {
        this.methodLoadContent(this.MethodActions.load, {'shipping_method':this.methodGetShippingMethod()});
    },

    methodGetShippingMethod: function() {
        var shipping = $$('input[name=shipping_method]:checked');
        return shipping.first().value;
    },

    methodLoadContent: function(action, data)
    {
        this.Request = new Ajax.Request(this.MethodController + action, {
            onComplete: function(transport) {
                $(this.MethodContainer).update(transport.responseText);
            }.bind(this),
            onFailure: this.methodFailure(),
            parameters: data
        });
    },

    methodFailure: function()
    {
        checkout.ajaxFailure.bind(checkout)
    },

    methodNext: function()
    {
        checkout.setLoadWaiting('shipping-method');
        new Ajax.Request(
            this.saveUrl, {
                method: 'post',
                onComplete: function() {
                    checkout.setLoadWaiting(false);
                    checkout.reloadStep('shipping');
                },
                onSuccess: this.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(this.form)
            }
        );
    },

});